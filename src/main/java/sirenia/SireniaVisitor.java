// Generated from E:/gitlab-repo/sirenia-lang/src/main/resources/Sirenia.g4 by ANTLR 4.13.1
package sirenia;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SireniaParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SireniaVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SireniaParser#file}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFile(SireniaParser.FileContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#exprs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprs(SireniaParser.ExprsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Loop}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop(SireniaParser.LoopContext ctx);
	/**
	 * Visit a parse tree produced by the {@code TryCatch}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTryCatch(SireniaParser.TryCatchContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Cond}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCond(SireniaParser.CondContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IdAttrExpr}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdAttrExpr(SireniaParser.IdAttrExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code DefExpr}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefExpr(SireniaParser.DefExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Defn}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefn(SireniaParser.DefnContext ctx);
	/**
	 * Visit a parse tree produced by the {@code javaInterOp}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJavaInterOp(SireniaParser.JavaInterOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BinExpr}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinExpr(SireniaParser.BinExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ScopeExpr}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitScopeExpr(SireniaParser.ScopeExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprAttrExpr}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprAttrExpr(SireniaParser.ExprAttrExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code idRefExpr}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdRefExpr(SireniaParser.IdRefExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code PreExpr}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPreExpr(SireniaParser.PreExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Throw}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThrow(SireniaParser.ThrowContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Literal}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(SireniaParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ParenExpr}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExpr(SireniaParser.ParenExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code category}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCategory(SireniaParser.CategoryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code If}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf(SireniaParser.IfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FnCallExpr}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFnCallExpr(SireniaParser.FnCallExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code pipExpr}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPipExpr(SireniaParser.PipExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Recur}
	 * labeled alternative in {@link SireniaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecur(SireniaParser.RecurContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#categoryExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCategoryExpr(SireniaParser.CategoryExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#maybeExpandExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMaybeExpandExpr(SireniaParser.MaybeExpandExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code importClass}
	 * labeled alternative in {@link SireniaParser#javaInterOpExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImportClass(SireniaParser.ImportClassContext ctx);
	/**
	 * Visit a parse tree produced by the {@code newObjExpr}
	 * labeled alternative in {@link SireniaParser#javaInterOpExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewObjExpr(SireniaParser.NewObjExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code methodCallExpr}
	 * labeled alternative in {@link SireniaParser#javaInterOpExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodCallExpr(SireniaParser.MethodCallExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code fieldGetExpr}
	 * labeled alternative in {@link SireniaParser#javaInterOpExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFieldGetExpr(SireniaParser.FieldGetExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code fieldSetExpr}
	 * labeled alternative in {@link SireniaParser#javaInterOpExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFieldSetExpr(SireniaParser.FieldSetExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#typedExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypedExpr(SireniaParser.TypedExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#defItem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefItem(SireniaParser.DefItemContext ctx);
	/**
	 * Visit a parse tree produced by the {@code defnExpr1}
	 * labeled alternative in {@link SireniaParser#defnExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefnExpr1(SireniaParser.DefnExpr1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code defnExprN}
	 * labeled alternative in {@link SireniaParser#defnExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefnExprN(SireniaParser.DefnExprNContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#fnParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFnParams(SireniaParser.FnParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#paramsD}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParamsD(SireniaParser.ParamsDContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#throwExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThrowExpr(SireniaParser.ThrowExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#tryCatchExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTryCatchExpr(SireniaParser.TryCatchExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#tryBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTryBlock(SireniaParser.TryBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#catchBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCatchBlock(SireniaParser.CatchBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#finallyBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFinallyBlock(SireniaParser.FinallyBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#ifExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfExpr(SireniaParser.IfExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#condExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondExpr(SireniaParser.CondExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#loopExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoopExpr(SireniaParser.LoopExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#loopParam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoopParam(SireniaParser.LoopParamContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#recurExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecurExpr(SireniaParser.RecurExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code charLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharLiteral(SireniaParser.CharLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code strLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrLiteral(SireniaParser.StrLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code zeroLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitZeroLiteral(SireniaParser.ZeroLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntLiteral(SireniaParser.IntLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code floatLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloatLiteral(SireniaParser.FloatLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code trueLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrueLiteral(SireniaParser.TrueLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code falseLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFalseLiteral(SireniaParser.FalseLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nullLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullLiteral(SireniaParser.NullLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tmplStringLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTmplStringLiteral(SireniaParser.TmplStringLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code emptyMapLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptyMapLiteral(SireniaParser.EmptyMapLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code MapLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMapLiteral(SireniaParser.MapLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code emptyVecLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptyVecLiteral(SireniaParser.EmptyVecLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code VecLiteral}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVecLiteral(SireniaParser.VecLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Fn}
	 * labeled alternative in {@link SireniaParser#literalExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFn(SireniaParser.FnContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#fnLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFnLiteral(SireniaParser.FnLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IdKeyPair}
	 * labeled alternative in {@link SireniaParser#kvPair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdKeyPair(SireniaParser.IdKeyPairContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StrKeyPair}
	 * labeled alternative in {@link SireniaParser#kvPair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrKeyPair(SireniaParser.StrKeyPairContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExprKeyPair}
	 * labeled alternative in {@link SireniaParser#kvPair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprKeyPair(SireniaParser.ExprKeyPairContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expandPair}
	 * labeled alternative in {@link SireniaParser#kvPair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpandPair(SireniaParser.ExpandPairContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#structParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStructParams(SireniaParser.StructParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#vecStructParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVecStructParams(SireniaParser.VecStructParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#mapStructParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMapStructParams(SireniaParser.MapStructParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#structPair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStructPair(SireniaParser.StructPairContext ctx);
	/**
	 * Visit a parse tree produced by {@link SireniaParser#as}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAs(SireniaParser.AsContext ctx);
}