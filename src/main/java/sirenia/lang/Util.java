package sirenia.lang;

import io.vavr.collection.Seq;
import io.vavr.collection.Vector;
import org.apache.commons.text.StringEscapeUtils;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public abstract class Util {
    public static <T> T ret1(T o, Object ignore) {
        return o;
    }

    public static RuntimeException toRE(Exception e) {
        if (e instanceof RuntimeException it) return it;
        return new RuntimeException(e);
    }

    public static <R,T> R also(T o,Function<T,R> f){
        return f.apply(o);
    }

    /**
     * 匹配方法
     * pTypes指定方法参数类型。
     * 如果某个位置的参数类型未指定（为null），则匹配任意类型。
     * */
    public static Method findMethod(String className, String methodName, Seq<String> pTypes) {
        Class<?> c = forName(className);
        Vector<Method> methods = Vector.ofAll(Arrays.asList(c.getMethods()));
        Vector<Method> matched = methods.filter(m->{
            if(!m.getName().equals(methodName))return false;
            if(m.getParameterCount()!=pTypes.length())return false;
            Class<?>[] mptypes = m.getParameterTypes();
            int i=0;
            for(String ptype: pTypes){
                if(ptype==null){
                    i++;
                }else if(!ptype.equals(mptypes[i++].getName())){
                    return false;
                }
            }
            return true;
        });
        if(matched.isEmpty()){
            throw new RuntimeException(String.format("method not found, class=%s, method=%s, pTypes=%s",
                    className, methodName, pTypes.mkString("[",",","]")));
        }else if(matched.length()>1){
            throw new RuntimeException(String.format("%s method found, class=%s, method=%s, pTypes=%s",
                    matched.length(), className, methodName, pTypes.mkString("[",",","]")));
        }else{
            return matched.head();
        }
    }

    public static Constructor findCtor(String className, Seq<String> pTypes) {
        Class<?> c = forName(className);
        Vector<Constructor> ctors = Vector.ofAll(Arrays.asList(c.getConstructors()));
        Vector<Constructor> matched = ctors.filter(m->{
            if(m.getParameterCount()!=pTypes.length())return false;
            Class<?>[] mptypes = m.getParameterTypes();
            int i=0;
            for(String ptype: pTypes){
                if(ptype==null){
                    i++;
                }else if(!ptype.equals(mptypes[i++].getName())){
                    return false;
                }
            }
            return true;
        });
        if(matched.isEmpty()){
            throw new RuntimeException(String.format("constructor not found, class=%s, pTypes=%s",
                    className, pTypes.mkString("[",",","]")));
        }else if(matched.length()>1){
            throw new RuntimeException(String.format("%s constructor found, class=%s, pTypes=%s",
                    matched.length(), className , pTypes.mkString("[",",","]")));
        }else{
            return matched.head();
        }
    }


    public static Field findField(String className, String fieldName) {
        try {
            Class<?> c = Class.forName(className);
            return c.getField(fieldName);
        } catch (Exception e) {
            throw sneakyThrow(e);
        }
    }

    public static Class<?> forName(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw sneakyThrow(e);
        }
    }

    public static String trimN(String str, int n) {
        int len = str.length();
        return str.substring(n, len - n);
    }

    public static String unescapeJava(String text) {
        return StringEscapeUtils.unescapeJava(text);
    }

    public static String typeDescriptor(String className) {
        int arrBegin = className.indexOf('[');
        if (arrBegin > 0) {
            StringBuilder sb = new StringBuilder();
            String component = objectInnerType(className.substring(0, arrBegin));
            int dim = (className.length() - arrBegin) / 2;
            sb.append("[".repeat(dim));
            sb.append(component).append(";");
            return sb.toString();
        } else {
            return switch (className) {
                case "void" -> "V";
                case "boolean" -> "Z";
                case "char" -> "C";
                case "byte" -> "B";
                case "short" -> "S";
                case "int" -> "I";
                case "float" -> "F";
                case "long" -> "J";
                case "double" -> "D";
                default -> "L" + objectInnerType(className) + ";";
            };
        }
//        throw new IllegalArgumentException("Invalid className: " + className);
    }

    public static String objectInnerType(String className) {
        return className.replace('.', '/');
    }

    public static String simpleClassName(String className){
        int idx = className.lastIndexOf('.');
        return idx>-1?className.substring(idx+1):className;
    }
    static public <K, V> void clearCache(ReferenceQueue rq, ConcurrentHashMap<K, Reference<V>> cache) {
        //cleanup any dead entries
        if (rq.poll() != null) {
            while (rq.poll() != null);
            for (java.util.Map.Entry<K, Reference<V>> e : cache.entrySet()) {
                Reference<V> val = e.getValue();
                if (val != null && val.get() == null)
                    cache.remove(e.getKey(), val);
            }
        }
    }

    static public RuntimeException sneakyThrow(Throwable t) {
        // http://www.mail-archive.com/javaposse@googlegroups.com/msg05984.html
        if (t == null)
            throw new NullPointerException();
        Util.<RuntimeException>sneakyThrow0(t);
        return null;
    }

    @SuppressWarnings("unchecked")
    static private <T extends Throwable> void sneakyThrow0(Throwable t) throws T {
        throw (T) t;
    }
    public static final <T,R> R nullOrMap(T o, Function<T,R> f){
        return o==null?null:f.apply(o);
    }

}
