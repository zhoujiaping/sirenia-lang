package sirenia.lang;

import io.vavr.collection.Array;
import io.vavr.collection.List;
import io.vavr.collection.Seq;

/**
 * 函数定义中，如果有参数收集（最后一个参数），则使用该类。实现对应的doInvoke方法，以及实现getRequiredArity方法。
 * defn f(a,b,...r)(
 * ...
 * );
 * 函数调用时，有两种可能。
 * 1. 不包含拓展运算符，则调用invoke。
 * 2. 包含拓展运算符，则收集参数到Seq，然后调用applyTo。
 * 它们都会在处理参数后调用invokeTo。
 *
 * 对于invoke，
 *
 *
 *
 * */
public abstract class RestFn extends Fn {
    abstract public int getRequiredArity();

    protected Object doInvoke(Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object arg9, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object arg9, Object arg10, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object arg9, Object arg10, Object arg11, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13,
                              Object arg14, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13,
                              Object arg14, Object arg15, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13,
                              Object arg14, Object arg15, Object arg16, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13,
                              Object arg14, Object arg15, Object arg16, Object arg17, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13,
                              Object arg14, Object arg15, Object arg16, Object arg17, Object arg18, Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13,
                              Object arg14, Object arg15, Object arg16, Object arg17, Object arg18, Object arg19,
                              Object args) {
        return null;
    }

    protected Object doInvoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                              Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13,
                              Object arg14, Object arg15, Object arg16, Object arg17, Object arg18, Object arg19,
                              Object arg20, Object args) {
        return null;
    }


    public Object applyTo(Seq args) {
        if (RT.boundedLength(args, getRequiredArity()) <= getRequiredArity()) {
            return Fn.applyToHelper(this, Util.ret1(args, args = null));
        }
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Util.ret1(args, args = null));
            case 1:
                return doInvoke(args.head()
                        , Util.ret1(args.tail(), args = null));
            case 2:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 3:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 4:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 5:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 6:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 7:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 8:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 9:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 10:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 11:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 12:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 13:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 14:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 15:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 16:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 17:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 18:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 19:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));
            case 20:
                return doInvoke(args.head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , (args = args.tail()).head()
                        , Util.ret1(args.tail(), args = null));

        }
        return throwArity(-1);
    }

    public Object invoke() {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(null);
            default:
                return throwArity(0);
        }

    }

    public Object invoke(Object arg1) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(Util.ret1(arg1, arg1 = null)));
            case 1:
                return doInvoke(Util.ret1(arg1, arg1 = null), null);
            default:
                return throwArity(1);
        }

    }

    public Object invoke(Object arg1, Object arg2) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null)));
            case 1:
                return doInvoke(Util.ret1(arg1, arg1 = null), Array.of(Util.ret1(arg2, arg2 = null)));
            case 2:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), null);
            default:
                return throwArity(2);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null)));
            case 1:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Array.of(Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null)));
            case 2:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null),
                        Array.of(Util.ret1(arg3, arg3 = null)));
            case 3:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        null);
            default:
                return throwArity(3);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Util.ret1(arg4, arg4 = null)));
            case 1:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Array.of(Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null)));
            case 2:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null),
                        Array.of(Util.ret1(arg3, arg3 = null), Util.ret1(arg4, arg4 = null)));
            case 3:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Array.of(Util.ret1(arg4, arg4 = null)));
            case 4:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), null);
            default:
                return throwArity(4);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null)));
            case 1:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Array.of(Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null)));
            case 2:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null),
                        Array.of(Util.ret1(arg3, arg3 = null), Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null)));
            case 3:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Array.of(Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null)));
            case 4:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Array.of(Util.ret1(arg5, arg5 = null)));
            case 5:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), null);
            default:
                return throwArity(5);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null)));
            case 1:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Array.of(Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null)));
            case 2:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null),
                        Array.of(Util.ret1(arg3, arg3 = null), Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null)));
            case 3:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Array.of(Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null)));
            case 4:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Array.of(Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null)));
            case 5:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null),
                        Array.of(Util.ret1(arg6, arg6 = null)));
            case 6:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        null);
            default:
                return throwArity(6);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null)));
            case 1:
                return doInvoke(Util.ret1(arg1, arg1 = null), Array.of(Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null)));
            case 2:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null)));
            case 3:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Array.of(Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null)));
            case 4:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Array.of(Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null)));
            case 5:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null), Array.of(Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null)));
            case 6:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null), Array.of(Util.ret1(arg7, arg7 = null)));
            case 7:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), null);
            default:
                return throwArity(7);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null)));
            case 1:
                return doInvoke(Util.ret1(arg1, arg1 = null), Array.of(Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null)));
            case 2:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null)));
            case 3:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Array.of(Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null)));
            case 4:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Array.of(Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null)));
            case 5:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null), Array.of(Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null)));
            case 6:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null), Array.of(Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null)));
            case 7:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Array.of(Util.ret1(arg8, arg8 = null)));
            case 8:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null), null);
            default:
                return throwArity(8);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null)));
            case 1:
                return doInvoke(Util.ret1(arg1, arg1 = null), Array.of(Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null)));
            case 2:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null)));
            case 3:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Array.of(Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null)));
            case 4:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Array.of(Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null)));
            case 5:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null), Array.of(Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null)));
            case 6:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null), Array.of(Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null)));
            case 7:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Array.of(Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null)));
            case 8:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null), Array.of(Util.ret1(arg9, arg9 = null)));
            case 9:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null), null);
            default:
                return throwArity(9);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9, Object arg10) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null)));
            case 1:
                return doInvoke(Util.ret1(arg1, arg1 = null), Array.of(Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null)));
            case 2:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null)));
            case 3:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Array.of(Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null)));
            case 4:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Array.of(Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null)));
            case 5:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null), Array.of(Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null)));
            case 6:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null), Array.of(Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null)));
            case 7:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Array.of(Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null)));
            case 8:
                return doInvoke(Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null), Array.of(Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null)));
            case 9:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null), Array.of(
                                Util.ret1(arg10, arg10 = null)));
            case 10:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), null);
            default:
                return throwArity(10);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9, Object arg10, Object arg11) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null)));
            case 1:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Array.of(
                                Util.ret1(arg2, arg2 = null),
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null)));
            case 2:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null)));
            case 3:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Array.of(
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null)));
            case 4:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Array.of(
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null)));
            case 5:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null), Array.of(
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null)));
            case 6:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null), Array.of(
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null)));
            case 7:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Array.of(
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null)));
            case 8:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null), Array.of(
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null)));
            case 9:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null), Array.of(
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null)));
            case 10:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Array.of(
                                Util.ret1(arg11, arg11 = null)));
            case 11:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null), null);
            default:
                return throwArity(11);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9, Object arg10, Object arg11, Object arg12) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null)));
            case 1:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Array.of(
                                Util.ret1(arg2, arg2 = null),
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null)));
            case 2:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null)));
            case 3:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Array.of(
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null)));
            case 4:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Array.of(
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null)));
            case 5:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null), Array.of(
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null)));
            case 6:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null), Array.of(
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null)));
            case 7:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Array.of(
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null)));
            case 8:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null), Array.of(
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null)));
            case 9:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null), Array.of(
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null)));
            case 10:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Array.of(
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null)));
            case 11:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null), Array.of(
                                Util.ret1(arg12, arg12 = null)));
            case 12:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null), null);
            default:
                return throwArity(12);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(
                        Array.of(
                                Util.ret1(arg1, arg1 = null),
                                Util.ret1(arg2, arg2 = null),
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null)));
            case 1:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Array.of(
                                Util.ret1(arg2, arg2 = null),
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null)));
            case 2:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Array.of(
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null)));
            case 3:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Array.of(
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null)));
            case 4:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Array.of(
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null)));
            case 5:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Array.of(
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null)));
            case 6:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Array.of(
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null)));
            case 7:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Array.of(
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null)));
            case 8:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Array.of(
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null)));
            case 9:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Array.of(
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null)));
            case 10:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Array.of(
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null)));
            case 11:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Array.of(
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null)));
            case 12:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Array.of(
                                Util.ret1(arg13, arg13 = null)));
            case 13:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), null);
            default:
                return throwArity(13);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),

                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null)));
            case 1:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Array.of(
                                Util.ret1(arg2, arg2 = null),
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null)));
            case 2:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null)));
            case 3:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Array.of(
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null)));
            case 4:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Array.of(
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null)));
            case 5:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Array.of(
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null)));
            case 6:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Array.of(
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null)));
            case 7:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Array.of(
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null)));
            case 8:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Array.of(
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null)));
            case 9:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Array.of(
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null)));
            case 10:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Array.of(
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null)));
            case 11:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Array.of(
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null)));
            case 12:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Array.of(
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null)));
            case 13:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Array.of(
                                Util.ret1(arg14, arg14 = null)));
            case 14:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),
                        null);
            default:
                return throwArity(14);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14,
                         Object arg15) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),

                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),
                        Util.ret1(arg15, arg15 = null)));
            case 1:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Array.of(
                                Util.ret1(arg2, arg2 = null),
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 2:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 3:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Array.of(
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 4:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Array.of(
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 5:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Array.of(
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 6:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Array.of(
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 7:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Array.of(
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 8:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Array.of(
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 9:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Array.of(
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 10:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Array.of(
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 11:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Array.of(
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 12:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Array.of(
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 13:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Array.of(
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null)));
            case 14:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),
                        Array.of(
                                Util.ret1(arg15, arg15 = null)));
            case 15:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null), null);
            default:
                return throwArity(15);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14,
                         Object arg15, Object arg16) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),

                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),
                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null)));
            case 1:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Array.of(
                                Util.ret1(arg2, arg2 = null),
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 2:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 3:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Array.of(
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 4:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Array.of(
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 5:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Array.of(
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 6:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Array.of(
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 7:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Array.of(
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 8:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Array.of(
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 9:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Array.of(
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 10:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Array.of(
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 11:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Array.of(
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 12:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Array.of(
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 13:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Array.of(
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 14:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),
                        Array.of(
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null)));
            case 15:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null), Array.of(
                                Util.ret1(arg16, arg16 = null)));
            case 16:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), null);
            default:
                return throwArity(16);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14,
                         Object arg15, Object arg16, Object arg17) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),

                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),
                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null),
                        Util.ret1(arg17, arg17 = null)));
            case 1:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Array.of(
                                Util.ret1(arg2, arg2 = null),
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 2:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 3:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Array.of(
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 4:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Array.of(
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 5:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null), Array.of(
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 6:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Array.of(
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 7:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Array.of(
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 8:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Array.of(
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 9:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Array.of(
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 10:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Array.of(
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 11:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Array.of(
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 12:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Array.of(
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 13:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Array.of(
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 14:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),
                        Array.of(
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 15:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null), Array.of(
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null)));
            case 16:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Array.of(
                                Util.ret1(arg17, arg17 = null)));
            case 17:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null),
                        Util.ret1(arg17, arg17 = null), null);
            default:
                return throwArity(17);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14,
                         Object arg15, Object arg16, Object arg17, Object arg18) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),

                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),
                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null),
                        Util.ret1(arg17, arg17 = null),
                        Util.ret1(arg18, arg18 = null)));
            case 1:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Array.of(
                                Util.ret1(arg2, arg2 = null),
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 2:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 3:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Array.of(
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 4:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Array.of(
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 5:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null), Array.of(
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 6:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null), Array.of(
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),

                                Util.ret1(arg18, arg18 = null)));
            case 7:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Array.of(
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 8:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Array.of(
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 9:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Array.of(
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 10:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Array.of(
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 11:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Array.of(
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 12:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Array.of(
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 13:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Array.of(
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 14:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),
                        Array.of(
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 15:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null), Array.of(
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 16:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Array.of(
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null)));
            case 17:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null),
                        Util.ret1(arg17, arg17 = null), Array.of(
                                Util.ret1(arg18, arg18 = null)));
            case 18:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null),
                        Util.ret1(arg17, arg17 = null),
                        Util.ret1(arg18, arg18 = null), null);
            default:
                return throwArity(18);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14,
                         Object arg15, Object arg16, Object arg17, Object arg18, Object arg19) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),

                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),
                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null),
                        Util.ret1(arg17, arg17 = null),
                        Util.ret1(arg18, arg18 = null),
                        Util.ret1(arg19, arg19 = null)));
            case 1:
                Seq packed = List.empty();
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Array.of(Util.ret1(arg2, arg2 = null),
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 2:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 3:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null), Array.of(
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 4:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Array.of(
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 5:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null), Array.of(
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),

                                Util.ret1(arg19, arg19 = null)));
            case 6:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null), Array.of(
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),

                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 7:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Array.of(
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),

                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 8:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null), Array.of(
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),

                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 9:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Array.of(
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 10:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Array.of(
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 11:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Array.of(
                                Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 12:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Array.of(
                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 13:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Array.of(
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 14:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),
                        Array.of(
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 15:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null), Array.of(
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 16:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Array.of(
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 17:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null),
                        Util.ret1(arg17, arg17 = null), Array.of(
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null)));
            case 18:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null),
                        Util.ret1(arg17, arg17 = null),
                        Util.ret1(arg18, arg18 = null), Array.of(
                                Util.ret1(arg19, arg19 = null)));
            case 19:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),

                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null),
                        Util.ret1(arg17, arg17 = null),
                        Util.ret1(arg18, arg18 = null),
                        Util.ret1(arg19, arg19 = null), null);
            default:
                return throwArity(19);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14,
                         Object arg15, Object arg16, Object arg17, Object arg18, Object arg19, Object arg20) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(Array.of(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null),
                        Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null),
                        Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null),
                        Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null),
                        Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null),

                        Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null),
                        Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null),
                        Util.ret1(arg17, arg17 = null),
                        Util.ret1(arg18, arg18 = null),
                        Util.ret1(arg19, arg19 = null),
                        Util.ret1(arg20, arg20 = null)));
            case 1:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Array.of(
                                Util.ret1(arg2, arg2 = null),
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 2:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null),
                        Util.ret1(arg2, arg2 = null), Array.of(
                                Util.ret1(arg3, arg3 = null),
                                Util.ret1(arg4, arg4 = null),
                                Util.ret1(arg5, arg5 = null),
                                Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null),
                                Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null),

                                Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 3:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Array.of(Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 4:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Array.of(Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null), Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null), Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null), Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null), Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 5:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Array.of(Util.ret1(arg6, arg6 = null), Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null), Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 6:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Array.of(Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 7:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Array.of(Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null), Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 8:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Array.of(Util.ret1(arg9, arg9 = null), Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null), Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 9:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Array.of(Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 10:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Array.of(Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null), Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 11:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Array.of(Util.ret1(arg12, arg12 = null), Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 12:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Array.of(Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 13:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Array.of(Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 14:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Array.of(Util.ret1(arg15, arg15 = null), Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 15:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Array.of(Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 16:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Array.of(Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 17:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Array.of(Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 18:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                        Array.of(Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 19:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                        Util.ret1(arg19, arg19 = null), Array.of(Util.ret1(arg20, arg20 = null)));
            case 20:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                        Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null), null);
            default:
                return throwArity(20);
        }

    }

    public Object invoke(Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7,
                         Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14,
                         Object arg15, Object arg16, Object arg17, Object arg18, Object arg19, Object arg20, Object... args) {
        switch (getRequiredArity()) {
            case 0:
                return doInvoke(ontoArrayPrepend(args, Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                        Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 1:
                return doInvoke(Util.ret1(arg1, arg1 = null), ontoArrayPrepend(args, Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null), Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null), Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null),
                        Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                        Util.ret1(arg20, arg20 = null)));
            case 2:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), ontoArrayPrepend(args, Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null),
                        Util.ret1(arg6, arg6 = null), Util.ret1(arg7, arg7 = null),
                        Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null),
                        Util.ret1(arg12, arg12 = null), Util.ret1(arg13, arg13 = null),
                        Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null),
                        Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                        Util.ret1(arg20, arg20 = null)));
            case 3:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        ontoArrayPrepend(args, Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                                Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 4:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), ontoArrayPrepend(args, Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null), Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null), Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null), Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null), Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 5:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), ontoArrayPrepend(args, Util.ret1(arg6, arg6 = null), Util.ret1(arg7, arg7 = null),
                                Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null), Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 6:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        ontoArrayPrepend(args, Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 7:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), ontoArrayPrepend(args, Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                                Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null),
                                Util.ret1(arg12, arg12 = null), Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 8:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), ontoArrayPrepend(args, Util.ret1(arg9, arg9 = null), Util.ret1(arg10, arg10 = null),
                                Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null), Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 9:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        ontoArrayPrepend(args, Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 10:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), ontoArrayPrepend(args, Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                                Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null),
                                Util.ret1(arg15, arg15 = null), Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 11:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), ontoArrayPrepend(args, Util.ret1(arg12, arg12 = null), Util.ret1(arg13, arg13 = null),
                                Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 12:
                return doInvoke(
                        Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        ontoArrayPrepend(args, Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 13:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), ontoArrayPrepend(args, Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                                Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null),
                                Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 14:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), ontoArrayPrepend(args, Util.ret1(arg15, arg15 = null), Util.ret1(arg16, arg16 = null),
                                Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 15:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        ontoArrayPrepend(args, Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 16:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), ontoArrayPrepend(args, Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                                Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 17:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), ontoArrayPrepend(args, Util.ret1(arg18, arg18 = null), Util.ret1(arg19, arg19 = null),
                                Util.ret1(arg20, arg20 = null)));
            case 18:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                        ontoArrayPrepend(args, Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null)));
            case 19:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                        Util.ret1(arg19, arg19 = null), ontoArrayPrepend(args, Util.ret1(arg20, arg20 = null)));
            case 20:
                return doInvoke(Util.ret1(arg1, arg1 = null), Util.ret1(arg2, arg2 = null), Util.ret1(arg3, arg3 = null),
                        Util.ret1(arg4, arg4 = null), Util.ret1(arg5, arg5 = null), Util.ret1(arg6, arg6 = null),
                        Util.ret1(arg7, arg7 = null), Util.ret1(arg8, arg8 = null), Util.ret1(arg9, arg9 = null),
                        Util.ret1(arg10, arg10 = null), Util.ret1(arg11, arg11 = null), Util.ret1(arg12, arg12 = null),
                        Util.ret1(arg13, arg13 = null), Util.ret1(arg14, arg14 = null), Util.ret1(arg15, arg15 = null),
                        Util.ret1(arg16, arg16 = null), Util.ret1(arg17, arg17 = null), Util.ret1(arg18, arg18 = null),
                        Util.ret1(arg19, arg19 = null), Util.ret1(arg20, arg20 = null), Array.of(args));
            default:
                return throwArity(21);
        }

    }

    protected static Seq ontoArrayPrepend(Object[] array, Object... args) {
        Object[] arr = new Object[array.length+args.length];
        System.arraycopy(args,0,arr,0,args.length);
        System.arraycopy(array,0,arr,args.length,array.length);
        return Array.of(arr);
    }

    public static void main(String[] args) {
        System.out.println(ontoArrayPrepend(new Object[]{3,4},1,2));
    }
}
