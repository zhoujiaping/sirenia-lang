package sirenia.lang;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;

public class Frame {
    protected static final Frame TOP = new Frame(null);
    protected Map<Var,Object> vars = HashMap.empty();
    protected Frame prev;

    protected Frame(Frame prev) {
        this.prev = prev;
    }

    protected Frame(Map vars, Frame prev) {
        this.vars = vars;
        this.prev = prev;
    }
}