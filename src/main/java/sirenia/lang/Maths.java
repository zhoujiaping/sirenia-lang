package sirenia.lang;

import com.github.kiprobinson.bigfraction.BigFraction;
import io.vavr.collection.Vector;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

public class Maths {
    //private static final BigInteger BI_LONG_MAX = BigInteger.valueOf(Long.MAX_VALUE);
    //private static final BigInteger BI_LONG_MIN = BigInteger.valueOf(Long.MIN_VALUE);

    /**
     * 从Long.parseLong拷贝过来的，稍作了修改，对超出范围的情况返回BigInteger，而不是抛异常。
     */
    private static Number parseJavaLong(CharSequence s, int beginIndex, int endIndex, int radix)
            throws NumberFormatException {
        Objects.requireNonNull(s);
        Objects.checkFromToIndex(beginIndex, endIndex, s.length());

        if (radix < Character.MIN_RADIX) {
            throw new NumberFormatException("radix " + radix +
                    " less than Character.MIN_RADIX");
        }
        if (radix > Character.MAX_RADIX) {
            throw new NumberFormatException("radix " + radix +
                    " greater than Character.MAX_RADIX");
        }

        boolean negative = false;
        int i = beginIndex;
        long limit = -Long.MAX_VALUE;

        if (i < endIndex) {
            char firstChar = s.charAt(i);
            if (firstChar < '0') { // Possible leading "+" or "-"
                if (firstChar == '-') {
                    negative = true;
                    limit = Long.MIN_VALUE;
                } else if (firstChar != '+') {
                    throw forCharSequence(s, beginIndex,
                            endIndex, i);
                }
                i++;
            }
            if (i >= endIndex) { // Cannot have lone "+", "-" or ""
                throw forCharSequence(s, beginIndex,
                        endIndex, i);
            }
            long multmin = limit / radix;
            long result = 0;
            while (i < endIndex) {
                // Accumulating negatively avoids surprises near MAX_VALUE
                int digit = Character.digit(s.charAt(i), radix);
                if (digit < 0 || result < multmin) {
                    return new BigInteger(s.subSequence(beginIndex, endIndex).toString(), radix);
//                    throw forCharSequence(s, beginIndex,
//                            endIndex, i);
                }
                result *= radix;
                if (result < limit + digit) {
                    throw forCharSequence(s, beginIndex,
                            endIndex, i);
                }
                i++;
                result -= digit;
            }
            return negative ? result : -result;
        } else {
            throw new NumberFormatException("");
        }
    }

    static NumberFormatException forCharSequence(CharSequence s,
                                                 int beginIndex, int endIndex, int errorIndex) {
        return new NumberFormatException("Error at index "
                + (errorIndex - beginIndex) + " in: \""
                + s.subSequence(beginIndex, endIndex) + "\"");
    }

    private static Number parseJavaLong(CharSequence text, int radix) {
        return parseJavaLong(text, 0, text.length(), radix);
    }

    public static Number parseJavaLong(String text) {
        if (text.startsWith("0x") || text.startsWith("0X")) {
            return parseJavaLong(text, "0x".length(), text.length(), 16);
        } else if (text.contains("r") || text.contains("R")) {
            int rIdx = text.indexOf('r');
            if (rIdx < 0) {
                rIdx = text.indexOf('R');
            }
            Long radix = (Long) parseJavaLong(text, 0, rIdx, 10);
            int nIdx = text.indexOf('N');
            if (nIdx < 0) {
                return parseJavaLong(text, rIdx + 1, text.length(), radix.intValue());
            } else {
                return new BigInteger(text.substring(rIdx + 1, text.length() - 1), radix.intValue());
            }
        } else {
            return parseJavaLong(text, 10);
        }
    }

    private static final int RATIO_MASK = 1 << 4;
    private static final int DECIMAL_MASK = 1 << 3;
    private static final int BIGINT_MASK = 1 << 2;
    private static final int DOUBLE_MASK = 1 << 1;
    private static final int LONG_MASK = 1 << 0;
    private static final int INT_MASK = 1 << 5;
    private static final Map<Class, Integer> MASKS = new HashMap<>();

    static {
        MASKS.put(BigFraction.class, RATIO_MASK);
        MASKS.put(BigDecimal.class, DECIMAL_MASK);
        MASKS.put(BigInteger.class, BIGINT_MASK);
        MASKS.put(Double.class, DOUBLE_MASK);
        MASKS.put(Long.class, LONG_MASK);
        MASKS.put(Integer.class, INT_MASK);
    }

    public static Number parseJavaDouble(String text) {
        if (text.endsWith("M")) {
            char[] chs = text.toCharArray();
            return new BigDecimal(chs, 0, chs.length - 1);
        } else {
            return Double.parseDouble(text);
        }
    }

    public static Number bitNeg(Number x) {
        if (x instanceof Long it)
            return bitNeg(it);
        else if (x instanceof BigInteger it)
            return bitNeg(it);
        throw new IllegalArgumentException();
    }

    public static Number bitNeg(Long x) {
        return ~x;
    }

    public static Number bitNeg(BigInteger x) {
        return x.not();
    }

    public static Number pos(Number x) {
        return x;
    }

    public static Number neg(Number x) {
        if (x instanceof Long it)
            return neg(it);
        else if (x instanceof BigInteger it)
            return neg(it);
        else if (x instanceof Double it)
            return neg(it);
        else if (x instanceof BigDecimal it)
            return neg(it);
        else if (x instanceof BigFraction it)
            return neg(it);
        throw new IllegalArgumentException();
    }

    public static Number neg(Long x) {
        if (x == Long.MIN_VALUE) {
            return neg(BigInteger.valueOf(x));
        }
        return -x;
    }

    public static Number neg(BigInteger x) {
        return x.negate();
    }

    public static Number neg(Double x) {
        return -x;
    }

    public static Number neg(BigDecimal x) {
        return x.negate();
    }

    public static Number neg(BigFraction x) {
        return x.negate();
    }


    private static Integer intValueExactlyOrNull(Number x) {
        if(x instanceof Integer it)
            return it;
        else if (x instanceof Long it) {
            if ((long) it.intValue() == it)
                return it.intValue();
            else
                return null;
        } else if (x instanceof Double it) {
            if ((double) it.intValue() == it)
                return it.intValue();
            else
                return null;
        } else if (x instanceof BigInteger it) {
            try {
                return it.intValueExact();
            } catch (Exception _e) {
                return null;
            }
        } else if (x instanceof BigDecimal it) {
            try {
                return it.intValueExact();
            } catch (Exception _e) {
                return null;
            }
        } else if (x instanceof BigFraction it) {
            try {
                return it.intValueExact();
            } catch (Exception _e) {
                return null;
            }
        }
        throw new IllegalArgumentException();
    }

    private static Long longValueExactlyOrNull(Number x) {
        if (x instanceof Long it) {
            return it;
        } else if (x instanceof Double it) {
            if ((double) it.longValue() == it)
                return it.longValue();
            else
                return null;
        } else if (x instanceof BigInteger it) {
            try {
                return it.longValueExact();
            } catch (Exception _e) {
                return null;
            }
        } else if (x instanceof BigDecimal it) {
            try {
                return it.longValueExact();
            } catch (Exception _e) {
                return null;
            }
        } else if (x instanceof BigFraction it) {
            try {
                return it.longValueExact();
            } catch (Exception _e) {
                return null;
            }
        }
        throw new IllegalArgumentException();
    }

    private static Double doubleValueExactlyOrNull(Number x) {
        if (x instanceof Long it) {
            return it.doubleValue();
        } else if (x instanceof Double it) {
            return it;
        } else if (x instanceof BigInteger it) {
            Double d = it.doubleValue();
            if (it.equals(BigInteger.valueOf(d.longValue())))// double如果没有小数部分，那么其整数部分肯定在long的范围内
                return d;
            else
                return null;
        } else if (x instanceof BigDecimal it) {
            Double d = it.doubleValue();
            if (it.equals(new BigDecimal(d.toString())))
                return d;
            else
                return null;
        } else if (x instanceof BigFraction it) {
            try {
                return it.doubleValueExact();
            } catch (Exception _e) {
                return null;
            }
        }
        throw new IllegalArgumentException();
    }

    public static Number pow(Number x, Number y) {
        //指数部分，只支持int/double，所以能精确转成int，就调用pow(x,int),否则调用Math.pow(double,double)
        Integer exp = intValueExactlyOrNull(y);
        if (exp == null) {
            //结果能精确转成long就返回long，否则返回double
            double r = Math.pow(x.doubleValue(), y.doubleValue());
            return down(r);
        } else {//pow(x,int)
            if (x instanceof Long base)
                return pow(BigInteger.valueOf(base), exp);
            else if (x instanceof BigInteger base)
                return down(base.pow(exp));
            else if (x instanceof Double base) {
                return pow(new BigDecimal(base.toString()), exp);
            } else if (x instanceof BigDecimal base) {
                return down(base.pow(exp));
            } else if (x instanceof BigFraction base) {
                return down(base.pow(exp));
            }
            throw new IllegalArgumentException();
        }
    }

    /**
     * 对参数，非必要不调用down。
     * 对返回值，都调用down。
     */
    private static Number down(Number x) {
        if (x instanceof Long it) return it;
        if (x instanceof Double it) return down(it);
        if (x instanceof BigInteger it) return down(it);
        if (x instanceof BigDecimal it) return down(it);
        if (x instanceof BigFraction it) return down(it);
        throw new IllegalArgumentException();
    }

    private static Number down(Double x) {
        Number r = longValueExactlyOrNull(x);
        if (r != null) return r;
        return x;
    }

    private static Number down(BigInteger x) {
        Number r = longValueExactlyOrNull(x);
        if (r != null) return r;
        return x;
    }

    private static Number down(BigDecimal x) {
        Number r = longValueExactlyOrNull(x);
        if (r != null) return r;
        r = doubleValueExactlyOrNull(x);
        if (r != null) return r;
        return x;
    }

    private static Number down(BigFraction x) {//分数就不转BigDecimal/BigInteger了
        Number r = longValueExactlyOrNull(x);
        if (r != null) return r;
        r = doubleValueExactlyOrNull(x);
        if (r != null) return r;
        return x;
    }

    private static BigInteger bigint(Number intOrBigInt) {
        if (intOrBigInt instanceof BigInteger it)
            return it;
        if (intOrBigInt instanceof Long it)
            return BigInteger.valueOf(it);
        throw new IllegalArgumentException();
    }

    public static Number mul(Number x, Number y) {
        int mask = MASKS.get(x.getClass()) | MASKS.get(y.getClass());
        //如果包含BigFraction，则使用BigFraction计算。
        //否则，如果都是Long/BigInteger，则使用BigInteger计算。
        //否则，使用BigDecimal计算
        if (containsRatio(mask))
            return down(BigFraction.valueOf(x).multiply(BigFraction.valueOf(y)));
        else if (onlyContainsLongOrBigInt(mask))
            return down(bigint(x).multiply(bigint(y)));
        else
            return down(bigdecimal(x).multiply(bigdecimal(y)));
    }

    private static boolean containsRatio(int mask) {
        return (mask & RATIO_MASK) != 0;
    }

    private static boolean onlyContainsLongOrBigInt(int mask) {
        int revMask = ~(LONG_MASK | BIGINT_MASK);
        return (mask & revMask) == 0;
    }

    private static BigDecimal bigdecimal(Number x) {
        if(x instanceof Integer it)
            return BigDecimal.valueOf(it);
        if (x instanceof Long it)
            return BigDecimal.valueOf(it);
        if (x instanceof BigDecimal it)
            return it;
        if (x instanceof Double it)
            return new BigDecimal(it.toString());
        if (x instanceof BigInteger it)
            return new BigDecimal(it.toString());
        throw new IllegalArgumentException();
    }


    public static Number div(Number x, Number y) {
        return down(BigFraction.valueOf(x).divide(BigFraction.valueOf(y)));
    }

    public static Number rem(Number x, Number y) {
        // 求余只能是整数对整数求余
        return down(bigint(down(x)).remainder(bigint(down(y))));
    }


    public static Number add(Number x, Number y) {
        int mask = MASKS.get(x.getClass()) | MASKS.get(y.getClass());
        if (containsRatio(mask))
            return down(BigFraction.valueOf(x).add(BigFraction.valueOf(y)));
        else if (onlyContainsLongOrBigInt(mask))
            return down(bigint(x).add(bigint(y)));
        else
            return down(bigdecimal(x).add(bigdecimal(y)));
    }

    public static Number sub(Number x, Number y) {
        int mask = MASKS.get(x.getClass()) | MASKS.get(y.getClass());
        if (containsRatio(mask))
            return down(BigFraction.valueOf(x).subtract(BigFraction.valueOf(y)));
        else if (onlyContainsLongOrBigInt(mask))
            return down(bigint(x).subtract(bigint(y)));
        else
            return down(bigdecimal(x).subtract(bigdecimal(y)));
    }

    public static Number shiftLeft(Number x, Number y) {
        Integer n = intValueExactlyOrNull(y);
        if (n == null)
            throw new IllegalArgumentException();
        //只有整数可以左移
        return down(bigint(down(x)).shiftLeft(n));
    }

    public static Number shiftRight(Number x, Number y) {
        Integer n = intValueExactlyOrNull(y);
        if (n == null)
            throw new IllegalArgumentException();
        return down(bigint(down(x)).shiftRight(n));
    }

    public static Number shiftRightUnsigned(Number x, Number y) {
        Integer n = intValueExactlyOrNull(y);
        if (n == null)
            throw new IllegalArgumentException();
        Long a = longValueExactlyOrNull(x);
        if (a == null)
            throw new IllegalArgumentException();
        return a >>> n;
    }

    public static boolean lt(Number x, Number y) {
        return compare(x, y) < 0;
    }

    private static int compare(Number x, Number y) {
        int mask = MASKS.get(x.getClass()) | MASKS.get(y.getClass());
        if (containsRatio(mask)) {
            return BigFraction.valueOf(x).compareTo(BigFraction.valueOf(y));
        }
        if (onlyContainsLongOrBigInt(mask)) {
            return bigint(x).compareTo(bigint(y));
        }
        return bigdecimal(x).compareTo(bigdecimal(y));
    }

    public static boolean ltEq(Number x, Number y) {
        return compare(x, y) <= 0;
    }

    public static boolean gt(Number x, Number y) {
        return compare(x, y) > 0;
    }

    public static boolean gtEq(Number x, Number y) {
        return compare(x, y) >= 0;
    }

    public static boolean eq(Number x, Number y) {
        return compare(x, y) == 0;
    }

    public static Number bitAnd(Number x, Number y) {
        return down(bigint(x).and(bigint(y)));
    }

    public static Number bitXor(Number x, Number y) {
        return down(bigint(x).xor(bigint(y)));
    }

    public static Number bitOr(Number x, Number y) {
        return down(bigint(x).or(bigint(y)));
    }

    public static Vector<Number> range(Number from, Number to) {
        return range(from, to, ltEq(from, to) ? 1L : -1L);
    }

    //TODO 应该返回一个惰性序列

    /**
     * 1..3 step=1   numCmp<0 stepCmp>0
     * 3..1 step=-1  numCmp>0 stepCmp<0
     */
    public static Vector<Number> range(Number from, Number to, Number step) {
        int stepCmp = compare(step, 0L);
        if (stepCmp == 0)
            throw new IllegalArgumentException();
        int numCmp = compare(from, to);
        if (numCmp == 0)
            return Vector.empty();
        else if (numCmp < 0 && stepCmp > 0)
            return Vector.ofAll(
                    Stream.iterate(down(from), it -> add(it, step))
                            .takeWhile(it -> lt(it, to))
            );
        else if (numCmp > 0 && stepCmp < 0)
            return Vector.ofAll(
                    Stream.iterate(down(from), it -> add(it, step))
                            .takeWhile(it -> gt(it, to))
            );
        else
            throw new IllegalArgumentException();
    }

    public static Vector<Number> rangeTo(Number from, Number to) {
        return rangeTo(from, to, ltEq(from, to) ? 1L : -1L);
    }

    public static Vector<Number> rangeTo(Number from, Number to, Number step) {
        int stepCmp = compare(step, 0L);
        if (stepCmp == 0)
            throw new IllegalArgumentException();
        int numCmp = compare(from, to);
        if (numCmp == 0)
            return Vector.of(down(from));
        else if (numCmp < 0 && stepCmp > 0)
            return Vector.ofAll(
                    Stream.iterate(down(from), it -> add(it, step))
                            .takeWhile(it -> ltEq(it, to))
            );
        else if (numCmp > 0 && stepCmp < 0)
            return Vector.ofAll(
                    Stream.iterate(down(from), it -> add(it, step))
                            .takeWhile(it -> gtEq(it, to))
            );
        else
            throw new IllegalArgumentException();
    }

    /**
     * java的Long/BigDecimal/Double/BigInteger，以及依赖库中的BigFraction，其hashcode实现并不遵循：
     *   如果两个值数学意义上相等，则其hashcode相等。
     * 对于Number，我们的eq函数（==运算符）语义上是数学意义上相等。
     * 所以必须提供对应的hash函数。
     *
     * 实现上，偷点懒，不管性能，全部转成对应分数形式，再取hashcode。
     */
    public static int hash(Number x){
        return BigFraction.valueOf(x).hashCode();
    }
}
