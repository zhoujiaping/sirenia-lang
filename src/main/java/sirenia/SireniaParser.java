// Generated from E:/gitlab-repo/sirenia-lang/src/main/resources/Sirenia.g4 by ANTLR 4.13.1
package sirenia;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue"})
public class SireniaParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.13.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, INVOKE_TYPE=30, COND_SEP=31, 
		NOT=32, BIT_NEG=33, EXPAND=34, POW=35, MUL=36, DIV=37, REM=38, ADD=39, 
		SUB=40, SHIFT_LEFT=41, SHIFT_RIGHT=42, SHIFT_RIGHT_UNSIGNED=43, LT=44, 
		LT_EQ=45, GT=46, GT_EQ=47, EQ=48, NOT_EQ=49, BIT_AND=50, BIT_XOR=51, BIT_OR=52, 
		AND=53, OR=54, RANGE=55, RANGE_TO=56, TMPL_STRING=57, CHAR=58, STRING=59, 
		INT=60, FLOAT=61, ID=62, LPAREN=63, RPAREN=64, LBRACK=65, RBRACK=66, LBRACE=67, 
		RBRACE=68, NEWLINE=69, SEMI=70, WS=71, LINE_COMMENT=72, COMMENT=73;
	public static final int
		RULE_file = 0, RULE_exprs = 1, RULE_expr = 2, RULE_categoryExpr = 3, RULE_maybeExpandExpr = 4, 
		RULE_javaInterOpExpr = 5, RULE_typedExpr = 6, RULE_defItem = 7, RULE_defnExpr = 8, 
		RULE_fnParams = 9, RULE_paramsD = 10, RULE_throwExpr = 11, RULE_tryCatchExpr = 12, 
		RULE_tryBlock = 13, RULE_catchBlock = 14, RULE_finallyBlock = 15, RULE_ifExpr = 16, 
		RULE_condExpr = 17, RULE_loopExpr = 18, RULE_loopParam = 19, RULE_recurExpr = 20, 
		RULE_literalExpr = 21, RULE_fnLiteral = 22, RULE_kvPair = 23, RULE_structParams = 24, 
		RULE_vecStructParams = 25, RULE_mapStructParams = 26, RULE_structPair = 27, 
		RULE_as = 28;
	private static String[] makeRuleNames() {
		return new String[] {
			"file", "exprs", "expr", "categoryExpr", "maybeExpandExpr", "javaInterOpExpr", 
			"typedExpr", "defItem", "defnExpr", "fnParams", "paramsD", "throwExpr", 
			"tryCatchExpr", "tryBlock", "catchBlock", "finallyBlock", "ifExpr", "condExpr", 
			"loopExpr", "loopParam", "recurExpr", "literalExpr", "fnLiteral", "kvPair", 
			"structParams", "vecStructParams", "mapStructParams", "structPair", "as"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "','", "'.'", "'|>'", "'def'", "'use'", "'#import'", "'as'", "'#new'", 
			"'#'", "'#invoke'", "'#get'", "'#set'", "'='", "'defn'", "'throw'", "'try'", 
			"'catch'", "'finally'", "'if'", "'else'", "'cond'", "'loop'", "'recur'", 
			"'0'", "'true'", "'false'", "'null'", "'=>'", "':'", null, "'->'", "'!'", 
			"'~'", "'...'", "'**'", "'*'", "'/'", "'%'", "'+'", "'-'", "'<<'", "'>>'", 
			"'>>>'", "'<'", "'<='", "'>'", "'>='", "'=='", "'!='", "'&'", "'^'", 
			"'|'", "'&&'", "'||'", "'..<'", "'..'", null, null, null, null, null, 
			null, "'('", "')'", "'['", "']'", "'{'", "'}'", null, "';'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, "INVOKE_TYPE", "COND_SEP", "NOT", 
			"BIT_NEG", "EXPAND", "POW", "MUL", "DIV", "REM", "ADD", "SUB", "SHIFT_LEFT", 
			"SHIFT_RIGHT", "SHIFT_RIGHT_UNSIGNED", "LT", "LT_EQ", "GT", "GT_EQ", 
			"EQ", "NOT_EQ", "BIT_AND", "BIT_XOR", "BIT_OR", "AND", "OR", "RANGE", 
			"RANGE_TO", "TMPL_STRING", "CHAR", "STRING", "INT", "FLOAT", "ID", "LPAREN", 
			"RPAREN", "LBRACK", "RBRACK", "LBRACE", "RBRACE", "NEWLINE", "SEMI", 
			"WS", "LINE_COMMENT", "COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Sirenia.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SireniaParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FileContext extends ParserRuleContext {
		public ExprsContext exprs() {
			return getRuleContext(ExprsContext.class,0);
		}
		public FileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_file; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitFile(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FileContext file() throws RecognitionException {
		FileContext _localctx = new FileContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_file);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58);
			exprs();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ExprsContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> SEMI() { return getTokens(SireniaParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(SireniaParser.SEMI, i);
		}
		public ExprsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exprs; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitExprs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprsContext exprs() throws RecognitionException {
		ExprsContext _localctx = new ExprsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_exprs);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(65);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(60);
					expr(0);
					setState(61);
					match(SEMI);
					}
					} 
				}
				setState(67);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			setState(69);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 4)) & ~0x3f) == 0 && ((1L << (_la - 4)) & -5773614618387767817L) != 0)) {
				{
				setState(68);
				expr(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class LoopContext extends ExprContext {
		public LoopExprContext loopExpr() {
			return getRuleContext(LoopExprContext.class,0);
		}
		public LoopContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitLoop(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class TryCatchContext extends ExprContext {
		public TryCatchExprContext tryCatchExpr() {
			return getRuleContext(TryCatchExprContext.class,0);
		}
		public TryCatchContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitTryCatch(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class CondContext extends ExprContext {
		public CondExprContext condExpr() {
			return getRuleContext(CondExprContext.class,0);
		}
		public CondContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitCond(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class IdAttrExprContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public IdAttrExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitIdAttrExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class DefExprContext extends ExprContext {
		public List<DefItemContext> defItem() {
			return getRuleContexts(DefItemContext.class);
		}
		public DefItemContext defItem(int i) {
			return getRuleContext(DefItemContext.class,i);
		}
		public DefExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitDefExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class DefnContext extends ExprContext {
		public DefnExprContext defnExpr() {
			return getRuleContext(DefnExprContext.class,0);
		}
		public DefnContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitDefn(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class JavaInterOpContext extends ExprContext {
		public JavaInterOpExprContext javaInterOpExpr() {
			return getRuleContext(JavaInterOpExprContext.class,0);
		}
		public JavaInterOpContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitJavaInterOp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class BinExprContext extends ExprContext {
		public Token op;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode MUL() { return getToken(SireniaParser.MUL, 0); }
		public TerminalNode DIV() { return getToken(SireniaParser.DIV, 0); }
		public TerminalNode REM() { return getToken(SireniaParser.REM, 0); }
		public TerminalNode ADD() { return getToken(SireniaParser.ADD, 0); }
		public TerminalNode SUB() { return getToken(SireniaParser.SUB, 0); }
		public TerminalNode SHIFT_LEFT() { return getToken(SireniaParser.SHIFT_LEFT, 0); }
		public TerminalNode SHIFT_RIGHT() { return getToken(SireniaParser.SHIFT_RIGHT, 0); }
		public TerminalNode SHIFT_RIGHT_UNSIGNED() { return getToken(SireniaParser.SHIFT_RIGHT_UNSIGNED, 0); }
		public TerminalNode LT() { return getToken(SireniaParser.LT, 0); }
		public TerminalNode LT_EQ() { return getToken(SireniaParser.LT_EQ, 0); }
		public TerminalNode GT() { return getToken(SireniaParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(SireniaParser.GT_EQ, 0); }
		public TerminalNode EQ() { return getToken(SireniaParser.EQ, 0); }
		public TerminalNode NOT_EQ() { return getToken(SireniaParser.NOT_EQ, 0); }
		public TerminalNode BIT_AND() { return getToken(SireniaParser.BIT_AND, 0); }
		public TerminalNode BIT_XOR() { return getToken(SireniaParser.BIT_XOR, 0); }
		public TerminalNode BIT_OR() { return getToken(SireniaParser.BIT_OR, 0); }
		public TerminalNode AND() { return getToken(SireniaParser.AND, 0); }
		public TerminalNode OR() { return getToken(SireniaParser.OR, 0); }
		public TerminalNode RANGE() { return getToken(SireniaParser.RANGE, 0); }
		public TerminalNode RANGE_TO() { return getToken(SireniaParser.RANGE_TO, 0); }
		public BinExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitBinExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ScopeExprContext extends ExprContext {
		public TerminalNode LBRACE() { return getToken(SireniaParser.LBRACE, 0); }
		public ExprsContext exprs() {
			return getRuleContext(ExprsContext.class,0);
		}
		public TerminalNode RBRACE() { return getToken(SireniaParser.RBRACE, 0); }
		public ScopeExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitScopeExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExprAttrExprContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode LBRACK() { return getToken(SireniaParser.LBRACK, 0); }
		public TerminalNode RBRACK() { return getToken(SireniaParser.RBRACK, 0); }
		public ExprAttrExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitExprAttrExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class IdRefExprContext extends ExprContext {
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public IdRefExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitIdRefExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class PreExprContext extends ExprContext {
		public Token op;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode NOT() { return getToken(SireniaParser.NOT, 0); }
		public TerminalNode BIT_NEG() { return getToken(SireniaParser.BIT_NEG, 0); }
		public TerminalNode ADD() { return getToken(SireniaParser.ADD, 0); }
		public TerminalNode SUB() { return getToken(SireniaParser.SUB, 0); }
		public PreExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitPreExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ThrowContext extends ExprContext {
		public ThrowExprContext throwExpr() {
			return getRuleContext(ThrowExprContext.class,0);
		}
		public ThrowContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitThrow(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class LiteralContext extends ExprContext {
		public LiteralExprContext literalExpr() {
			return getRuleContext(LiteralExprContext.class,0);
		}
		public LiteralContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ParenExprContext extends ExprContext {
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public ExprsContext exprs() {
			return getRuleContext(ExprsContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public ParenExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitParenExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class CategoryContext extends ExprContext {
		public CategoryExprContext categoryExpr() {
			return getRuleContext(CategoryExprContext.class,0);
		}
		public CategoryContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitCategory(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class IfContext extends ExprContext {
		public IfExprContext ifExpr() {
			return getRuleContext(IfExprContext.class,0);
		}
		public IfContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitIf(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class FnCallExprContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public TerminalNode EXPAND() { return getToken(SireniaParser.EXPAND, 0); }
		public FnCallExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitFnCallExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class PipExprContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public PipExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitPipExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class RecurContext extends ExprContext {
		public RecurExprContext recurExpr() {
			return getRuleContext(RecurExprContext.class,0);
		}
		public RecurContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitRecur(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 4;
		enterRecursionRule(_localctx, 4, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(102);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				{
				_localctx = new DefnContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(72);
				defnExpr();
				}
				break;
			case 2:
				{
				_localctx = new IfContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(73);
				ifExpr();
				}
				break;
			case 3:
				{
				_localctx = new CondContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(74);
				condExpr();
				}
				break;
			case 4:
				{
				_localctx = new LoopContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(75);
				loopExpr();
				}
				break;
			case 5:
				{
				_localctx = new RecurContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(76);
				recurExpr();
				}
				break;
			case 6:
				{
				_localctx = new TryCatchContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(77);
				tryCatchExpr();
				}
				break;
			case 7:
				{
				_localctx = new ThrowContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(78);
				throwExpr();
				}
				break;
			case 8:
				{
				_localctx = new IdRefExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(79);
				match(ID);
				}
				break;
			case 9:
				{
				_localctx = new PreExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(80);
				((PreExprContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 1662152343552L) != 0)) ) {
					((PreExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(81);
				expr(16);
				}
				break;
			case 10:
				{
				_localctx = new ScopeExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(82);
				match(LBRACE);
				setState(83);
				exprs();
				setState(84);
				match(RBRACE);
				}
				break;
			case 11:
				{
				_localctx = new LiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(86);
				literalExpr();
				}
				break;
			case 12:
				{
				_localctx = new ParenExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(87);
				match(LPAREN);
				setState(88);
				exprs();
				setState(89);
				match(RPAREN);
				}
				break;
			case 13:
				{
				_localctx = new JavaInterOpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(91);
				javaInterOpExpr();
				}
				break;
			case 14:
				{
				_localctx = new CategoryContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(92);
				categoryExpr();
				}
				break;
			case 15:
				{
				_localctx = new DefExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(93);
				match(T__3);
				setState(94);
				defItem();
				setState(99);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(95);
						match(T__0);
						setState(96);
						defItem();
						}
						} 
					}
					setState(101);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
				}
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(166);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(164);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
					case 1:
						{
						_localctx = new BinExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(104);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(105);
						((BinExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 481036337152L) != 0)) ) {
							((BinExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(106);
						expr(16);
						}
						break;
					case 2:
						{
						_localctx = new BinExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(107);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(108);
						((BinExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==ADD || _la==SUB) ) {
							((BinExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(109);
						expr(15);
						}
						break;
					case 3:
						{
						_localctx = new BinExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(110);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(111);
						((BinExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 15393162788864L) != 0)) ) {
							((BinExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(112);
						expr(14);
						}
						break;
					case 4:
						{
						_localctx = new BinExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(113);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(114);
						((BinExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 263882790666240L) != 0)) ) {
							((BinExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(115);
						expr(13);
						}
						break;
					case 5:
						{
						_localctx = new BinExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(116);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(117);
						((BinExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==EQ || _la==NOT_EQ) ) {
							((BinExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(118);
						expr(12);
						}
						break;
					case 6:
						{
						_localctx = new BinExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(119);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(120);
						((BinExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & 7881299347898368L) != 0)) ) {
							((BinExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(121);
						expr(11);
						}
						break;
					case 7:
						{
						_localctx = new BinExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(122);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(123);
						((BinExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==AND || _la==OR) ) {
							((BinExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(124);
						expr(10);
						}
						break;
					case 8:
						{
						_localctx = new BinExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(125);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(126);
						((BinExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==RANGE || _la==RANGE_TO) ) {
							((BinExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(127);
						expr(9);
						}
						break;
					case 9:
						{
						_localctx = new PipExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(128);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(129);
						match(T__2);
						setState(130);
						match(ID);
						setState(131);
						expr(3);
						}
						break;
					case 10:
						{
						_localctx = new FnCallExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(132);
						if (!(precpred(_ctx, 28))) throw new FailedPredicateException(this, "precpred(_ctx, 28)");
						setState(133);
						match(LPAREN);
						setState(136);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==EXPAND) {
							{
							setState(134);
							match(EXPAND);
							setState(135);
							expr(0);
							}
						}

						setState(138);
						match(RPAREN);
						}
						break;
					case 11:
						{
						_localctx = new FnCallExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(139);
						if (!(precpred(_ctx, 27))) throw new FailedPredicateException(this, "precpred(_ctx, 27)");
						setState(140);
						match(LPAREN);
						setState(141);
						expr(0);
						setState(146);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(142);
								match(T__0);
								setState(143);
								expr(0);
								}
								} 
							}
							setState(148);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
						}
						setState(152);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==T__0) {
							{
							setState(149);
							match(T__0);
							setState(150);
							match(EXPAND);
							setState(151);
							expr(0);
							}
						}

						setState(154);
						match(RPAREN);
						}
						break;
					case 12:
						{
						_localctx = new IdAttrExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(156);
						if (!(precpred(_ctx, 19))) throw new FailedPredicateException(this, "precpred(_ctx, 19)");
						setState(157);
						match(T__1);
						setState(158);
						match(ID);
						}
						break;
					case 13:
						{
						_localctx = new ExprAttrExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(159);
						if (!(precpred(_ctx, 18))) throw new FailedPredicateException(this, "precpred(_ctx, 18)");
						setState(160);
						match(LBRACK);
						setState(161);
						expr(0);
						setState(162);
						match(RBRACK);
						}
						break;
					}
					} 
				}
				setState(168);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class CategoryExprContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public TerminalNode STRING() { return getToken(SireniaParser.STRING, 0); }
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public TerminalNode LBRACE() { return getToken(SireniaParser.LBRACE, 0); }
		public ExprsContext exprs() {
			return getRuleContext(ExprsContext.class,0);
		}
		public TerminalNode RBRACE() { return getToken(SireniaParser.RBRACE, 0); }
		public CategoryExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_categoryExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitCategoryExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CategoryExprContext categoryExpr() throws RecognitionException {
		CategoryExprContext _localctx = new CategoryExprContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_categoryExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(169);
			match(T__4);
			setState(170);
			match(LPAREN);
			setState(171);
			match(STRING);
			setState(172);
			match(RPAREN);
			setState(173);
			match(LBRACE);
			setState(174);
			exprs();
			setState(175);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class MaybeExpandExprContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode EXPAND() { return getToken(SireniaParser.EXPAND, 0); }
		public MaybeExpandExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_maybeExpandExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitMaybeExpandExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MaybeExpandExprContext maybeExpandExpr() throws RecognitionException {
		MaybeExpandExprContext _localctx = new MaybeExpandExprContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_maybeExpandExpr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(178);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EXPAND) {
				{
				setState(177);
				match(EXPAND);
				}
			}

			setState(180);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class JavaInterOpExprContext extends ParserRuleContext {
		public JavaInterOpExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_javaInterOpExpr; }
	 
		public JavaInterOpExprContext() { }
		public void copyFrom(JavaInterOpExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class MethodCallExprContext extends JavaInterOpExprContext {
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public TerminalNode STRING() { return getToken(SireniaParser.STRING, 0); }
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public List<TypedExprContext> typedExpr() {
			return getRuleContexts(TypedExprContext.class);
		}
		public TypedExprContext typedExpr(int i) {
			return getRuleContext(TypedExprContext.class,i);
		}
		public MethodCallExprContext(JavaInterOpExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitMethodCallExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ImportClassContext extends JavaInterOpExprContext {
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public List<TerminalNode> STRING() { return getTokens(SireniaParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(SireniaParser.STRING, i);
		}
		public ImportClassContext(JavaInterOpExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitImportClass(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class NewObjExprContext extends JavaInterOpExprContext {
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public TerminalNode STRING() { return getToken(SireniaParser.STRING, 0); }
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public List<TypedExprContext> typedExpr() {
			return getRuleContexts(TypedExprContext.class);
		}
		public TypedExprContext typedExpr(int i) {
			return getRuleContext(TypedExprContext.class,i);
		}
		public NewObjExprContext(JavaInterOpExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitNewObjExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class FieldSetExprContext extends JavaInterOpExprContext {
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public TerminalNode STRING() { return getToken(SireniaParser.STRING, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public FieldSetExprContext(JavaInterOpExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitFieldSetExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class FieldGetExprContext extends JavaInterOpExprContext {
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public TerminalNode STRING() { return getToken(SireniaParser.STRING, 0); }
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public FieldGetExprContext(JavaInterOpExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitFieldGetExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JavaInterOpExprContext javaInterOpExpr() throws RecognitionException {
		JavaInterOpExprContext _localctx = new JavaInterOpExprContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_javaInterOpExpr);
		int _la;
		try {
			setState(230);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__5:
				_localctx = new ImportClassContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(182);
				match(T__5);
				setState(183);
				match(LPAREN);
				setState(184);
				match(STRING);
				setState(187);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
				case 1:
					{
					setState(185);
					match(T__6);
					setState(186);
					match(STRING);
					}
					break;
				}
				}
				break;
			case T__7:
				_localctx = new NewObjExprContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(189);
				match(T__7);
				setState(190);
				match(LPAREN);
				setState(191);
				match(STRING);
				setState(196);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(192);
					match(T__0);
					setState(193);
					typedExpr();
					}
					}
					setState(198);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(199);
				match(RPAREN);
				}
				break;
			case T__8:
			case T__9:
				_localctx = new MethodCallExprContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(200);
				_la = _input.LA(1);
				if ( !(_la==T__8 || _la==T__9) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(201);
				match(LPAREN);
				setState(202);
				match(STRING);
				setState(207);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(203);
					match(T__0);
					setState(204);
					typedExpr();
					}
					}
					setState(209);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(210);
				match(RPAREN);
				}
				break;
			case T__10:
				_localctx = new FieldGetExprContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(211);
				match(T__10);
				setState(212);
				match(LPAREN);
				setState(213);
				match(STRING);
				setState(216);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__0) {
					{
					setState(214);
					match(T__0);
					setState(215);
					expr(0);
					}
				}

				setState(218);
				match(RPAREN);
				}
				break;
			case T__11:
				_localctx = new FieldSetExprContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(219);
				match(T__11);
				setState(220);
				match(LPAREN);
				setState(221);
				match(STRING);
				setState(222);
				match(T__0);
				setState(223);
				expr(0);
				setState(226);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__0) {
					{
					setState(224);
					match(T__0);
					setState(225);
					expr(0);
					}
				}

				setState(228);
				match(RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TypedExprContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode STRING() { return getToken(SireniaParser.STRING, 0); }
		public TypedExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typedExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitTypedExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypedExprContext typedExpr() throws RecognitionException {
		TypedExprContext _localctx = new TypedExprContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_typedExpr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(232);
			expr(0);
			setState(234);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==STRING) {
				{
				setState(233);
				match(STRING);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DefItemContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public StructParamsContext structParams() {
			return getRuleContext(StructParamsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public DefItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_defItem; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitDefItem(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DefItemContext defItem() throws RecognitionException {
		DefItemContext _localctx = new DefItemContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_defItem);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(238);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
				{
				setState(236);
				match(ID);
				}
				break;
			case LBRACK:
			case LBRACE:
				{
				setState(237);
				structParams();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(242);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				{
				setState(240);
				match(T__12);
				setState(241);
				expr(0);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class DefnExprContext extends ParserRuleContext {
		public DefnExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_defnExpr; }
	 
		public DefnExprContext() { }
		public void copyFrom(DefnExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class DefnExpr1Context extends DefnExprContext {
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public FnParamsContext fnParams() {
			return getRuleContext(FnParamsContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public DefnExpr1Context(DefnExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitDefnExpr1(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class DefnExprNContext extends DefnExprContext {
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public TerminalNode LBRACE() { return getToken(SireniaParser.LBRACE, 0); }
		public List<FnLiteralContext> fnLiteral() {
			return getRuleContexts(FnLiteralContext.class);
		}
		public FnLiteralContext fnLiteral(int i) {
			return getRuleContext(FnLiteralContext.class,i);
		}
		public TerminalNode RBRACE() { return getToken(SireniaParser.RBRACE, 0); }
		public List<TerminalNode> SEMI() { return getTokens(SireniaParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(SireniaParser.SEMI, i);
		}
		public DefnExprNContext(DefnExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitDefnExprN(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DefnExprContext defnExpr() throws RecognitionException {
		DefnExprContext _localctx = new DefnExprContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_defnExpr);
		int _la;
		try {
			int _alt;
			setState(267);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				_localctx = new DefnExpr1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(244);
				match(T__13);
				setState(245);
				match(ID);
				setState(246);
				match(LPAREN);
				setState(247);
				fnParams();
				setState(248);
				match(RPAREN);
				setState(249);
				expr(0);
				}
				break;
			case 2:
				_localctx = new DefnExprNContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(251);
				match(T__13);
				setState(252);
				match(ID);
				setState(253);
				match(LBRACE);
				setState(254);
				fnLiteral();
				setState(259);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(255);
						match(SEMI);
						setState(256);
						fnLiteral();
						}
						} 
					}
					setState(261);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
				}
				setState(263);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SEMI) {
					{
					setState(262);
					match(SEMI);
					}
				}

				setState(265);
				match(RBRACE);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FnParamsContext extends ParserRuleContext {
		public TerminalNode EXPAND() { return getToken(SireniaParser.EXPAND, 0); }
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public StructParamsContext structParams() {
			return getRuleContext(StructParamsContext.class,0);
		}
		public List<ParamsDContext> paramsD() {
			return getRuleContexts(ParamsDContext.class);
		}
		public ParamsDContext paramsD(int i) {
			return getRuleContext(ParamsDContext.class,i);
		}
		public FnParamsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fnParams; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitFnParams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FnParamsContext fnParams() throws RecognitionException {
		FnParamsContext _localctx = new FnParamsContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_fnParams);
		int _la;
		try {
			int _alt;
			setState(292);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case EXPAND:
			case RPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(274);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==EXPAND) {
					{
					setState(269);
					match(EXPAND);
					setState(272);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case ID:
						{
						setState(270);
						match(ID);
						}
						break;
					case LBRACK:
					case LBRACE:
						{
						setState(271);
						structParams();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
				}

				}
				break;
			case ID:
			case LBRACK:
			case LBRACE:
				enterOuterAlt(_localctx, 2);
				{
				setState(276);
				paramsD();
				setState(281);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(277);
						match(T__0);
						setState(278);
						paramsD();
						}
						} 
					}
					setState(283);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
				}
				setState(290);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__0) {
					{
					setState(284);
					match(T__0);
					setState(285);
					match(EXPAND);
					setState(288);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case ID:
						{
						setState(286);
						match(ID);
						}
						break;
					case LBRACK:
					case LBRACE:
						{
						setState(287);
						structParams();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
				}

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ParamsDContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public StructParamsContext structParams() {
			return getRuleContext(StructParamsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ParamsDContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paramsD; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitParamsD(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamsDContext paramsD() throws RecognitionException {
		ParamsDContext _localctx = new ParamsDContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_paramsD);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(296);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
				{
				setState(294);
				match(ID);
				}
				break;
			case LBRACK:
			case LBRACE:
				{
				setState(295);
				structParams();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(300);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__12) {
				{
				setState(298);
				match(T__12);
				setState(299);
				expr(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ThrowExprContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ThrowExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_throwExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitThrowExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ThrowExprContext throwExpr() throws RecognitionException {
		ThrowExprContext _localctx = new ThrowExprContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_throwExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(302);
			match(T__14);
			setState(303);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TryCatchExprContext extends ParserRuleContext {
		public TryBlockContext tryBlock() {
			return getRuleContext(TryBlockContext.class,0);
		}
		public CatchBlockContext catchBlock() {
			return getRuleContext(CatchBlockContext.class,0);
		}
		public FinallyBlockContext finallyBlock() {
			return getRuleContext(FinallyBlockContext.class,0);
		}
		public TryCatchExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tryCatchExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitTryCatchExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TryCatchExprContext tryCatchExpr() throws RecognitionException {
		TryCatchExprContext _localctx = new TryCatchExprContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_tryCatchExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(305);
			tryBlock();
			setState(307);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
			case 1:
				{
				setState(306);
				catchBlock();
				}
				break;
			}
			setState(310);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				{
				setState(309);
				finallyBlock();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TryBlockContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TryBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tryBlock; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitTryBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TryBlockContext tryBlock() throws RecognitionException {
		TryBlockContext _localctx = new TryBlockContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_tryBlock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(312);
			match(T__15);
			setState(313);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class CatchBlockContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public CatchBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_catchBlock; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitCatchBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CatchBlockContext catchBlock() throws RecognitionException {
		CatchBlockContext _localctx = new CatchBlockContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_catchBlock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(315);
			match(T__16);
			setState(316);
			match(LPAREN);
			setState(317);
			match(ID);
			setState(318);
			match(RPAREN);
			setState(319);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FinallyBlockContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public FinallyBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_finallyBlock; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitFinallyBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FinallyBlockContext finallyBlock() throws RecognitionException {
		FinallyBlockContext _localctx = new FinallyBlockContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_finallyBlock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(321);
			match(T__17);
			setState(322);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class IfExprContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public IfExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitIfExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfExprContext ifExpr() throws RecognitionException {
		IfExprContext _localctx = new IfExprContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_ifExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(324);
			match(T__18);
			setState(325);
			expr(0);
			setState(326);
			expr(0);
			setState(329);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				{
				setState(327);
				match(T__19);
				setState(328);
				expr(0);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class CondExprContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(SireniaParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(SireniaParser.RBRACE, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COND_SEP() { return getTokens(SireniaParser.COND_SEP); }
		public TerminalNode COND_SEP(int i) {
			return getToken(SireniaParser.COND_SEP, i);
		}
		public List<TerminalNode> SEMI() { return getTokens(SireniaParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(SireniaParser.SEMI, i);
		}
		public CondExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitCondExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CondExprContext condExpr() throws RecognitionException {
		CondExprContext _localctx = new CondExprContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_condExpr);
		int _la;
		try {
			int _alt;
			setState(355);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(331);
				match(T__20);
				setState(332);
				match(LBRACE);
				setState(333);
				match(RBRACE);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(334);
				match(T__20);
				setState(335);
				match(LBRACE);
				{
				setState(336);
				expr(0);
				setState(337);
				match(COND_SEP);
				setState(338);
				expr(0);
				}
				setState(347);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(340);
						match(SEMI);
						setState(341);
						expr(0);
						setState(342);
						match(COND_SEP);
						setState(343);
						expr(0);
						}
						} 
					}
					setState(349);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
				}
				setState(351);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SEMI) {
					{
					setState(350);
					match(SEMI);
					}
				}

				setState(353);
				match(RBRACE);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LoopExprContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<LoopParamContext> loopParam() {
			return getRuleContexts(LoopParamContext.class);
		}
		public LoopParamContext loopParam(int i) {
			return getRuleContext(LoopParamContext.class,i);
		}
		public LoopExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loopExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitLoopExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LoopExprContext loopExpr() throws RecognitionException {
		LoopExprContext _localctx = new LoopExprContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_loopExpr);
		int _la;
		try {
			setState(379);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,37,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(357);
				match(T__21);
				setState(358);
				match(LPAREN);
				setState(359);
				match(RPAREN);
				setState(360);
				expr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(361);
				match(T__21);
				setState(362);
				match(LPAREN);
				setState(363);
				loopParam();
				setState(364);
				match(T__12);
				setState(365);
				expr(0);
				setState(373);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(366);
					match(T__0);
					setState(367);
					loopParam();
					setState(368);
					match(T__12);
					setState(369);
					expr(0);
					}
					}
					setState(375);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(376);
				match(RPAREN);
				setState(377);
				expr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LoopParamContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public StructParamsContext structParams() {
			return getRuleContext(StructParamsContext.class,0);
		}
		public LoopParamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loopParam; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitLoopParam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LoopParamContext loopParam() throws RecognitionException {
		LoopParamContext _localctx = new LoopParamContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_loopParam);
		try {
			setState(383);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(381);
				match(ID);
				}
				break;
			case LBRACK:
			case LBRACE:
				enterOuterAlt(_localctx, 2);
				{
				setState(382);
				structParams();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class RecurExprContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode EXPAND() { return getToken(SireniaParser.EXPAND, 0); }
		public RecurExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_recurExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitRecurExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RecurExprContext recurExpr() throws RecognitionException {
		RecurExprContext _localctx = new RecurExprContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_recurExpr);
		int _la;
		try {
			int _alt;
			setState(405);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,41,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(385);
				match(T__22);
				setState(386);
				match(LPAREN);
				setState(387);
				match(RPAREN);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(388);
				match(T__22);
				setState(389);
				match(LPAREN);
				setState(390);
				expr(0);
				setState(395);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(391);
						match(T__0);
						setState(392);
						expr(0);
						}
						} 
					}
					setState(397);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
				}
				setState(401);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__0) {
					{
					setState(398);
					match(T__0);
					setState(399);
					match(EXPAND);
					setState(400);
					expr(0);
					}
				}

				setState(403);
				match(RPAREN);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class LiteralExprContext extends ParserRuleContext {
		public LiteralExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literalExpr; }
	 
		public LiteralExprContext() { }
		public void copyFrom(LiteralExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class StrLiteralContext extends LiteralExprContext {
		public TerminalNode STRING() { return getToken(SireniaParser.STRING, 0); }
		public StrLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitStrLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class NullLiteralContext extends LiteralExprContext {
		public NullLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitNullLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class VecLiteralContext extends LiteralExprContext {
		public TerminalNode LBRACK() { return getToken(SireniaParser.LBRACK, 0); }
		public List<MaybeExpandExprContext> maybeExpandExpr() {
			return getRuleContexts(MaybeExpandExprContext.class);
		}
		public MaybeExpandExprContext maybeExpandExpr(int i) {
			return getRuleContext(MaybeExpandExprContext.class,i);
		}
		public TerminalNode RBRACK() { return getToken(SireniaParser.RBRACK, 0); }
		public VecLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitVecLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class TmplStringLiteralContext extends LiteralExprContext {
		public TerminalNode TMPL_STRING() { return getToken(SireniaParser.TMPL_STRING, 0); }
		public TmplStringLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitTmplStringLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class IntLiteralContext extends LiteralExprContext {
		public TerminalNode INT() { return getToken(SireniaParser.INT, 0); }
		public IntLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitIntLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class FnContext extends LiteralExprContext {
		public FnLiteralContext fnLiteral() {
			return getRuleContext(FnLiteralContext.class,0);
		}
		public FnContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitFn(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class CharLiteralContext extends LiteralExprContext {
		public TerminalNode CHAR() { return getToken(SireniaParser.CHAR, 0); }
		public CharLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitCharLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class TrueLiteralContext extends LiteralExprContext {
		public TrueLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitTrueLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class EmptyVecLiteralContext extends LiteralExprContext {
		public TerminalNode LBRACK() { return getToken(SireniaParser.LBRACK, 0); }
		public TerminalNode RBRACK() { return getToken(SireniaParser.RBRACK, 0); }
		public EmptyVecLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitEmptyVecLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class EmptyMapLiteralContext extends LiteralExprContext {
		public TerminalNode LBRACE() { return getToken(SireniaParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(SireniaParser.RBRACE, 0); }
		public EmptyMapLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitEmptyMapLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ZeroLiteralContext extends LiteralExprContext {
		public ZeroLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitZeroLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class MapLiteralContext extends LiteralExprContext {
		public TerminalNode LBRACE() { return getToken(SireniaParser.LBRACE, 0); }
		public List<KvPairContext> kvPair() {
			return getRuleContexts(KvPairContext.class);
		}
		public KvPairContext kvPair(int i) {
			return getRuleContext(KvPairContext.class,i);
		}
		public TerminalNode RBRACE() { return getToken(SireniaParser.RBRACE, 0); }
		public MapLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitMapLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class FalseLiteralContext extends LiteralExprContext {
		public FalseLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitFalseLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class FloatLiteralContext extends LiteralExprContext {
		public TerminalNode FLOAT() { return getToken(SireniaParser.FLOAT, 0); }
		public FloatLiteralContext(LiteralExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitFloatLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralExprContext literalExpr() throws RecognitionException {
		LiteralExprContext _localctx = new LiteralExprContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_literalExpr);
		int _la;
		try {
			int _alt;
			setState(449);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,46,_ctx) ) {
			case 1:
				_localctx = new CharLiteralContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(407);
				match(CHAR);
				}
				break;
			case 2:
				_localctx = new StrLiteralContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(408);
				match(STRING);
				}
				break;
			case 3:
				_localctx = new ZeroLiteralContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(409);
				match(T__23);
				}
				break;
			case 4:
				_localctx = new IntLiteralContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(410);
				match(INT);
				}
				break;
			case 5:
				_localctx = new FloatLiteralContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(411);
				match(FLOAT);
				}
				break;
			case 6:
				_localctx = new TrueLiteralContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(412);
				match(T__24);
				}
				break;
			case 7:
				_localctx = new FalseLiteralContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(413);
				match(T__25);
				}
				break;
			case 8:
				_localctx = new NullLiteralContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(414);
				match(T__26);
				}
				break;
			case 9:
				_localctx = new TmplStringLiteralContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(415);
				match(TMPL_STRING);
				}
				break;
			case 10:
				_localctx = new EmptyMapLiteralContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(416);
				match(LBRACE);
				setState(417);
				match(RBRACE);
				}
				break;
			case 11:
				_localctx = new MapLiteralContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(418);
				match(LBRACE);
				setState(419);
				kvPair();
				setState(424);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,42,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(420);
						match(T__0);
						setState(421);
						kvPair();
						}
						} 
					}
					setState(426);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,42,_ctx);
				}
				setState(428);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__0) {
					{
					setState(427);
					match(T__0);
					}
				}

				setState(430);
				match(RBRACE);
				}
				break;
			case 12:
				_localctx = new EmptyVecLiteralContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(432);
				match(LBRACK);
				setState(433);
				match(RBRACK);
				}
				break;
			case 13:
				_localctx = new VecLiteralContext(_localctx);
				enterOuterAlt(_localctx, 13);
				{
				setState(434);
				match(LBRACK);
				setState(435);
				maybeExpandExpr();
				setState(440);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,44,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(436);
						match(T__0);
						setState(437);
						maybeExpandExpr();
						}
						} 
					}
					setState(442);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,44,_ctx);
				}
				setState(444);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__0) {
					{
					setState(443);
					match(T__0);
					}
				}

				setState(446);
				match(RBRACK);
				}
				break;
			case 14:
				_localctx = new FnContext(_localctx);
				enterOuterAlt(_localctx, 14);
				{
				setState(448);
				fnLiteral();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class FnLiteralContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public FnParamsContext fnParams() {
			return getRuleContext(FnParamsContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public FnLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fnLiteral; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitFnLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FnLiteralContext fnLiteral() throws RecognitionException {
		FnLiteralContext _localctx = new FnLiteralContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_fnLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(456);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
				{
				setState(451);
				match(ID);
				}
				break;
			case LPAREN:
				{
				{
				setState(452);
				match(LPAREN);
				setState(453);
				fnParams();
				setState(454);
				match(RPAREN);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(458);
			match(T__27);
			setState(459);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class KvPairContext extends ParserRuleContext {
		public KvPairContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_kvPair; }
	 
		public KvPairContext() { }
		public void copyFrom(KvPairContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExprKeyPairContext extends KvPairContext {
		public TerminalNode LPAREN() { return getToken(SireniaParser.LPAREN, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode RPAREN() { return getToken(SireniaParser.RPAREN, 0); }
		public ExprKeyPairContext(KvPairContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitExprKeyPair(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class IdKeyPairContext extends KvPairContext {
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public IdKeyPairContext(KvPairContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitIdKeyPair(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class StrKeyPairContext extends KvPairContext {
		public TerminalNode STRING() { return getToken(SireniaParser.STRING, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public StrKeyPairContext(KvPairContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitStrKeyPair(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ExpandPairContext extends KvPairContext {
		public TerminalNode EXPAND() { return getToken(SireniaParser.EXPAND, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ExpandPairContext(KvPairContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitExpandPair(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KvPairContext kvPair() throws RecognitionException {
		KvPairContext _localctx = new KvPairContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_kvPair);
		try {
			setState(475);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
				_localctx = new IdKeyPairContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(461);
				match(ID);
				setState(462);
				match(T__28);
				setState(463);
				expr(0);
				}
				break;
			case STRING:
				_localctx = new StrKeyPairContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(464);
				match(STRING);
				setState(465);
				match(T__28);
				setState(466);
				expr(0);
				}
				break;
			case LPAREN:
				_localctx = new ExprKeyPairContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(467);
				match(LPAREN);
				setState(468);
				expr(0);
				setState(469);
				match(RPAREN);
				setState(470);
				match(T__28);
				setState(471);
				expr(0);
				}
				break;
			case EXPAND:
				_localctx = new ExpandPairContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(473);
				match(EXPAND);
				setState(474);
				expr(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StructParamsContext extends ParserRuleContext {
		public VecStructParamsContext vecStructParams() {
			return getRuleContext(VecStructParamsContext.class,0);
		}
		public MapStructParamsContext mapStructParams() {
			return getRuleContext(MapStructParamsContext.class,0);
		}
		public StructParamsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_structParams; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitStructParams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StructParamsContext structParams() throws RecognitionException {
		StructParamsContext _localctx = new StructParamsContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_structParams);
		try {
			setState(479);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LBRACK:
				enterOuterAlt(_localctx, 1);
				{
				setState(477);
				vecStructParams();
				}
				break;
			case LBRACE:
				enterOuterAlt(_localctx, 2);
				{
				setState(478);
				mapStructParams();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class VecStructParamsContext extends ParserRuleContext {
		public TerminalNode LBRACK() { return getToken(SireniaParser.LBRACK, 0); }
		public TerminalNode RBRACK() { return getToken(SireniaParser.RBRACK, 0); }
		public TerminalNode EXPAND() { return getToken(SireniaParser.EXPAND, 0); }
		public AsContext as() {
			return getRuleContext(AsContext.class,0);
		}
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public VecStructParamsContext vecStructParams() {
			return getRuleContext(VecStructParamsContext.class,0);
		}
		public List<ParamsDContext> paramsD() {
			return getRuleContexts(ParamsDContext.class);
		}
		public ParamsDContext paramsD(int i) {
			return getRuleContext(ParamsDContext.class,i);
		}
		public VecStructParamsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_vecStructParams; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitVecStructParams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VecStructParamsContext vecStructParams() throws RecognitionException {
		VecStructParamsContext _localctx = new VecStructParamsContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_vecStructParams);
		int _la;
		try {
			int _alt;
			setState(515);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,57,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(481);
				match(LBRACK);
				setState(487);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==EXPAND) {
					{
					setState(482);
					match(EXPAND);
					setState(485);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case ID:
						{
						setState(483);
						match(ID);
						}
						break;
					case LBRACK:
						{
						setState(484);
						vecStructParams();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
				}

				setState(490);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__6) {
					{
					setState(489);
					as();
					}
				}

				setState(492);
				match(RBRACK);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(493);
				match(LBRACK);
				setState(494);
				paramsD();
				setState(499);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,53,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(495);
						match(T__0);
						setState(496);
						paramsD();
						}
						} 
					}
					setState(501);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,53,_ctx);
				}
				setState(508);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__0) {
					{
					setState(502);
					match(T__0);
					setState(503);
					match(EXPAND);
					setState(506);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case ID:
						{
						setState(504);
						match(ID);
						}
						break;
					case LBRACK:
						{
						setState(505);
						vecStructParams();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
				}

				setState(511);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__6) {
					{
					setState(510);
					as();
					}
				}

				setState(513);
				match(RBRACK);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class MapStructParamsContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(SireniaParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(SireniaParser.RBRACE, 0); }
		public TerminalNode EXPAND() { return getToken(SireniaParser.EXPAND, 0); }
		public AsContext as() {
			return getRuleContext(AsContext.class,0);
		}
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public MapStructParamsContext mapStructParams() {
			return getRuleContext(MapStructParamsContext.class,0);
		}
		public List<StructPairContext> structPair() {
			return getRuleContexts(StructPairContext.class);
		}
		public StructPairContext structPair(int i) {
			return getRuleContext(StructPairContext.class,i);
		}
		public MapStructParamsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mapStructParams; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitMapStructParams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MapStructParamsContext mapStructParams() throws RecognitionException {
		MapStructParamsContext _localctx = new MapStructParamsContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_mapStructParams);
		int _la;
		try {
			int _alt;
			setState(551);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,65,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(517);
				match(LBRACE);
				setState(523);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==EXPAND) {
					{
					setState(518);
					match(EXPAND);
					setState(521);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case ID:
						{
						setState(519);
						match(ID);
						}
						break;
					case LBRACE:
						{
						setState(520);
						mapStructParams();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
				}

				setState(526);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__6) {
					{
					setState(525);
					as();
					}
				}

				setState(528);
				match(RBRACE);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(529);
				match(LBRACE);
				setState(530);
				structPair();
				setState(535);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,61,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(531);
						match(T__0);
						setState(532);
						structPair();
						}
						} 
					}
					setState(537);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,61,_ctx);
				}
				setState(544);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__0) {
					{
					setState(538);
					match(T__0);
					setState(539);
					match(EXPAND);
					setState(542);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case ID:
						{
						setState(540);
						match(ID);
						}
						break;
					case LBRACE:
						{
						setState(541);
						mapStructParams();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
				}

				setState(547);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__6) {
					{
					setState(546);
					as();
					}
				}

				setState(549);
				match(RBRACE);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class StructPairContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public ParamsDContext paramsD() {
			return getRuleContext(ParamsDContext.class,0);
		}
		public StructPairContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_structPair; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitStructPair(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StructPairContext structPair() throws RecognitionException {
		StructPairContext _localctx = new StructPairContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_structPair);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(553);
			match(ID);
			setState(556);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__28) {
				{
				setState(554);
				match(T__28);
				setState(555);
				paramsD();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class AsContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SireniaParser.ID, 0); }
		public AsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_as; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SireniaVisitor ) return ((SireniaVisitor<? extends T>)visitor).visitAs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AsContext as() throws RecognitionException {
		AsContext _localctx = new AsContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_as);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(558);
			match(T__6);
			setState(559);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 2:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 15);
		case 1:
			return precpred(_ctx, 14);
		case 2:
			return precpred(_ctx, 13);
		case 3:
			return precpred(_ctx, 12);
		case 4:
			return precpred(_ctx, 11);
		case 5:
			return precpred(_ctx, 10);
		case 6:
			return precpred(_ctx, 9);
		case 7:
			return precpred(_ctx, 8);
		case 8:
			return precpred(_ctx, 2);
		case 9:
			return precpred(_ctx, 28);
		case 10:
			return precpred(_ctx, 27);
		case 11:
			return precpred(_ctx, 19);
		case 12:
			return precpred(_ctx, 18);
		}
		return true;
	}

	public static final String _serializedATN =
		"\u0004\u0001I\u0232\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0002"+
		"\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004\u0002"+
		"\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007\u0002"+
		"\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b\u0002"+
		"\f\u0007\f\u0002\r\u0007\r\u0002\u000e\u0007\u000e\u0002\u000f\u0007\u000f"+
		"\u0002\u0010\u0007\u0010\u0002\u0011\u0007\u0011\u0002\u0012\u0007\u0012"+
		"\u0002\u0013\u0007\u0013\u0002\u0014\u0007\u0014\u0002\u0015\u0007\u0015"+
		"\u0002\u0016\u0007\u0016\u0002\u0017\u0007\u0017\u0002\u0018\u0007\u0018"+
		"\u0002\u0019\u0007\u0019\u0002\u001a\u0007\u001a\u0002\u001b\u0007\u001b"+
		"\u0002\u001c\u0007\u001c\u0001\u0000\u0001\u0000\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0005\u0001@\b\u0001\n\u0001\f\u0001C\t\u0001\u0001\u0001"+
		"\u0003\u0001F\b\u0001\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0005\u0002b\b\u0002"+
		"\n\u0002\f\u0002e\t\u0002\u0003\u0002g\b\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0003\u0002\u0089\b\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0005\u0002\u0091\b\u0002\n\u0002\f\u0002\u0094"+
		"\t\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0003\u0002\u0099\b\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0005\u0002\u00a5\b\u0002"+
		"\n\u0002\f\u0002\u00a8\t\u0002\u0001\u0003\u0001\u0003\u0001\u0003\u0001"+
		"\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0004\u0003"+
		"\u0004\u00b3\b\u0004\u0001\u0004\u0001\u0004\u0001\u0005\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0003\u0005\u00bc\b\u0005\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0005\u0005\u00c3\b\u0005\n"+
		"\u0005\f\u0005\u00c6\t\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0005\u0005\u00ce\b\u0005\n\u0005\f\u0005"+
		"\u00d1\t\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005"+
		"\u0001\u0005\u0003\u0005\u00d9\b\u0005\u0001\u0005\u0001\u0005\u0001\u0005"+
		"\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0003\u0005"+
		"\u00e3\b\u0005\u0001\u0005\u0001\u0005\u0003\u0005\u00e7\b\u0005\u0001"+
		"\u0006\u0001\u0006\u0003\u0006\u00eb\b\u0006\u0001\u0007\u0001\u0007\u0003"+
		"\u0007\u00ef\b\u0007\u0001\u0007\u0001\u0007\u0003\u0007\u00f3\b\u0007"+
		"\u0001\b\u0001\b\u0001\b\u0001\b\u0001\b\u0001\b\u0001\b\u0001\b\u0001"+
		"\b\u0001\b\u0001\b\u0001\b\u0001\b\u0005\b\u0102\b\b\n\b\f\b\u0105\t\b"+
		"\u0001\b\u0003\b\u0108\b\b\u0001\b\u0001\b\u0003\b\u010c\b\b\u0001\t\u0001"+
		"\t\u0001\t\u0003\t\u0111\b\t\u0003\t\u0113\b\t\u0001\t\u0001\t\u0001\t"+
		"\u0005\t\u0118\b\t\n\t\f\t\u011b\t\t\u0001\t\u0001\t\u0001\t\u0001\t\u0003"+
		"\t\u0121\b\t\u0003\t\u0123\b\t\u0003\t\u0125\b\t\u0001\n\u0001\n\u0003"+
		"\n\u0129\b\n\u0001\n\u0001\n\u0003\n\u012d\b\n\u0001\u000b\u0001\u000b"+
		"\u0001\u000b\u0001\f\u0001\f\u0003\f\u0134\b\f\u0001\f\u0003\f\u0137\b"+
		"\f\u0001\r\u0001\r\u0001\r\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e"+
		"\u0001\u000e\u0001\u000e\u0001\u000f\u0001\u000f\u0001\u000f\u0001\u0010"+
		"\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0003\u0010\u014a\b\u0010"+
		"\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011"+
		"\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011"+
		"\u0001\u0011\u0001\u0011\u0005\u0011\u015a\b\u0011\n\u0011\f\u0011\u015d"+
		"\t\u0011\u0001\u0011\u0003\u0011\u0160\b\u0011\u0001\u0011\u0001\u0011"+
		"\u0003\u0011\u0164\b\u0011\u0001\u0012\u0001\u0012\u0001\u0012\u0001\u0012"+
		"\u0001\u0012\u0001\u0012\u0001\u0012\u0001\u0012\u0001\u0012\u0001\u0012"+
		"\u0001\u0012\u0001\u0012\u0001\u0012\u0001\u0012\u0005\u0012\u0174\b\u0012"+
		"\n\u0012\f\u0012\u0177\t\u0012\u0001\u0012\u0001\u0012\u0001\u0012\u0003"+
		"\u0012\u017c\b\u0012\u0001\u0013\u0001\u0013\u0003\u0013\u0180\b\u0013"+
		"\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014"+
		"\u0001\u0014\u0001\u0014\u0005\u0014\u018a\b\u0014\n\u0014\f\u0014\u018d"+
		"\t\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0003\u0014\u0192\b\u0014"+
		"\u0001\u0014\u0001\u0014\u0003\u0014\u0196\b\u0014\u0001\u0015\u0001\u0015"+
		"\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015"+
		"\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015"+
		"\u0001\u0015\u0005\u0015\u01a7\b\u0015\n\u0015\f\u0015\u01aa\t\u0015\u0001"+
		"\u0015\u0003\u0015\u01ad\b\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001"+
		"\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0005\u0015\u01b7"+
		"\b\u0015\n\u0015\f\u0015\u01ba\t\u0015\u0001\u0015\u0003\u0015\u01bd\b"+
		"\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0003\u0015\u01c2\b\u0015\u0001"+
		"\u0016\u0001\u0016\u0001\u0016\u0001\u0016\u0001\u0016\u0003\u0016\u01c9"+
		"\b\u0016\u0001\u0016\u0001\u0016\u0001\u0016\u0001\u0017\u0001\u0017\u0001"+
		"\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0001"+
		"\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0003"+
		"\u0017\u01dc\b\u0017\u0001\u0018\u0001\u0018\u0003\u0018\u01e0\b\u0018"+
		"\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0003\u0019\u01e6\b\u0019"+
		"\u0003\u0019\u01e8\b\u0019\u0001\u0019\u0003\u0019\u01eb\b\u0019\u0001"+
		"\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0005\u0019\u01f2"+
		"\b\u0019\n\u0019\f\u0019\u01f5\t\u0019\u0001\u0019\u0001\u0019\u0001\u0019"+
		"\u0001\u0019\u0003\u0019\u01fb\b\u0019\u0003\u0019\u01fd\b\u0019\u0001"+
		"\u0019\u0003\u0019\u0200\b\u0019\u0001\u0019\u0001\u0019\u0003\u0019\u0204"+
		"\b\u0019\u0001\u001a\u0001\u001a\u0001\u001a\u0001\u001a\u0003\u001a\u020a"+
		"\b\u001a\u0003\u001a\u020c\b\u001a\u0001\u001a\u0003\u001a\u020f\b\u001a"+
		"\u0001\u001a\u0001\u001a\u0001\u001a\u0001\u001a\u0001\u001a\u0005\u001a"+
		"\u0216\b\u001a\n\u001a\f\u001a\u0219\t\u001a\u0001\u001a\u0001\u001a\u0001"+
		"\u001a\u0001\u001a\u0003\u001a\u021f\b\u001a\u0003\u001a\u0221\b\u001a"+
		"\u0001\u001a\u0003\u001a\u0224\b\u001a\u0001\u001a\u0001\u001a\u0003\u001a"+
		"\u0228\b\u001a\u0001\u001b\u0001\u001b\u0001\u001b\u0003\u001b\u022d\b"+
		"\u001b\u0001\u001c\u0001\u001c\u0001\u001c\u0001\u001c\u0000\u0001\u0004"+
		"\u001d\u0000\u0002\u0004\u0006\b\n\f\u000e\u0010\u0012\u0014\u0016\u0018"+
		"\u001a\u001c\u001e \"$&(*,.02468\u0000\n\u0002\u0000 !\'(\u0001\u0000"+
		"$&\u0001\u0000\'(\u0001\u0000)+\u0001\u0000,/\u0001\u000001\u0001\u0000"+
		"24\u0001\u000056\u0001\u000078\u0001\u0000\t\n\u0280\u0000:\u0001\u0000"+
		"\u0000\u0000\u0002A\u0001\u0000\u0000\u0000\u0004f\u0001\u0000\u0000\u0000"+
		"\u0006\u00a9\u0001\u0000\u0000\u0000\b\u00b2\u0001\u0000\u0000\u0000\n"+
		"\u00e6\u0001\u0000\u0000\u0000\f\u00e8\u0001\u0000\u0000\u0000\u000e\u00ee"+
		"\u0001\u0000\u0000\u0000\u0010\u010b\u0001\u0000\u0000\u0000\u0012\u0124"+
		"\u0001\u0000\u0000\u0000\u0014\u0128\u0001\u0000\u0000\u0000\u0016\u012e"+
		"\u0001\u0000\u0000\u0000\u0018\u0131\u0001\u0000\u0000\u0000\u001a\u0138"+
		"\u0001\u0000\u0000\u0000\u001c\u013b\u0001\u0000\u0000\u0000\u001e\u0141"+
		"\u0001\u0000\u0000\u0000 \u0144\u0001\u0000\u0000\u0000\"\u0163\u0001"+
		"\u0000\u0000\u0000$\u017b\u0001\u0000\u0000\u0000&\u017f\u0001\u0000\u0000"+
		"\u0000(\u0195\u0001\u0000\u0000\u0000*\u01c1\u0001\u0000\u0000\u0000,"+
		"\u01c8\u0001\u0000\u0000\u0000.\u01db\u0001\u0000\u0000\u00000\u01df\u0001"+
		"\u0000\u0000\u00002\u0203\u0001\u0000\u0000\u00004\u0227\u0001\u0000\u0000"+
		"\u00006\u0229\u0001\u0000\u0000\u00008\u022e\u0001\u0000\u0000\u0000:"+
		";\u0003\u0002\u0001\u0000;\u0001\u0001\u0000\u0000\u0000<=\u0003\u0004"+
		"\u0002\u0000=>\u0005F\u0000\u0000>@\u0001\u0000\u0000\u0000?<\u0001\u0000"+
		"\u0000\u0000@C\u0001\u0000\u0000\u0000A?\u0001\u0000\u0000\u0000AB\u0001"+
		"\u0000\u0000\u0000BE\u0001\u0000\u0000\u0000CA\u0001\u0000\u0000\u0000"+
		"DF\u0003\u0004\u0002\u0000ED\u0001\u0000\u0000\u0000EF\u0001\u0000\u0000"+
		"\u0000F\u0003\u0001\u0000\u0000\u0000GH\u0006\u0002\uffff\uffff\u0000"+
		"Hg\u0003\u0010\b\u0000Ig\u0003 \u0010\u0000Jg\u0003\"\u0011\u0000Kg\u0003"+
		"$\u0012\u0000Lg\u0003(\u0014\u0000Mg\u0003\u0018\f\u0000Ng\u0003\u0016"+
		"\u000b\u0000Og\u0005>\u0000\u0000PQ\u0007\u0000\u0000\u0000Qg\u0003\u0004"+
		"\u0002\u0010RS\u0005C\u0000\u0000ST\u0003\u0002\u0001\u0000TU\u0005D\u0000"+
		"\u0000Ug\u0001\u0000\u0000\u0000Vg\u0003*\u0015\u0000WX\u0005?\u0000\u0000"+
		"XY\u0003\u0002\u0001\u0000YZ\u0005@\u0000\u0000Zg\u0001\u0000\u0000\u0000"+
		"[g\u0003\n\u0005\u0000\\g\u0003\u0006\u0003\u0000]^\u0005\u0004\u0000"+
		"\u0000^c\u0003\u000e\u0007\u0000_`\u0005\u0001\u0000\u0000`b\u0003\u000e"+
		"\u0007\u0000a_\u0001\u0000\u0000\u0000be\u0001\u0000\u0000\u0000ca\u0001"+
		"\u0000\u0000\u0000cd\u0001\u0000\u0000\u0000dg\u0001\u0000\u0000\u0000"+
		"ec\u0001\u0000\u0000\u0000fG\u0001\u0000\u0000\u0000fI\u0001\u0000\u0000"+
		"\u0000fJ\u0001\u0000\u0000\u0000fK\u0001\u0000\u0000\u0000fL\u0001\u0000"+
		"\u0000\u0000fM\u0001\u0000\u0000\u0000fN\u0001\u0000\u0000\u0000fO\u0001"+
		"\u0000\u0000\u0000fP\u0001\u0000\u0000\u0000fR\u0001\u0000\u0000\u0000"+
		"fV\u0001\u0000\u0000\u0000fW\u0001\u0000\u0000\u0000f[\u0001\u0000\u0000"+
		"\u0000f\\\u0001\u0000\u0000\u0000f]\u0001\u0000\u0000\u0000g\u00a6\u0001"+
		"\u0000\u0000\u0000hi\n\u000f\u0000\u0000ij\u0007\u0001\u0000\u0000j\u00a5"+
		"\u0003\u0004\u0002\u0010kl\n\u000e\u0000\u0000lm\u0007\u0002\u0000\u0000"+
		"m\u00a5\u0003\u0004\u0002\u000fno\n\r\u0000\u0000op\u0007\u0003\u0000"+
		"\u0000p\u00a5\u0003\u0004\u0002\u000eqr\n\f\u0000\u0000rs\u0007\u0004"+
		"\u0000\u0000s\u00a5\u0003\u0004\u0002\rtu\n\u000b\u0000\u0000uv\u0007"+
		"\u0005\u0000\u0000v\u00a5\u0003\u0004\u0002\fwx\n\n\u0000\u0000xy\u0007"+
		"\u0006\u0000\u0000y\u00a5\u0003\u0004\u0002\u000bz{\n\t\u0000\u0000{|"+
		"\u0007\u0007\u0000\u0000|\u00a5\u0003\u0004\u0002\n}~\n\b\u0000\u0000"+
		"~\u007f\u0007\b\u0000\u0000\u007f\u00a5\u0003\u0004\u0002\t\u0080\u0081"+
		"\n\u0002\u0000\u0000\u0081\u0082\u0005\u0003\u0000\u0000\u0082\u0083\u0005"+
		">\u0000\u0000\u0083\u00a5\u0003\u0004\u0002\u0003\u0084\u0085\n\u001c"+
		"\u0000\u0000\u0085\u0088\u0005?\u0000\u0000\u0086\u0087\u0005\"\u0000"+
		"\u0000\u0087\u0089\u0003\u0004\u0002\u0000\u0088\u0086\u0001\u0000\u0000"+
		"\u0000\u0088\u0089\u0001\u0000\u0000\u0000\u0089\u008a\u0001\u0000\u0000"+
		"\u0000\u008a\u00a5\u0005@\u0000\u0000\u008b\u008c\n\u001b\u0000\u0000"+
		"\u008c\u008d\u0005?\u0000\u0000\u008d\u0092\u0003\u0004\u0002\u0000\u008e"+
		"\u008f\u0005\u0001\u0000\u0000\u008f\u0091\u0003\u0004\u0002\u0000\u0090"+
		"\u008e\u0001\u0000\u0000\u0000\u0091\u0094\u0001\u0000\u0000\u0000\u0092"+
		"\u0090\u0001\u0000\u0000\u0000\u0092\u0093\u0001\u0000\u0000\u0000\u0093"+
		"\u0098\u0001\u0000\u0000\u0000\u0094\u0092\u0001\u0000\u0000\u0000\u0095"+
		"\u0096\u0005\u0001\u0000\u0000\u0096\u0097\u0005\"\u0000\u0000\u0097\u0099"+
		"\u0003\u0004\u0002\u0000\u0098\u0095\u0001\u0000\u0000\u0000\u0098\u0099"+
		"\u0001\u0000\u0000\u0000\u0099\u009a\u0001\u0000\u0000\u0000\u009a\u009b"+
		"\u0005@\u0000\u0000\u009b\u00a5\u0001\u0000\u0000\u0000\u009c\u009d\n"+
		"\u0013\u0000\u0000\u009d\u009e\u0005\u0002\u0000\u0000\u009e\u00a5\u0005"+
		">\u0000\u0000\u009f\u00a0\n\u0012\u0000\u0000\u00a0\u00a1\u0005A\u0000"+
		"\u0000\u00a1\u00a2\u0003\u0004\u0002\u0000\u00a2\u00a3\u0005B\u0000\u0000"+
		"\u00a3\u00a5\u0001\u0000\u0000\u0000\u00a4h\u0001\u0000\u0000\u0000\u00a4"+
		"k\u0001\u0000\u0000\u0000\u00a4n\u0001\u0000\u0000\u0000\u00a4q\u0001"+
		"\u0000\u0000\u0000\u00a4t\u0001\u0000\u0000\u0000\u00a4w\u0001\u0000\u0000"+
		"\u0000\u00a4z\u0001\u0000\u0000\u0000\u00a4}\u0001\u0000\u0000\u0000\u00a4"+
		"\u0080\u0001\u0000\u0000\u0000\u00a4\u0084\u0001\u0000\u0000\u0000\u00a4"+
		"\u008b\u0001\u0000\u0000\u0000\u00a4\u009c\u0001\u0000\u0000\u0000\u00a4"+
		"\u009f\u0001\u0000\u0000\u0000\u00a5\u00a8\u0001\u0000\u0000\u0000\u00a6"+
		"\u00a4\u0001\u0000\u0000\u0000\u00a6\u00a7\u0001\u0000\u0000\u0000\u00a7"+
		"\u0005\u0001\u0000\u0000\u0000\u00a8\u00a6\u0001\u0000\u0000\u0000\u00a9"+
		"\u00aa\u0005\u0005\u0000\u0000\u00aa\u00ab\u0005?\u0000\u0000\u00ab\u00ac"+
		"\u0005;\u0000\u0000\u00ac\u00ad\u0005@\u0000\u0000\u00ad\u00ae\u0005C"+
		"\u0000\u0000\u00ae\u00af\u0003\u0002\u0001\u0000\u00af\u00b0\u0005D\u0000"+
		"\u0000\u00b0\u0007\u0001\u0000\u0000\u0000\u00b1\u00b3\u0005\"\u0000\u0000"+
		"\u00b2\u00b1\u0001\u0000\u0000\u0000\u00b2\u00b3\u0001\u0000\u0000\u0000"+
		"\u00b3\u00b4\u0001\u0000\u0000\u0000\u00b4\u00b5\u0003\u0004\u0002\u0000"+
		"\u00b5\t\u0001\u0000\u0000\u0000\u00b6\u00b7\u0005\u0006\u0000\u0000\u00b7"+
		"\u00b8\u0005?\u0000\u0000\u00b8\u00bb\u0005;\u0000\u0000\u00b9\u00ba\u0005"+
		"\u0007\u0000\u0000\u00ba\u00bc\u0005;\u0000\u0000\u00bb\u00b9\u0001\u0000"+
		"\u0000\u0000\u00bb\u00bc\u0001\u0000\u0000\u0000\u00bc\u00e7\u0001\u0000"+
		"\u0000\u0000\u00bd\u00be\u0005\b\u0000\u0000\u00be\u00bf\u0005?\u0000"+
		"\u0000\u00bf\u00c4\u0005;\u0000\u0000\u00c0\u00c1\u0005\u0001\u0000\u0000"+
		"\u00c1\u00c3\u0003\f\u0006\u0000\u00c2\u00c0\u0001\u0000\u0000\u0000\u00c3"+
		"\u00c6\u0001\u0000\u0000\u0000\u00c4\u00c2\u0001\u0000\u0000\u0000\u00c4"+
		"\u00c5\u0001\u0000\u0000\u0000\u00c5\u00c7\u0001\u0000\u0000\u0000\u00c6"+
		"\u00c4\u0001\u0000\u0000\u0000\u00c7\u00e7\u0005@\u0000\u0000\u00c8\u00c9"+
		"\u0007\t\u0000\u0000\u00c9\u00ca\u0005?\u0000\u0000\u00ca\u00cf\u0005"+
		";\u0000\u0000\u00cb\u00cc\u0005\u0001\u0000\u0000\u00cc\u00ce\u0003\f"+
		"\u0006\u0000\u00cd\u00cb\u0001\u0000\u0000\u0000\u00ce\u00d1\u0001\u0000"+
		"\u0000\u0000\u00cf\u00cd\u0001\u0000\u0000\u0000\u00cf\u00d0\u0001\u0000"+
		"\u0000\u0000\u00d0\u00d2\u0001\u0000\u0000\u0000\u00d1\u00cf\u0001\u0000"+
		"\u0000\u0000\u00d2\u00e7\u0005@\u0000\u0000\u00d3\u00d4\u0005\u000b\u0000"+
		"\u0000\u00d4\u00d5\u0005?\u0000\u0000\u00d5\u00d8\u0005;\u0000\u0000\u00d6"+
		"\u00d7\u0005\u0001\u0000\u0000\u00d7\u00d9\u0003\u0004\u0002\u0000\u00d8"+
		"\u00d6\u0001\u0000\u0000\u0000\u00d8\u00d9\u0001\u0000\u0000\u0000\u00d9"+
		"\u00da\u0001\u0000\u0000\u0000\u00da\u00e7\u0005@\u0000\u0000\u00db\u00dc"+
		"\u0005\f\u0000\u0000\u00dc\u00dd\u0005?\u0000\u0000\u00dd\u00de\u0005"+
		";\u0000\u0000\u00de\u00df\u0005\u0001\u0000\u0000\u00df\u00e2\u0003\u0004"+
		"\u0002\u0000\u00e0\u00e1\u0005\u0001\u0000\u0000\u00e1\u00e3\u0003\u0004"+
		"\u0002\u0000\u00e2\u00e0\u0001\u0000\u0000\u0000\u00e2\u00e3\u0001\u0000"+
		"\u0000\u0000\u00e3\u00e4\u0001\u0000\u0000\u0000\u00e4\u00e5\u0005@\u0000"+
		"\u0000\u00e5\u00e7\u0001\u0000\u0000\u0000\u00e6\u00b6\u0001\u0000\u0000"+
		"\u0000\u00e6\u00bd\u0001\u0000\u0000\u0000\u00e6\u00c8\u0001\u0000\u0000"+
		"\u0000\u00e6\u00d3\u0001\u0000\u0000\u0000\u00e6\u00db\u0001\u0000\u0000"+
		"\u0000\u00e7\u000b\u0001\u0000\u0000\u0000\u00e8\u00ea\u0003\u0004\u0002"+
		"\u0000\u00e9\u00eb\u0005;\u0000\u0000\u00ea\u00e9\u0001\u0000\u0000\u0000"+
		"\u00ea\u00eb\u0001\u0000\u0000\u0000\u00eb\r\u0001\u0000\u0000\u0000\u00ec"+
		"\u00ef\u0005>\u0000\u0000\u00ed\u00ef\u00030\u0018\u0000\u00ee\u00ec\u0001"+
		"\u0000\u0000\u0000\u00ee\u00ed\u0001\u0000\u0000\u0000\u00ef\u00f2\u0001"+
		"\u0000\u0000\u0000\u00f0\u00f1\u0005\r\u0000\u0000\u00f1\u00f3\u0003\u0004"+
		"\u0002\u0000\u00f2\u00f0\u0001\u0000\u0000\u0000\u00f2\u00f3\u0001\u0000"+
		"\u0000\u0000\u00f3\u000f\u0001\u0000\u0000\u0000\u00f4\u00f5\u0005\u000e"+
		"\u0000\u0000\u00f5\u00f6\u0005>\u0000\u0000\u00f6\u00f7\u0005?\u0000\u0000"+
		"\u00f7\u00f8\u0003\u0012\t\u0000\u00f8\u00f9\u0005@\u0000\u0000\u00f9"+
		"\u00fa\u0003\u0004\u0002\u0000\u00fa\u010c\u0001\u0000\u0000\u0000\u00fb"+
		"\u00fc\u0005\u000e\u0000\u0000\u00fc\u00fd\u0005>\u0000\u0000\u00fd\u00fe"+
		"\u0005C\u0000\u0000\u00fe\u0103\u0003,\u0016\u0000\u00ff\u0100\u0005F"+
		"\u0000\u0000\u0100\u0102\u0003,\u0016\u0000\u0101\u00ff\u0001\u0000\u0000"+
		"\u0000\u0102\u0105\u0001\u0000\u0000\u0000\u0103\u0101\u0001\u0000\u0000"+
		"\u0000\u0103\u0104\u0001\u0000\u0000\u0000\u0104\u0107\u0001\u0000\u0000"+
		"\u0000\u0105\u0103\u0001\u0000\u0000\u0000\u0106\u0108\u0005F\u0000\u0000"+
		"\u0107\u0106\u0001\u0000\u0000\u0000\u0107\u0108\u0001\u0000\u0000\u0000"+
		"\u0108\u0109\u0001\u0000\u0000\u0000\u0109\u010a\u0005D\u0000\u0000\u010a"+
		"\u010c\u0001\u0000\u0000\u0000\u010b\u00f4\u0001\u0000\u0000\u0000\u010b"+
		"\u00fb\u0001\u0000\u0000\u0000\u010c\u0011\u0001\u0000\u0000\u0000\u010d"+
		"\u0110\u0005\"\u0000\u0000\u010e\u0111\u0005>\u0000\u0000\u010f\u0111"+
		"\u00030\u0018\u0000\u0110\u010e\u0001\u0000\u0000\u0000\u0110\u010f\u0001"+
		"\u0000\u0000\u0000\u0111\u0113\u0001\u0000\u0000\u0000\u0112\u010d\u0001"+
		"\u0000\u0000\u0000\u0112\u0113\u0001\u0000\u0000\u0000\u0113\u0125\u0001"+
		"\u0000\u0000\u0000\u0114\u0119\u0003\u0014\n\u0000\u0115\u0116\u0005\u0001"+
		"\u0000\u0000\u0116\u0118\u0003\u0014\n\u0000\u0117\u0115\u0001\u0000\u0000"+
		"\u0000\u0118\u011b\u0001\u0000\u0000\u0000\u0119\u0117\u0001\u0000\u0000"+
		"\u0000\u0119\u011a\u0001\u0000\u0000\u0000\u011a\u0122\u0001\u0000\u0000"+
		"\u0000\u011b\u0119\u0001\u0000\u0000\u0000\u011c\u011d\u0005\u0001\u0000"+
		"\u0000\u011d\u0120\u0005\"\u0000\u0000\u011e\u0121\u0005>\u0000\u0000"+
		"\u011f\u0121\u00030\u0018\u0000\u0120\u011e\u0001\u0000\u0000\u0000\u0120"+
		"\u011f\u0001\u0000\u0000\u0000\u0121\u0123\u0001\u0000\u0000\u0000\u0122"+
		"\u011c\u0001\u0000\u0000\u0000\u0122\u0123\u0001\u0000\u0000\u0000\u0123"+
		"\u0125\u0001\u0000\u0000\u0000\u0124\u0112\u0001\u0000\u0000\u0000\u0124"+
		"\u0114\u0001\u0000\u0000\u0000\u0125\u0013\u0001\u0000\u0000\u0000\u0126"+
		"\u0129\u0005>\u0000\u0000\u0127\u0129\u00030\u0018\u0000\u0128\u0126\u0001"+
		"\u0000\u0000\u0000\u0128\u0127\u0001\u0000\u0000\u0000\u0129\u012c\u0001"+
		"\u0000\u0000\u0000\u012a\u012b\u0005\r\u0000\u0000\u012b\u012d\u0003\u0004"+
		"\u0002\u0000\u012c\u012a\u0001\u0000\u0000\u0000\u012c\u012d\u0001\u0000"+
		"\u0000\u0000\u012d\u0015\u0001\u0000\u0000\u0000\u012e\u012f\u0005\u000f"+
		"\u0000\u0000\u012f\u0130\u0003\u0004\u0002\u0000\u0130\u0017\u0001\u0000"+
		"\u0000\u0000\u0131\u0133\u0003\u001a\r\u0000\u0132\u0134\u0003\u001c\u000e"+
		"\u0000\u0133\u0132\u0001\u0000\u0000\u0000\u0133\u0134\u0001\u0000\u0000"+
		"\u0000\u0134\u0136\u0001\u0000\u0000\u0000\u0135\u0137\u0003\u001e\u000f"+
		"\u0000\u0136\u0135\u0001\u0000\u0000\u0000\u0136\u0137\u0001\u0000\u0000"+
		"\u0000\u0137\u0019\u0001\u0000\u0000\u0000\u0138\u0139\u0005\u0010\u0000"+
		"\u0000\u0139\u013a\u0003\u0004\u0002\u0000\u013a\u001b\u0001\u0000\u0000"+
		"\u0000\u013b\u013c\u0005\u0011\u0000\u0000\u013c\u013d\u0005?\u0000\u0000"+
		"\u013d\u013e\u0005>\u0000\u0000\u013e\u013f\u0005@\u0000\u0000\u013f\u0140"+
		"\u0003\u0004\u0002\u0000\u0140\u001d\u0001\u0000\u0000\u0000\u0141\u0142"+
		"\u0005\u0012\u0000\u0000\u0142\u0143\u0003\u0004\u0002\u0000\u0143\u001f"+
		"\u0001\u0000\u0000\u0000\u0144\u0145\u0005\u0013\u0000\u0000\u0145\u0146"+
		"\u0003\u0004\u0002\u0000\u0146\u0149\u0003\u0004\u0002\u0000\u0147\u0148"+
		"\u0005\u0014\u0000\u0000\u0148\u014a\u0003\u0004\u0002\u0000\u0149\u0147"+
		"\u0001\u0000\u0000\u0000\u0149\u014a\u0001\u0000\u0000\u0000\u014a!\u0001"+
		"\u0000\u0000\u0000\u014b\u014c\u0005\u0015\u0000\u0000\u014c\u014d\u0005"+
		"C\u0000\u0000\u014d\u0164\u0005D\u0000\u0000\u014e\u014f\u0005\u0015\u0000"+
		"\u0000\u014f\u0150\u0005C\u0000\u0000\u0150\u0151\u0003\u0004\u0002\u0000"+
		"\u0151\u0152\u0005\u001f\u0000\u0000\u0152\u0153\u0003\u0004\u0002\u0000"+
		"\u0153\u015b\u0001\u0000\u0000\u0000\u0154\u0155\u0005F\u0000\u0000\u0155"+
		"\u0156\u0003\u0004\u0002\u0000\u0156\u0157\u0005\u001f\u0000\u0000\u0157"+
		"\u0158\u0003\u0004\u0002\u0000\u0158\u015a\u0001\u0000\u0000\u0000\u0159"+
		"\u0154\u0001\u0000\u0000\u0000\u015a\u015d\u0001\u0000\u0000\u0000\u015b"+
		"\u0159\u0001\u0000\u0000\u0000\u015b\u015c\u0001\u0000\u0000\u0000\u015c"+
		"\u015f\u0001\u0000\u0000\u0000\u015d\u015b\u0001\u0000\u0000\u0000\u015e"+
		"\u0160\u0005F\u0000\u0000\u015f\u015e\u0001\u0000\u0000\u0000\u015f\u0160"+
		"\u0001\u0000\u0000\u0000\u0160\u0161\u0001\u0000\u0000\u0000\u0161\u0162"+
		"\u0005D\u0000\u0000\u0162\u0164\u0001\u0000\u0000\u0000\u0163\u014b\u0001"+
		"\u0000\u0000\u0000\u0163\u014e\u0001\u0000\u0000\u0000\u0164#\u0001\u0000"+
		"\u0000\u0000\u0165\u0166\u0005\u0016\u0000\u0000\u0166\u0167\u0005?\u0000"+
		"\u0000\u0167\u0168\u0005@\u0000\u0000\u0168\u017c\u0003\u0004\u0002\u0000"+
		"\u0169\u016a\u0005\u0016\u0000\u0000\u016a\u016b\u0005?\u0000\u0000\u016b"+
		"\u016c\u0003&\u0013\u0000\u016c\u016d\u0005\r\u0000\u0000\u016d\u0175"+
		"\u0003\u0004\u0002\u0000\u016e\u016f\u0005\u0001\u0000\u0000\u016f\u0170"+
		"\u0003&\u0013\u0000\u0170\u0171\u0005\r\u0000\u0000\u0171\u0172\u0003"+
		"\u0004\u0002\u0000\u0172\u0174\u0001\u0000\u0000\u0000\u0173\u016e\u0001"+
		"\u0000\u0000\u0000\u0174\u0177\u0001\u0000\u0000\u0000\u0175\u0173\u0001"+
		"\u0000\u0000\u0000\u0175\u0176\u0001\u0000\u0000\u0000\u0176\u0178\u0001"+
		"\u0000\u0000\u0000\u0177\u0175\u0001\u0000\u0000\u0000\u0178\u0179\u0005"+
		"@\u0000\u0000\u0179\u017a\u0003\u0004\u0002\u0000\u017a\u017c\u0001\u0000"+
		"\u0000\u0000\u017b\u0165\u0001\u0000\u0000\u0000\u017b\u0169\u0001\u0000"+
		"\u0000\u0000\u017c%\u0001\u0000\u0000\u0000\u017d\u0180\u0005>\u0000\u0000"+
		"\u017e\u0180\u00030\u0018\u0000\u017f\u017d\u0001\u0000\u0000\u0000\u017f"+
		"\u017e\u0001\u0000\u0000\u0000\u0180\'\u0001\u0000\u0000\u0000\u0181\u0182"+
		"\u0005\u0017\u0000\u0000\u0182\u0183\u0005?\u0000\u0000\u0183\u0196\u0005"+
		"@\u0000\u0000\u0184\u0185\u0005\u0017\u0000\u0000\u0185\u0186\u0005?\u0000"+
		"\u0000\u0186\u018b\u0003\u0004\u0002\u0000\u0187\u0188\u0005\u0001\u0000"+
		"\u0000\u0188\u018a\u0003\u0004\u0002\u0000\u0189\u0187\u0001\u0000\u0000"+
		"\u0000\u018a\u018d\u0001\u0000\u0000\u0000\u018b\u0189\u0001\u0000\u0000"+
		"\u0000\u018b\u018c\u0001\u0000\u0000\u0000\u018c\u0191\u0001\u0000\u0000"+
		"\u0000\u018d\u018b\u0001\u0000\u0000\u0000\u018e\u018f\u0005\u0001\u0000"+
		"\u0000\u018f\u0190\u0005\"\u0000\u0000\u0190\u0192\u0003\u0004\u0002\u0000"+
		"\u0191\u018e\u0001\u0000\u0000\u0000\u0191\u0192\u0001\u0000\u0000\u0000"+
		"\u0192\u0193\u0001\u0000\u0000\u0000\u0193\u0194\u0005@\u0000\u0000\u0194"+
		"\u0196\u0001\u0000\u0000\u0000\u0195\u0181\u0001\u0000\u0000\u0000\u0195"+
		"\u0184\u0001\u0000\u0000\u0000\u0196)\u0001\u0000\u0000\u0000\u0197\u01c2"+
		"\u0005:\u0000\u0000\u0198\u01c2\u0005;\u0000\u0000\u0199\u01c2\u0005\u0018"+
		"\u0000\u0000\u019a\u01c2\u0005<\u0000\u0000\u019b\u01c2\u0005=\u0000\u0000"+
		"\u019c\u01c2\u0005\u0019\u0000\u0000\u019d\u01c2\u0005\u001a\u0000\u0000"+
		"\u019e\u01c2\u0005\u001b\u0000\u0000\u019f\u01c2\u00059\u0000\u0000\u01a0"+
		"\u01a1\u0005C\u0000\u0000\u01a1\u01c2\u0005D\u0000\u0000\u01a2\u01a3\u0005"+
		"C\u0000\u0000\u01a3\u01a8\u0003.\u0017\u0000\u01a4\u01a5\u0005\u0001\u0000"+
		"\u0000\u01a5\u01a7\u0003.\u0017\u0000\u01a6\u01a4\u0001\u0000\u0000\u0000"+
		"\u01a7\u01aa\u0001\u0000\u0000\u0000\u01a8\u01a6\u0001\u0000\u0000\u0000"+
		"\u01a8\u01a9\u0001\u0000\u0000\u0000\u01a9\u01ac\u0001\u0000\u0000\u0000"+
		"\u01aa\u01a8\u0001\u0000\u0000\u0000\u01ab\u01ad\u0005\u0001\u0000\u0000"+
		"\u01ac\u01ab\u0001\u0000\u0000\u0000\u01ac\u01ad\u0001\u0000\u0000\u0000"+
		"\u01ad\u01ae\u0001\u0000\u0000\u0000\u01ae\u01af\u0005D\u0000\u0000\u01af"+
		"\u01c2\u0001\u0000\u0000\u0000\u01b0\u01b1\u0005A\u0000\u0000\u01b1\u01c2"+
		"\u0005B\u0000\u0000\u01b2\u01b3\u0005A\u0000\u0000\u01b3\u01b8\u0003\b"+
		"\u0004\u0000\u01b4\u01b5\u0005\u0001\u0000\u0000\u01b5\u01b7\u0003\b\u0004"+
		"\u0000\u01b6\u01b4\u0001\u0000\u0000\u0000\u01b7\u01ba\u0001\u0000\u0000"+
		"\u0000\u01b8\u01b6\u0001\u0000\u0000\u0000\u01b8\u01b9\u0001\u0000\u0000"+
		"\u0000\u01b9\u01bc\u0001\u0000\u0000\u0000\u01ba\u01b8\u0001\u0000\u0000"+
		"\u0000\u01bb\u01bd\u0005\u0001\u0000\u0000\u01bc\u01bb\u0001\u0000\u0000"+
		"\u0000\u01bc\u01bd\u0001\u0000\u0000\u0000\u01bd\u01be\u0001\u0000\u0000"+
		"\u0000\u01be\u01bf\u0005B\u0000\u0000\u01bf\u01c2\u0001\u0000\u0000\u0000"+
		"\u01c0\u01c2\u0003,\u0016\u0000\u01c1\u0197\u0001\u0000\u0000\u0000\u01c1"+
		"\u0198\u0001\u0000\u0000\u0000\u01c1\u0199\u0001\u0000\u0000\u0000\u01c1"+
		"\u019a\u0001\u0000\u0000\u0000\u01c1\u019b\u0001\u0000\u0000\u0000\u01c1"+
		"\u019c\u0001\u0000\u0000\u0000\u01c1\u019d\u0001\u0000\u0000\u0000\u01c1"+
		"\u019e\u0001\u0000\u0000\u0000\u01c1\u019f\u0001\u0000\u0000\u0000\u01c1"+
		"\u01a0\u0001\u0000\u0000\u0000\u01c1\u01a2\u0001\u0000\u0000\u0000\u01c1"+
		"\u01b0\u0001\u0000\u0000\u0000\u01c1\u01b2\u0001\u0000\u0000\u0000\u01c1"+
		"\u01c0\u0001\u0000\u0000\u0000\u01c2+\u0001\u0000\u0000\u0000\u01c3\u01c9"+
		"\u0005>\u0000\u0000\u01c4\u01c5\u0005?\u0000\u0000\u01c5\u01c6\u0003\u0012"+
		"\t\u0000\u01c6\u01c7\u0005@\u0000\u0000\u01c7\u01c9\u0001\u0000\u0000"+
		"\u0000\u01c8\u01c3\u0001\u0000\u0000\u0000\u01c8\u01c4\u0001\u0000\u0000"+
		"\u0000\u01c9\u01ca\u0001\u0000\u0000\u0000\u01ca\u01cb\u0005\u001c\u0000"+
		"\u0000\u01cb\u01cc\u0003\u0004\u0002\u0000\u01cc-\u0001\u0000\u0000\u0000"+
		"\u01cd\u01ce\u0005>\u0000\u0000\u01ce\u01cf\u0005\u001d\u0000\u0000\u01cf"+
		"\u01dc\u0003\u0004\u0002\u0000\u01d0\u01d1\u0005;\u0000\u0000\u01d1\u01d2"+
		"\u0005\u001d\u0000\u0000\u01d2\u01dc\u0003\u0004\u0002\u0000\u01d3\u01d4"+
		"\u0005?\u0000\u0000\u01d4\u01d5\u0003\u0004\u0002\u0000\u01d5\u01d6\u0005"+
		"@\u0000\u0000\u01d6\u01d7\u0005\u001d\u0000\u0000\u01d7\u01d8\u0003\u0004"+
		"\u0002\u0000\u01d8\u01dc\u0001\u0000\u0000\u0000\u01d9\u01da\u0005\"\u0000"+
		"\u0000\u01da\u01dc\u0003\u0004\u0002\u0000\u01db\u01cd\u0001\u0000\u0000"+
		"\u0000\u01db\u01d0\u0001\u0000\u0000\u0000\u01db\u01d3\u0001\u0000\u0000"+
		"\u0000\u01db\u01d9\u0001\u0000\u0000\u0000\u01dc/\u0001\u0000\u0000\u0000"+
		"\u01dd\u01e0\u00032\u0019\u0000\u01de\u01e0\u00034\u001a\u0000\u01df\u01dd"+
		"\u0001\u0000\u0000\u0000\u01df\u01de\u0001\u0000\u0000\u0000\u01e01\u0001"+
		"\u0000\u0000\u0000\u01e1\u01e7\u0005A\u0000\u0000\u01e2\u01e5\u0005\""+
		"\u0000\u0000\u01e3\u01e6\u0005>\u0000\u0000\u01e4\u01e6\u00032\u0019\u0000"+
		"\u01e5\u01e3\u0001\u0000\u0000\u0000\u01e5\u01e4\u0001\u0000\u0000\u0000"+
		"\u01e6\u01e8\u0001\u0000\u0000\u0000\u01e7\u01e2\u0001\u0000\u0000\u0000"+
		"\u01e7\u01e8\u0001\u0000\u0000\u0000\u01e8\u01ea\u0001\u0000\u0000\u0000"+
		"\u01e9\u01eb\u00038\u001c\u0000\u01ea\u01e9\u0001\u0000\u0000\u0000\u01ea"+
		"\u01eb\u0001\u0000\u0000\u0000\u01eb\u01ec\u0001\u0000\u0000\u0000\u01ec"+
		"\u0204\u0005B\u0000\u0000\u01ed\u01ee\u0005A\u0000\u0000\u01ee\u01f3\u0003"+
		"\u0014\n\u0000\u01ef\u01f0\u0005\u0001\u0000\u0000\u01f0\u01f2\u0003\u0014"+
		"\n\u0000\u01f1\u01ef\u0001\u0000\u0000\u0000\u01f2\u01f5\u0001\u0000\u0000"+
		"\u0000\u01f3\u01f1\u0001\u0000\u0000\u0000\u01f3\u01f4\u0001\u0000\u0000"+
		"\u0000\u01f4\u01fc\u0001\u0000\u0000\u0000\u01f5\u01f3\u0001\u0000\u0000"+
		"\u0000\u01f6\u01f7\u0005\u0001\u0000\u0000\u01f7\u01fa\u0005\"\u0000\u0000"+
		"\u01f8\u01fb\u0005>\u0000\u0000\u01f9\u01fb\u00032\u0019\u0000\u01fa\u01f8"+
		"\u0001\u0000\u0000\u0000\u01fa\u01f9\u0001\u0000\u0000\u0000\u01fb\u01fd"+
		"\u0001\u0000\u0000\u0000\u01fc\u01f6\u0001\u0000\u0000\u0000\u01fc\u01fd"+
		"\u0001\u0000\u0000\u0000\u01fd\u01ff\u0001\u0000\u0000\u0000\u01fe\u0200"+
		"\u00038\u001c\u0000\u01ff\u01fe\u0001\u0000\u0000\u0000\u01ff\u0200\u0001"+
		"\u0000\u0000\u0000\u0200\u0201\u0001\u0000\u0000\u0000\u0201\u0202\u0005"+
		"B\u0000\u0000\u0202\u0204\u0001\u0000\u0000\u0000\u0203\u01e1\u0001\u0000"+
		"\u0000\u0000\u0203\u01ed\u0001\u0000\u0000\u0000\u02043\u0001\u0000\u0000"+
		"\u0000\u0205\u020b\u0005C\u0000\u0000\u0206\u0209\u0005\"\u0000\u0000"+
		"\u0207\u020a\u0005>\u0000\u0000\u0208\u020a\u00034\u001a\u0000\u0209\u0207"+
		"\u0001\u0000\u0000\u0000\u0209\u0208\u0001\u0000\u0000\u0000\u020a\u020c"+
		"\u0001\u0000\u0000\u0000\u020b\u0206\u0001\u0000\u0000\u0000\u020b\u020c"+
		"\u0001\u0000\u0000\u0000\u020c\u020e\u0001\u0000\u0000\u0000\u020d\u020f"+
		"\u00038\u001c\u0000\u020e\u020d\u0001\u0000\u0000\u0000\u020e\u020f\u0001"+
		"\u0000\u0000\u0000\u020f\u0210\u0001\u0000\u0000\u0000\u0210\u0228\u0005"+
		"D\u0000\u0000\u0211\u0212\u0005C\u0000\u0000\u0212\u0217\u00036\u001b"+
		"\u0000\u0213\u0214\u0005\u0001\u0000\u0000\u0214\u0216\u00036\u001b\u0000"+
		"\u0215\u0213\u0001\u0000\u0000\u0000\u0216\u0219\u0001\u0000\u0000\u0000"+
		"\u0217\u0215\u0001\u0000\u0000\u0000\u0217\u0218\u0001\u0000\u0000\u0000"+
		"\u0218\u0220\u0001\u0000\u0000\u0000\u0219\u0217\u0001\u0000\u0000\u0000"+
		"\u021a\u021b\u0005\u0001\u0000\u0000\u021b\u021e\u0005\"\u0000\u0000\u021c"+
		"\u021f\u0005>\u0000\u0000\u021d\u021f\u00034\u001a\u0000\u021e\u021c\u0001"+
		"\u0000\u0000\u0000\u021e\u021d\u0001\u0000\u0000\u0000\u021f\u0221\u0001"+
		"\u0000\u0000\u0000\u0220\u021a\u0001\u0000\u0000\u0000\u0220\u0221\u0001"+
		"\u0000\u0000\u0000\u0221\u0223\u0001\u0000\u0000\u0000\u0222\u0224\u0003"+
		"8\u001c\u0000\u0223\u0222\u0001\u0000\u0000\u0000\u0223\u0224\u0001\u0000"+
		"\u0000\u0000\u0224\u0225\u0001\u0000\u0000\u0000\u0225\u0226\u0005D\u0000"+
		"\u0000\u0226\u0228\u0001\u0000\u0000\u0000\u0227\u0205\u0001\u0000\u0000"+
		"\u0000\u0227\u0211\u0001\u0000\u0000\u0000\u02285\u0001\u0000\u0000\u0000"+
		"\u0229\u022c\u0005>\u0000\u0000\u022a\u022b\u0005\u001d\u0000\u0000\u022b"+
		"\u022d\u0003\u0014\n\u0000\u022c\u022a\u0001\u0000\u0000\u0000\u022c\u022d"+
		"\u0001\u0000\u0000\u0000\u022d7\u0001\u0000\u0000\u0000\u022e\u022f\u0005"+
		"\u0007\u0000\u0000\u022f\u0230\u0005>\u0000\u0000\u02309\u0001\u0000\u0000"+
		"\u0000CAEcf\u0088\u0092\u0098\u00a4\u00a6\u00b2\u00bb\u00c4\u00cf\u00d8"+
		"\u00e2\u00e6\u00ea\u00ee\u00f2\u0103\u0107\u010b\u0110\u0112\u0119\u0120"+
		"\u0122\u0124\u0128\u012c\u0133\u0136\u0149\u015b\u015f\u0163\u0175\u017b"+
		"\u017f\u018b\u0191\u0195\u01a8\u01ac\u01b8\u01bc\u01c1\u01c8\u01db\u01df"+
		"\u01e5\u01e7\u01ea\u01f3\u01fa\u01fc\u01ff\u0203\u0209\u020b\u020e\u0217"+
		"\u021e\u0220\u0223\u0227\u022c";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}