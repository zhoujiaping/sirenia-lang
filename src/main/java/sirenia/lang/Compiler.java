package sirenia.lang;

import io.vavr.Tuple2;
import io.vavr.collection.*;
import io.vavr.control.Either;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.commons.Method;
import sirenia.SireniaBaseVisitor;
import sirenia.SireniaLexer;
import sirenia.SireniaParser;
import sirenia.asm.AsmUtil;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import static org.objectweb.asm.Opcodes.*;

/**
 * TODO
 * 各种字面量
 * 各种运算符
 * 变量作用域?
 * 管道运算符
 * 基本类型？
 * 数组？
 * 多线程？
 * 标准库?
 * 正则？
 * 对象系统？
 * 惰性求值？
 *
 * @author zhoujiaping
 */
public class Compiler extends SireniaBaseVisitor<Object> {
    public static final Var CLASS_LOADER = Var.create(new DynamicClassLoader());
    //private static final Var LOCAL_ENV = Var.create(LocalEnv.TOP);
    private static final Var SCOPE = Var.create(null);
    private static final Var CLASSES_DIR = Var.create("/gitlab-repo/sirenia-lang/target/classes");
    private static final Var WRITE_CLASS_FILE = Var.create(true);
    private static final Var FN_METHOD = Var.create(null);
    //localbinding   current destruct
    private static final Var DESTRUCT = Var.create(null);
    private static final Var LOOP_ARGS = Var.create(null);
    private static final Var LOOP_LABEL = Var.create(null);
    private static final Map<String, String> defaultImports = HashMap.of("Object", "java.lang.Object")
            .put("RT", "sirenia.lang.RT");//TODO
    private static final Var IMPORTS = Var.create(defaultImports);

    private static final Vector<java.lang.reflect.Method> FN_INVOKES;

    private static final java.lang.reflect.Method FN_APPLY_TO;

    //PathNode chain
    static final public Var CLEAR_PATH = Var.create(null);

    //tail of PathNode chain
    static final public Var CLEAR_ROOT = Var.create(null);

    //LocalBinding -> Set<Expr> LocalBindingExpr/AssignExpr
    static final public Var CLEAR_SITES = Var.create(null);

    //static final public Var FN_EXPR = Var.create(null);
    static java.util.Map categoryOpsCache = new ConcurrentHashMap();

    static final public Var CATEGORY = Var.create(createCategory(null, "sirenia.lang.RT"));

    static {
        //invoke()
        //invoke(Object arg1)
        //invoke(Object arg1,Object arg2)
        //...
        //invoke(Object arg1,Object arg2,...,Object arg20,Object[] arg21)
        FN_INVOKES = Vector.of(Fn.class.getMethods())
                .filter(it -> it.getName().equals("invoke"))
                .sortBy(it -> it.getParameterCount());

        FN_APPLY_TO = Vector.of(Fn.class.getMethods())
                .find(it -> it.getName().equals("applyTo")).get();
    }

    public static String resolveAlias(String maybeAlias) {
        Map<String, String> imports = IMPORTS.deref();
        return imports.getOrElse(maybeAlias, maybeAlias);
    }

    public static Object eval(String moduleName, String code) {
        Thread.currentThread().setContextClassLoader(CLASS_LOADER.deref());
        String className = moduleName;
        FnExpr fnExpr = new FnExpr(className, className, className);
        FnMethod method = new FnMethod(null, false);
        fnExpr.addFnMethod(method);

        Compiler compiler = new Compiler();
        PathNode pathNode = new PathNode(PATHTYPE.PATH, null);
        ScopeExpr scopeExpr = new ScopeExpr(SCOPE.deref());
        try {
            Var.pushThreadBindings(HashMap.of(
                    SCOPE, scopeExpr,
                    FN_METHOD, method,
                    CLEAR_PATH, pathNode,
                    CLEAR_ROOT, pathNode,
                    CLEAR_SITES, HashMap.empty()));
            SireniaParser.FileContext tree = parse(code);
            fnExpr.setLine(tree.start.getLine());
            scopeExpr.appendBody((Expr) compiler.visit(tree));
            method.body = scopeExpr;
            Fn f = fnExpr.eval();
            return f.invoke();
        } finally {
            Var.popThreadBindings();
        }
    }
    static CommonTokenStream TOKENS;
    private static SireniaParser.FileContext parse(String code) {
        CodePointCharStream charStream = CharStreams.fromString(code);
        SireniaLexer lexer = new SireniaLexer(charStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        TOKENS = tokens;

        SireniaParser parser = new SireniaParser(tokens);
        //报告所有歧义
        parser.getInterpreter().setPredictionMode(PredictionMode.LL_EXACT_AMBIG_DETECTION);
        //parser.removeErrorListeners();
        parser.addErrorListener(new DiagnosticErrorListener());
        parser.setErrorHandler(new BailErrorStrategy());
        return parser.file();
    }


    /**
     * fnParams : LPAREN (EXPAND (ID|structParams))? RPAREN
     * | ID
     * | LPAREN paramsD (',' paramsD)* (',' EXPAND (ID|structParams))? RPAREN;
     * <p>
     * paramsD : (ID|structParams) ('=' expr)? ;
     */
    @Override
    public Object visitFnLiteral(SireniaParser.FnLiteralContext ctx) {
        int startLine = ctx.getStart().getLine();
        FnMethod closingMethod = FN_METHOD.deref();
        String fnName = RT.genName("fn");
        FnExpr fn = new FnExpr(fnName, closingMethod.objx.fullClassName + "$" + fnName, fnName)
                .setLine(startLine);
        parseFnMethod(fn,
                ctx.ID() != null ?
                        Either.left(ctx.ID().getText()) :
                        Either.right(ctx.fnParams()), ctx.expr())
                .setLine(startLine);
        return fnInstancingExpr(startLine,fn);
    }

    @Override
    public Object visitDefnExpr1(SireniaParser.DefnExpr1Context ctx) {
        int line = ctx.start.getLine();
        FnMethod closingMethod = FN_METHOD.deref();
        String fnName = ctx.ID().getText();
        FnExpr fn = new FnExpr(fnName, closingMethod.objx.fullClassName + "$" + fnName, fnName)
                .setLine(line);
        parseFnMethod(fn, Either.right(ctx.fnParams()), ctx.expr())
                .setLine(line);
        return createDefExpr(fnName,()->fnInstancingExpr(line,fn));
    }

    @Override
    public Object visitDefnExprN(SireniaParser.DefnExprNContext ctx) {
        int line = ctx.start.getLine();
        FnMethod closingMethod = FN_METHOD.deref();
        String fnName = ctx.ID().getText();
        FnExpr fn = new FnExpr(fnName, closingMethod.objx.fullClassName + "$" + fnName, fnName)
                .setLine(line);
        Vector.ofAll(ctx.fnLiteral()).forEach(f ->
                parseFnMethod(fn, f.ID() != null ?
                        Either.left(f.ID().getText()) :
                        Either.right(f.fnParams()), f.expr())
                        .setLine(f.start.getLine()));
        return createDefExpr(fnName,()->fnInstancingExpr(line,fn));
    }

    private FnMethod parseFnMethod(FnExpr fn, Either<String, SireniaParser.FnParamsContext> fnParams, SireniaParser.ExprContext fnBody) {
        FnMethod closingMethod = FN_METHOD.deref();
        FnMethod method = new FnMethod(closingMethod, fnParams.isRight() && fnParams.get().EXPAND() != null);
        fn.addFnMethod(method);
        ScopeExpr scopeExpr = new ScopeExpr(SCOPE.deref());
        try {
            Var.pushThreadBindings(HashMap.of(
                    SCOPE, scopeExpr,
                    FN_METHOD, method,
                    CLEAR_SITES, HashMap.empty()));
            //单参，无括号、无默认值版本
            if (fnParams.isLeft()) {
                Sym argSym = new Sym(fnParams.getLeft());
                LocalBinding argLb = registerLocal(argSym, BindingType.METHOD_ARG);
                method.addArg(argLb);
            } else {//参数部分有括号版本，可能有默认值
                Vector.ofAll(fnParams.get().paramsD()).forEachWithIndex((pd, i) -> {
                    String argName = pd.ID() != null ? pd.ID().getText() : RT.genName("p" + i);
                    Sym argSym = new Sym(argName);
                    LocalBinding argLb = registerLocal(argSym, BindingType.METHOD_ARG);
                    method.addArg(argLb);
                    if (pd.expr() != null) {//有默认值
                        scopeExpr.appendBody(new AssignExpr(argLb,
                                new OrElseExpr(
                                        new LocalBindingExpr(argLb),
                                        (Expr) visit(pd.expr()))));
                    }
                    if (pd.structParams() != null) {
                        try {
                            Var.pushThreadBindings(HashMap.of(DESTRUCT, argLb));
                            scopeExpr.appendBody((Expr) visit(pd.structParams()));
                        } finally {
                            Var.popThreadBindings();
                        }
                    }
                });
                if (fnParams.get().EXPAND() != null) {
                    String restName = fnParams.get().ID() != null ? fnParams.get().ID().getText() : RT.genName("r");
                    Sym argSym = new Sym(restName);
                    LocalBinding argLb = registerLocal(argSym, BindingType.METHOD_ARG);
                    method.addArg(argLb);
                    if (fnParams.get().structParams() != null) {
                        try {
                            Var.pushThreadBindings(HashMap.of(DESTRUCT, argLb));
                            scopeExpr.appendBody((Expr) visit(fnParams.get().structParams()));
                        } finally {
                            Var.popThreadBindings();
                        }
                    }
                }
            }
            try {
                Var.pushThreadBindings(HashMap.of(LOOP_ARGS, method.args));
                LocalBinding thisFn = registerLocal(new Sym(fn.name), BindingType.THIS);
                thisFn.idx = 0;
                thisFn.autoClear = false;
                scopeExpr.appendBody((Expr) visit(fnBody));
            } finally {
                Var.popThreadBindings();
            }
            method.body = scopeExpr;
            return method;
        } finally {
            Var.popThreadBindings();
        }
    }

    private static Expr fnInstancingExpr(int line, FnExpr fn) {
        //TODO compile的时机不对
        Seq ctorArgs = Vector.ofAll(fn.closes.map(lb -> Param.of(new LocalBindingExpr(lb), null)));
        return new NewObjExpr(() ->
                fn.getCompiledClass()
                , fn.fullClassName, ctorArgs).setLine(line);
    }

    @Override
    public Object visitFnCallExpr(SireniaParser.FnCallExprContext ctx) {
        Expr fn = ((Expr) visit(ctx.expr(0)));
        //第一个expr是函数，后面的expr列表是参数
        Vector<Expr> args = Vector.ofAll(ctx.expr()).drop(1).map(exprCtx -> (Expr) visit(exprCtx));
        //是否有参数展开
        if (ctx.EXPAND() == null) {
            return new FnCallExpr(fn, args).setLine(ctx.start.getLine());
        } else {
            return new FnCallExpr(fn, args.dropRight(1), args.last()).setLine(ctx.start.getLine());
        }
    }

    /**
     * loopExpr: 'loop' LPAREN RPAREN expr
     * | 'loop' LPAREN loopParam '=' expr (',' loopParam '=' expr)* RPAREN expr
     * ;
     * loopParam:ID|structParams;
     */
    @Override
    public Object visitLoopExpr(SireniaParser.LoopExprContext ctx) {
        LoopExpr loopExpr = new LoopExpr(SCOPE.deref())
                .setLine(ctx.start.getLine());
        PathNode looproot = new PathNode(PATHTYPE.PATH, CLEAR_PATH.deref());
        PathNode clearroot = new PathNode(PATHTYPE.PATH, looproot);
        PathNode clearpath = new PathNode(PATHTYPE.PATH, looproot);
        //PathNode pathNode = new PathNode(PATHTYPE.PATH, CLEAR_PATH.deref());
        try {
            Var.pushThreadBindings(HashMap.of(
                    SCOPE, loopExpr,
                    CLEAR_PATH, clearpath,
                    CLEAR_ROOT, clearroot));
            Vector.ofAll(ctx.loopParam()).forEachWithIndex((p, i) -> {
                String argName = p.ID() != null ? p.ID().getText() : RT.genName("p" + i);
                registerLocal(new Sym(argName), BindingType.LOOP_ARG);
                loopExpr.inits = loopExpr.inits.append((Expr) visit(ctx.expr(i)));
            });
            Vector<LocalBinding> loopArgs = loopExpr.lbs.toVector();
            ScopeExpr loopBody = new ScopeExpr(loopExpr);
            try {
                Var.pushThreadBindings(HashMap.of(
                        SCOPE, loopBody,
                        LOOP_ARGS, loopExpr.lbs));
                Vector.ofAll(ctx.loopParam()).forEachWithIndex((p, i) -> {
                    if (p.structParams() != null) {
                        try {
                            Var.pushThreadBindings(HashMap.of(DESTRUCT, loopArgs.get(i)));
                            loopBody.appendBody((Expr) visit(p.structParams()));
                        } finally {
                            Var.popThreadBindings();
                        }
                    }
                });
                loopBody.appendBody((Expr) visit(ctx.expr(ctx.expr().size() - 1)));
            } finally {
                Var.popThreadBindings();
            }
            loopExpr.body = loopBody;
            return loopExpr;
        } finally {
            Var.popThreadBindings();
        }
    }

    @Override
    public Object visitRecurExpr(SireniaParser.RecurExprContext ctx) {
        RecurExpr recur = new RecurExpr()
                .setLine(ctx.start.getLine());
        Set<LocalBinding> loopArgs = LOOP_ARGS.deref();
        int loopArgCount = loopArgs.length();
        int recurParamCount = ctx.expr().size();
        if (loopArgCount != recurParamCount) {
            throw new RuntimeException(String.format("recur arg size = %s, but expect %s", recurParamCount, loopArgCount));
        }
        loopArgs.forEachWithIndex((argLb, i) -> {
            recur.appendExpr(new AssignExpr(argLb, (Expr) visit(ctx.expr(i))));
        });
        return recur;
    }

    @Override
    public Object visitTryCatchExpr(SireniaParser.TryCatchExprContext ctx) {
        ScopeExpr scope = SCOPE.deref();
        TryCatchExpr tryCatchExpr = new TryCatchExpr(scope)
                .setLine(ctx.start.getLine());
        tryCatchExpr.tryExpr = new ScopeExpr(tryCatchExpr);
        try {
            Var.pushThreadBindings(HashMap.of(SCOPE, tryCatchExpr));
            try {
                Var.pushThreadBindings(HashMap.of(SCOPE, tryCatchExpr.tryExpr));
                tryCatchExpr.tryExpr.appendBody((Expr) visit(ctx.tryBlock().expr()));
            } finally {
                Var.popThreadBindings();
            }
            if (ctx.catchBlock() != null) {
                String exName = ctx.catchBlock().ID().getText();
                tryCatchExpr.catchExpr = new ScopeExpr(tryCatchExpr);
                tryCatchExpr.catchLb(registerLocal(new Sym(exName)));
                try {
                    Var.pushThreadBindings(HashMap.of(SCOPE, tryCatchExpr.catchExpr));
                    Expr catchBody = (Expr) visit(ctx.catchBlock().expr());
                    tryCatchExpr.catchExpr.appendBody(catchBody);
                } finally {
                    Var.popThreadBindings();
                }
            }
            if (ctx.finallyBlock() != null) {
                tryCatchExpr.finallyExpr = new ScopeExpr(tryCatchExpr);
                try {
                    Var.pushThreadBindings(HashMap.of(SCOPE, tryCatchExpr.finallyExpr));
                    Expr finallyBody = (Expr) visit(ctx.finallyBlock().expr());
                    tryCatchExpr.finallyExpr.appendBody(finallyBody);
                } finally {
                    Var.popThreadBindings();
                }
            }
        } finally {
            Var.popThreadBindings();
        }
        return tryCatchExpr;
    }

    @Override
    public Object visitParenExpr(SireniaParser.ParenExprContext ctx) {
        return visit(ctx.exprs());
    }

    @Override
    public Object visitScopeExpr(SireniaParser.ScopeExprContext ctx) {
        ScopeExpr scope = new ScopeExpr(SCOPE.deref());
        try {
            Var.pushThreadBindings(HashMap.of(SCOPE, scope));
            return scope.appendBody((Expr) visit(ctx.exprs()));
        } finally {
            Var.popThreadBindings();
        }
    }

    @Override
    public Object visitImportClass(SireniaParser.ImportClassContext ctx) {
        Map imports = IMPORTS.deref();
        String className = parseQuotedStr(ctx.STRING(0).getText());
        try {
            Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw Util.sneakyThrow(e);
        }
        String as;
        if (ctx.STRING().size() == 1)//none as
            as = Util.simpleClassName(className);
        else
            as = ctx.STRING(1).getText();
        IMPORTS.set(imports.put(className, as));
        return CONSTANT_NULL_EXPR;
    }

    @Override
    public Object visitNewObjExpr(SireniaParser.NewObjExprContext ctx) {
        String className = parseQuotedStr(ctx.STRING().getText());
        Vector args = Vector.ofAll(ctx.typedExpr()).map(it ->
                Param.of((Expr) visit(it.expr()), Util.nullOrMap(ctx.STRING(), s -> s.getText())));
        return new NewObjExpr(null, className, args)
                .setLine(ctx.start.getLine());
    }

    @Override
    public Object visitMethodCallExpr(SireniaParser.MethodCallExprContext ctx) {
        boolean isStatic = ctx.getChild(0).getText().equals("#");
        Tuple2<String, String> cnAndMn = split2(parseQuotedStr(ctx.STRING().getText()));
        Vector args = Vector.ofAll(ctx.typedExpr()).map(it ->
                Param.of((Expr) visit(it.expr()), Util.nullOrMap(it.STRING(), s -> parseQuotedStr(s.getText()))));
        return new MethodCallExpr(isStatic, cnAndMn._1, cnAndMn._2, args)
                .setLine(ctx.start.getLine());
    }

    private Tuple2<String, String> split2(String text) {
        int idx = text.indexOf('/');
        return new Tuple2<>(text.substring(0, idx), text.substring(idx + 1));
    }

    @Override
    public Object visitFieldGetExpr(SireniaParser.FieldGetExprContext ctx) {
        Tuple2<String, String> t2 = split2(parseQuotedStr(ctx.STRING().getText()));
        if (ctx.expr() == null)
            return FieldAccessExpr.forGetStatic(t2._1, t2._2)
                    .setLine(ctx.start.getLine());
        else
            return FieldAccessExpr.forGetMember(t2._1, t2._2, (Expr) visit(ctx.expr()))
                    .setLine(ctx.start.getLine());
    }

    @Override
    public Object visitFieldSetExpr(SireniaParser.FieldSetExprContext ctx) {
        Tuple2<String, String> t2 = split2(parseQuotedStr(ctx.STRING().getText()));
        if (ctx.expr().size() == 1)
            return FieldAccessExpr.forPutStatic(t2._1, t2._2, (Expr) visit(ctx.expr(0)))
                    .setLine(ctx.start.getLine());
        else
            return FieldAccessExpr.forPutMember(t2._1, t2._2, (Expr) visit(ctx.expr(0)), (Expr) visit(ctx.expr(1)))
                    .setLine(ctx.start.getLine());
    }

    @Override
    public Object visitNullLiteral(SireniaParser.NullLiteralContext ctx) {
        return CONSTANT_NULL_EXPR;
    }

    @Override
    public Object visitZeroLiteral(SireniaParser.ZeroLiteralContext ctx) {
        return NumberExpr.ZERO;
    }

    @Override
    public Object visitStrLiteral(SireniaParser.StrLiteralContext ctx) {
        return new StringLiteralExpr(parseQuotedStr(ctx.getText()));
    }

    private String parseQuotedStr(String text) {
        return Util.unescapeJava(Util.trimN(text, 1));
    }

    @Override
    public Object visitExprs(SireniaParser.ExprsContext ctx) {
        ParenExpr parenExpr = new ParenExpr();
        ctx.expr().forEach(it -> parenExpr.appendExpr((Expr) visit(it)));
        return parenExpr;
    }

    @Override
    public Object visitIdAttrExpr(SireniaParser.IdAttrExprContext ctx) {
        return rt("get", (Expr) visit(ctx.expr()), new StringLiteralExpr(ctx.ID().getText()))
                .setLine(ctx.start.getLine());
    }

    @Override
    public Object visitExprAttrExpr(SireniaParser.ExprAttrExprContext ctx) {
        return rt("get", (Expr) visit(ctx.expr(0)), (Expr) visit(ctx.expr(1)))
                .setLine(ctx.start.getLine());
    }

    @Override
    public Object visitMaybeExpandExpr(SireniaParser.MaybeExpandExprContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Object visitVecLiteral(SireniaParser.VecLiteralContext ctx) {
        Vector<SireniaParser.MaybeExpandExprContext> ctxs = Vector.ofAll(ctx.maybeExpandExpr());
        boolean hasExpand = ctxs.exists(it -> it.EXPAND() != null);
        if (hasExpand) {
            //idx = hashset(...)
            //vectorExpand(items,idx)
            NewArrayExpr itemsExpr = new NewArrayExpr(ctxs.map(c -> (Expr) visit(c)));
            NewArrayExpr expandIdx = new NewArrayExpr();
            ctxs.forEachWithIndex((c, i) -> {
                if (c.EXPAND() != null) expandIdx.addItem(NumberExpr.of(i));
            });
            Expr idxExpr = rt("hashset", expandIdx);
            return rt("vectorExpand", itemsExpr, idxExpr)
                    .setLine(ctx.start.getLine());
        } else {
            return new VecExpr(ctxs.map(it -> (Expr) visit(it)))
                    .setLine(ctx.start.getLine());
        }
    }

    @Override
    public Object visitIntLiteral(SireniaParser.IntLiteralContext ctx) {
        String text = ctx.INT().getText();
        Number n = Maths.parseJavaLong(text);
        return new NumberExpr(n);
    }

//    @Override
//    public Object visitAssignExpr(SireniaParser.AssignExprContext ctx) {
//        Expr initExpr = (Expr) visit(ctx.expr());
//        ScopeExpr scope = SCOPE.deref();
//        Sym sym = new Sym(ctx.ID().getText());
//        FnMethod method = FN_METHOD.deref();
//        LocalBinding lb = scope.findBindingR(sym);
//        if (method.objx.closes.contains(lb)) {
//            throw new RuntimeException("can not assignment free variables:" + lb.sym);
//        }
//        return new AssignExpr(lb, initExpr);
//    }

    @Override
    public Object visitFloatLiteral(SireniaParser.FloatLiteralContext ctx) {
        String text = ctx.FLOAT().getText();
        Number n = Maths.parseJavaDouble(text);
        return new NumberExpr(n);
    }

    /**
     * 'def' defItem (',' defItem)* # DefExpr
     * <p>
     * defItem: (ID|structParams) ('=' expr)?;
     */
    @Override
    public Object visitDefExpr(SireniaParser.DefExprContext ctx) {
        int line = ctx.start.getLine();
        ParenExpr parenExpr = new ParenExpr();
        Vector.ofAll(ctx.defItem()).forEachWithIndex((item, i) -> {
            String name = item.ID() != null ? item.ID().getText() : RT.genName("x");
            DefExpr defExpr = createDefExpr(name,()->item.expr() == null ? CONSTANT_NULL_EXPR : (Expr) visit(item.expr()))
                    .setLine(line);
            parenExpr.appendExpr(defExpr);
            if (item.structParams() != null) {
                try {
                    Var.pushThreadBindings(HashMap.of(DESTRUCT, defExpr.binding));
                    parenExpr.appendExpr((Expr) visit(item.structParams()));
                } finally {
                    Var.popThreadBindings();
                }
            }
        });
        return parenExpr;
    }

    @Override
    public Object visitCategoryExpr(SireniaParser.CategoryExprContext ctx) {
        String categoryClassName = parseQuotedStr(ctx.STRING().getText());
        try {
            Var.pushThreadBindings(HashMap.of(CATEGORY, createCategory(CATEGORY.deref(), categoryClassName)));
            return visit(ctx.exprs());
        } finally {
            Var.popThreadBindings();
        }
    }

    static Category createCategory(Category parent, String categoryClassName) {
        String fullName = resolveAlias(categoryClassName);
        Map binOps = (Map) categoryOpsCache.get(fullName + "#binOps");
        Map preOps = (Map) categoryOpsCache.get(fullName + "#preOps");
        if (binOps != null) {
            return new Category(parent, binOps, preOps);
        }
        Class categoryClass = Util.forName(fullName);

        Vector<java.lang.reflect.Method> methods = Vector.ofAll(Arrays.asList(categoryClass.getMethods()));
        binOps = methods.filter(it -> it.isAnnotationPresent(BinOp.class))
                .toMap(it -> it.getAnnotation(BinOp.class).value(), it -> it);

        preOps = methods.filter(it -> it.isAnnotationPresent(PreOp.class))
                .toMap(it -> it.getAnnotation(PreOp.class).value(), it -> it);

        categoryOpsCache.put(fullName + "#preOps", preOps);
        categoryOpsCache.put(fullName + "#binOps", binOps);
        return new Category(parent, binOps, preOps);
    }

    @Override
    public Object visitPreExpr(SireniaParser.PreExprContext ctx) {
        return new PreExpr(ctx.op.getText(), (Expr) visit(ctx.expr()))
                .setLine(ctx.start.getLine());
    }

    @Override
    public Object visitBinExpr(SireniaParser.BinExprContext ctx) {
        return new BinExpr(ctx.op.getText(), (Expr) visit(ctx.expr(0)), (Expr) visit(ctx.expr(1)))
                .setLine(ctx.start.getLine());
    }

    @Override
    public Object visitIfExpr(SireniaParser.IfExprContext ctx) {
        int line = ctx.start.getLine();
        return createIfExpr(() -> (Expr) visit(ctx.expr(0)),
                () -> (Expr) visit(ctx.expr(1)),
                () -> ctx.expr().size() == 3 ? (Expr) visit(ctx.expr(2)) : CONSTANT_NULL_EXPR)
                .setLine(line);
    }

    private IfExpr createIfExpr(Supplier<Expr> test, Supplier<Expr> thenBody, Supplier<Expr> elseBody) {
        ScopeExpr scope = SCOPE.deref();
        Expr testExpr = test.get();
        ScopeExpr thenExpr = new ScopeExpr(SCOPE.deref());
        PathNode branch = new PathNode(PATHTYPE.BRANCH, CLEAR_PATH.deref());
        Map<LocalBinding, Set<ClearSupportExpr>> oldSites = CLEAR_SITES.deref();
        Map<LocalBinding, Set<ClearSupportExpr>> sitesAfterThen;
        Map<LocalBinding, Set<ClearSupportExpr>> sitesAfterElse;

        try {
            Var.pushThreadBindings(HashMap.of(
                    SCOPE, thenExpr,
                    CLEAR_PATH, new PathNode(PATHTYPE.PATH, branch)));
            thenExpr.appendBody(thenBody.get());
            sitesAfterThen = CLEAR_SITES.deref();
        } finally {
            Var.popThreadBindings();
        }
        ScopeExpr elseExpr = new ScopeExpr(SCOPE.deref());
        if (elseBody == null) {//no else
            sitesAfterElse = HashMap.empty();
            elseExpr.appendBody(CONSTANT_NULL_EXPR);
        } else {
            try {
                Var.pushThreadBindings(HashMap.of(
                        SCOPE, elseExpr,
                        CLEAR_PATH, new PathNode(PATHTYPE.PATH, branch)));
                elseExpr.appendBody(elseBody.get());
                sitesAfterElse = CLEAR_SITES.deref();
            } finally {
                Var.popThreadBindings();
            }
        }
        /*
         * eg:
         * def x = 1;
         * def y = 2;
         * if(1){
         *   println(x);//这里会为x设置清除点，也就是会在读x之后将x设置为null
         * }else{
         *   println(y);//这里会为y设置清除点，也就是会在读y之后将y设置为null
         * }
         * 如果进入then分支，则y可以被清除了，所以需要将else分支中对y设置的清除点，在then分支设置一个。
         * 这在y是一个非常大的数据结构的时候可以有效的及时释放内存。
         * */
        //then里面新增/更新的清除点，并且在else里面没有新增/更新，则需要在else里面pad
        sitesAfterThen.filter((lb, exprs) ->
                        scope.containsBindingR(lb)
                                && oldSites.getOrElse(lb, null) != exprs
                                && sitesAfterElse.getOrElse(lb, null) == exprs)
                .forEach((lb, exprs) ->
                        elseExpr.prependBody(new LocalBindingExpr(lb)));
        //同理
        sitesAfterElse.filter((lb, exprs) ->
                        scope.containsBindingR(lb)
                                && oldSites.getOrElse(lb, null) == sitesAfterThen.getOrElse(lb, null)
                                && oldSites.getOrElse(lb, null) != exprs)
                .forEach((lb, exprs) ->
                        thenExpr.prependBody(new LocalBindingExpr(lb)));
        return new IfExpr(testExpr, thenExpr, elseExpr);
    }

    /**
     * cond{
     * test1->expr1;
     * test2->expr2;
     * ...
     * testN->exprN;
     * }
     */
    @Override
    public Object visitCondExpr(SireniaParser.CondExprContext ctx) {
        return parseCondExpr(Vector.ofAll(ctx.expr()));
    }

    private Expr parseCondExpr(Vector<SireniaParser.ExprContext> exprs) {
        if (exprs.isEmpty())
            return CONSTANT_NULL_EXPR;
        return createIfExpr(() -> (Expr) visit(exprs.get(0)),
                () -> (Expr) visit(exprs.get(1)),
                () -> parseCondExpr(exprs.drop(2)));
    }

    private static LocalBinding registerLocal(Sym sym) {
        return registerLocal(sym, BindingType.LOCAL);
    }

    private static LocalBinding registerLocal(Sym sym, BindingType bindingType) {
        LocalBinding b = new LocalBinding(sym, bindingType);
        FnMethod method = FN_METHOD.deref();
        ScopeExpr scopeExpr = SCOPE.deref();
        scopeExpr.addBinding(b);
        method.locals = method.locals.add(b);
        b.clearPathRoot = CLEAR_ROOT.deref();
        return b;
    }


    private static int acquireIdx(FnMethod method, int idx) {
        int aidx = acquireIdx(method);
        if (aidx != idx) {
            throw new RuntimeException();
        }
        return idx;
    }

    private static int acquireIdx(FnMethod method) {
        if (method.freeLocalIdxs.isEmpty()) {
            return ++method.maxIdx;
        }
        Integer first = method.freeLocalIdxs.head();
        method.freeLocalIdxs = method.freeLocalIdxs.drop(1);
        return first;
    }

    private static void releaseIdx(FnMethod method, Integer idx) {
        method.freeLocalIdxs = method.freeLocalIdxs.add(idx);
    }

    static void closeOver(LocalBinding lb, FnMethod method) {
        if (lb != null && method != null && !method.locals.contains(lb)) {
            method.objx.closes = method.objx.closes.add(lb);
            closeOver(lb, method.parent);
        }
    }

    /*
     * 函数体内如何引用函数自身？
     * 创建函数体时，在其作用域添加一个绑定。
     * */
    @Override
    public Object visitIdRefExpr(SireniaParser.IdRefExprContext ctx) {
        FnMethod method = FN_METHOD.deref();
        ScopeExpr scope = SCOPE.deref();
        String name = ctx.ID().getText();
        LocalBinding binding = scope.findBindingR(new Sym(name));
        if (binding == null) {
            throw new RuntimeException("binding not found : " + name);
        }
        closeOver(binding, method);
        return new LocalBindingExpr(binding);
    }

    /**
     * paramsD : (ID|structParams) ('=' expr)? ;
     */
    @Override
    public Object visitParamsD(SireniaParser.ParamsDContext ctx) {
        LocalBinding destructLb = DESTRUCT.deref();
        if (ctx.ID() != null) {
            //idParam上层已经处理了，不应该到这里
            throw new RuntimeException();
        }
        if (ctx.expr() == null) {//没有默认值
            return visit(ctx.structParams());
        } else {
            Expr orElse = new OrElseExpr(new LocalBindingExpr(destructLb), (Expr) visit(ctx.expr()));
            return new ParenExpr()
                    .appendExpr(new AssignExpr(destructLb, orElse))
                    .appendExpr((Expr) visit(ctx.structParams()));
        }

    }

    //def [a,b,c,...r as arr] = ...

    /**
     * vecStructParams: LBRACK (EXPAND (ID|vecStructParams))? as? RBRACK //(...r)
     * | LBRACK paramsD (','paramsD)* (',' EXPAND (ID|vecStructParams))? as? RBRACK ;//(a=1,...r)
     * <p>
     * paramsD : (ID|structParams) ('=' expr)? ;
     */
    @Override
    public Object visitVecStructParams(SireniaParser.VecStructParamsContext ctx) {
        LocalBinding targetLb = DESTRUCT.deref();
        ParenExpr parenExpr = new ParenExpr()
                .setLine(ctx.start.getLine());
        parenExpr.appendExpr(new AssignExpr(targetLb, new OrElseExpr(new LocalBindingExpr(targetLb), new VecExpr())));
        DefExpr asDef = createDefExpr(ctx.as() == null ? RT.genName("as") : ctx.as().ID().getText(),
                ()->new VecExpr(Vector.empty()));
        parenExpr.appendExpr(asDef);
        LocalBinding asLb = asDef.binding;
        final Box<LocalBinding> tempLb = new Box<>(null);
        Vector.ofAll(ctx.paramsD()).forEachWithIndex((pd, i) -> {
            if (pd.ID() != null) {
                Expr init;
                if (pd.expr() != null) {//has default value
                    init = new OrElseExpr(
                            rt("get", new LocalBindingExpr(targetLb), NumberExpr.of(i)),
                            (Expr) visit(pd.expr()));
                } else {
                    init = rt("get", new LocalBindingExpr(targetLb), NumberExpr.of(i));
                }
                DefExpr itemDef = createDefExpr(pd.ID().getText(),()->init);
                parenExpr.appendExpr(itemDef);
                parenExpr.appendExpr(new AssignExpr(asLb, rt("conj", new LocalBindingExpr(asLb), new LocalBindingExpr(itemDef.binding))));
            } else {
                if (tempLb.value == null) {
                    DefExpr tempDef = createDefExpr(RT.genName("x"),()->CONSTANT_NULL_EXPR);
                    parenExpr.appendExpr(tempDef);
                    tempLb.value = tempDef.binding;
                }
                parenExpr.appendExpr(new AssignExpr(tempLb.value, rt("get", new LocalBindingExpr(targetLb), NumberExpr.of(i))));
                try {
                    Var.pushThreadBindings(HashMap.of(DESTRUCT, tempLb.value));
                    parenExpr.appendExpr(new AssignExpr(tempLb.value, (Expr) visit(pd)));//see visitParamsD
                } finally {
                    Var.popThreadBindings();
                }
                parenExpr.appendExpr(new AssignExpr(asLb, rt("conj", new LocalBindingExpr(asLb), new LocalBindingExpr(tempLb.value))));
            }
        });
        if (ctx.EXPAND() != null) {
            String restName = ctx.ID() != null ? ctx.ID().getText() : RT.genName("r");
            DefExpr restDef = createDefExpr(restName,()->new OrElseExpr(
                    rt("drop", new LocalBindingExpr(targetLb), NumberExpr.of(ctx.paramsD().size())),
                    new VecExpr()));
            parenExpr.appendExpr(restDef);
            LocalBinding restLb = restDef.binding;
            if (ctx.vecStructParams() != null) {
                try {
                    Var.pushThreadBindings(HashMap.of(DESTRUCT, restLb));
                    parenExpr.appendExpr(new AssignExpr(restLb, (Expr) visit(ctx.vecStructParams())));
                } finally {
                    Var.popThreadBindings();
                }
            }
            parenExpr.appendExpr(new AssignExpr(asLb, rt("concat", new LocalBindingExpr(asLb), new LocalBindingExpr(restLb))));
        }
        parenExpr.appendExpr(new LocalBindingExpr(asLb));
        return parenExpr;
    }

    private static Expr rt(String methodName, Expr... args) {
        return new MethodCallExpr(true, "RT", methodName, args);
    }

    @Override
    public Object visitMapStructParams(SireniaParser.MapStructParamsContext ctx) {
        LocalBinding targetLb = DESTRUCT.deref();
        ParenExpr parenExpr = new ParenExpr()
                .setLine(ctx.start.getLine());
        parenExpr.appendExpr(new AssignExpr(targetLb, new OrElseExpr(new LocalBindingExpr(targetLb), new MapExpr())));
        //def as = {}
        DefExpr asDef = createDefExpr(ctx.as() == null ? RT.genName("as") : ctx.as().ID().getText(),
                ()->new MapExpr());
        parenExpr.appendExpr(asDef);
        LocalBinding asLb = asDef.binding;
        //def value = null;
        final Box<LocalBinding> tempLb = new Box<>(null);
        Vector.ofAll(ctx.structPair()).forEach(pair -> {
            StringLiteralExpr keyExpr = new StringLiteralExpr(pair.ID().getText());
            if (pair.paramsD().ID() != null) {
                DefExpr itemDef;
                String name = pair.paramsD().ID().getText();
                if (pair.paramsD().expr() != null) {
                    itemDef = createDefExpr(name,()->new OrElseExpr(
                            rt("get", new LocalBindingExpr(targetLb), keyExpr),
                            (Expr) visit(pair.paramsD().expr())));
                } else {
                    itemDef = createDefExpr(name,()->rt("get", new LocalBindingExpr(targetLb), keyExpr));
                }
                parenExpr.appendExpr(itemDef);
                parenExpr.appendExpr(new AssignExpr(asLb, rt("assoc", new LocalBindingExpr(asLb), keyExpr, new LocalBindingExpr(itemDef.binding))));
            } else {
                if (tempLb.value == null) {
                    DefExpr tempDef = createDefExpr(RT.genName("x"),()->rt("get", new LocalBindingExpr(targetLb), keyExpr));
                    parenExpr.appendExpr(tempDef);
                    tempLb.value = tempDef.binding;
                } else {
                    parenExpr.appendExpr(new AssignExpr(tempLb.value, rt("get", new LocalBindingExpr(targetLb), keyExpr)));
                }
                try {
                    Var.pushThreadBindings(HashMap.of(DESTRUCT, tempLb.value));
                    parenExpr.appendExpr(new AssignExpr(tempLb.value, (Expr) visit(pair.paramsD())));//see visitParamsD
                } finally {
                    Var.popThreadBindings();
                }
                parenExpr.appendExpr(new AssignExpr(asLb, rt("assoc", new LocalBindingExpr(asLb), keyExpr, new LocalBindingExpr(tempLb.value))));
            }
        });
        if (ctx.ID() != null) {
            String restName = ctx.ID().getText();
            DefExpr restDef = createDefExpr(restName,()->
                    rt("dissocAll", new LocalBindingExpr(targetLb), rt("keys", new LocalBindingExpr(asLb))));
            parenExpr.appendExpr(restDef);
            parenExpr.appendExpr(new AssignExpr(asLb, rt("merge", new LocalBindingExpr(asLb), new LocalBindingExpr(restDef.binding))));
        } else if (ctx.mapStructParams() != null) {
            String restName = RT.genName("r");
            DefExpr restDef = createDefExpr(restName,()->
                    rt("dissocAll", new LocalBindingExpr(targetLb), rt("keys", new LocalBindingExpr(asLb))));
            parenExpr.appendExpr(restDef);
            try {
                Var.pushThreadBindings(HashMap.of(DESTRUCT, restDef.binding));
                parenExpr.appendExpr(new AssignExpr(restDef.binding, (Expr) visit(ctx.mapStructParams())));
            } finally {
                Var.popThreadBindings();
            }
            parenExpr.appendExpr(new AssignExpr(asLb, rt("merge", new LocalBindingExpr(asLb), new LocalBindingExpr(restDef.binding))));
        }
        parenExpr.appendExpr(new LocalBindingExpr(asLb));
        return parenExpr;
    }

    @Override
    public Object visitEmptyVecLiteral(SireniaParser.EmptyVecLiteralContext ctx) {
        return FieldAccessExpr.forGetStatic("RT", "EMPTY_VECTOR");
    }

    @Override
    public Object visitEmptyMapLiteral(SireniaParser.EmptyMapLiteralContext ctx) {
        return FieldAccessExpr.forGetStatic("RT", "EMPTY_MAP");
    }

    @Override
    public Object visitMapLiteral(SireniaParser.MapLiteralContext ctx) {
        Vector<SireniaParser.KvPairContext> pairs = Vector.ofAll(ctx.kvPair());
        boolean existsExpand = pairs.exists(it -> it instanceof SireniaParser.ExpandPairContext);
        if (existsExpand) {
            //idx = hashset(...)
            //hashmapExpand(kvs,idx)
            NewArrayExpr kvsExpr = new NewArrayExpr();
            pairs.forEach(pair -> {
                if (pair instanceof SireniaParser.IdKeyPairContext it) {
                    kvsExpr.addItem(new StringLiteralExpr(it.ID().getText()));
                    kvsExpr.addItem((Expr) visit(it.expr()));
                } else if (pair instanceof SireniaParser.StrKeyPairContext it) {
                    kvsExpr.addItem(new StringLiteralExpr(parseQuotedStr(it.STRING().getText())));
                    kvsExpr.addItem((Expr) visit(it.expr()));
                } else if (pair instanceof SireniaParser.ExprKeyPairContext it) {
                    kvsExpr.addItem((Expr) visit(it.expr(0)));
                    kvsExpr.addItem((Expr) visit(it.expr(1)));
                } else if (pair instanceof SireniaParser.ExpandPairContext it) {
                    kvsExpr.addItem((Expr) visit(it.expr()));
                }
            });
            NewArrayExpr idx = new NewArrayExpr();
            pairs.forEachWithIndex((c, i) -> {
                if (c instanceof SireniaParser.ExpandPairContext) idx.addItem(NumberExpr.of(i));
            });
            Expr idxExpr = new MethodCallExpr(true, "RT", "hashset", Vector.of(idx));
            return new MethodCallExpr(true, "RT", "hashmapExpand", Vector.of(kvsExpr, idxExpr))
                    .setLine(ctx.start.getLine());
        } else {
            MapExpr mapExpr = new MapExpr()
                    .setLine(ctx.start.getLine());
            pairs.forEach(pair -> {
                if (pair instanceof SireniaParser.IdKeyPairContext it) {
                    mapExpr.addEntry(new StringLiteralExpr(it.ID().getText()),
                            (Expr) visit(it.expr()));
                } else if (pair instanceof SireniaParser.StrKeyPairContext it) {
                    mapExpr.addEntry(new StringLiteralExpr(parseQuotedStr(it.STRING().getText())),
                            (Expr) visit(it.expr()));
                } else if (pair instanceof SireniaParser.ExprKeyPairContext it) {
                    mapExpr.addEntry((Expr) visit(it.expr(0)),
                            (Expr) visit(it.expr(1)));
                }
            });
            return mapExpr;
        }
    }

    @Override
    public Object visitTrueLiteral(SireniaParser.TrueLiteralContext ctx) {
        return BoolExpr.of(ctx.getText().equals("true"));
    }

    @Override
    public Object visitFalseLiteral(SireniaParser.FalseLiteralContext ctx) {
        return BoolExpr.of(ctx.getText().equals("true"));
    }

    @Override
    public Object visitCharLiteral(SireniaParser.CharLiteralContext ctx) {
        String text = ctx.CHAR().getText();
        text = Util.unescapeJava(Util.trimN(text, 1));
        return new CharLiteralExpr(text.charAt(0));
    }

    @Override
    public Object visitPipExpr(SireniaParser.PipExprContext ctx) {
        ParenExpr parenExpr = new ParenExpr();
        String name = ctx.ID().getText();
        parenExpr.appendExpr(createDefExpr(name,()->(Expr) visit(ctx.expr(0))));
        parenExpr.appendExpr((Expr) visit(ctx.expr(1)));
        return parenExpr;
    }

    enum C {
        EXPRESSION,
        STATEMENT,
        RETURN,
        EVAL,
    }

    static abstract class Expr {
        int line;
        <T extends Expr> T setLine(int line){
            this.line = line;
            return (T) this;
        }
        void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            throw new RuntimeException("operation is not supported " + this.getClass());
        }
        void emitLineNumber(GeneratorAdapter gen){
            if(line>0)
                gen.visitLineNumber(line,gen.mark());
        }
    }

    static class TryCatchExpr extends ScopeExpr {
        int retIdx;
        LocalBinding catchLb;
        int throwIdx;
        ScopeExpr tryExpr;
        ScopeExpr catchExpr;
        ScopeExpr finallyExpr;

        TryCatchExpr(ScopeExpr parent) {
            super(parent);
        }

        void catchLb(LocalBinding lb) {
            lb.autoClear = false;
            this.catchLb = lb;
        }

        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            if (catchExpr == null && finallyExpr == null) {
                tryExpr.emit(context, objx, method, gen);
                return;
            }

            Label start = gen.mark();
            //def result = null; 可以不赋值null吗？理论上可以，但是asm不让，可能是asm的bug。
            if (context == C.EXPRESSION) {
                retIdx = acquireIdx(method);
                AsmUtil.pushObjLdc(gen, null);
                AsmUtil.storeLocal(gen, retIdx);
            }
            if (catchExpr != null)
                catchLb.idx = acquireIdx(method);

            //def ex = null;
            throwIdx = acquireIdx(method);
            AsmUtil.pushObjLdc(gen, null);
            AsmUtil.storeLocal(gen, throwIdx);

            Label startTry = gen.newLabel();
            Label endTry = gen.newLabel();
            Label startCatch = gen.newLabel();
            Label endCatchTry = gen.newLabel();
            Label startCatchTry = gen.newLabel();
            Label startCatchCatch = gen.newLabel();
            Label finallyLabel = gen.newLabel();
            Label retLabel = gen.newLabel();
            Label end = gen.newLabel();

            gen.mark(startTry);
            //插入NOP，否则可能try-catch块的startTry和catchTry为同一个标签（非法）
            gen.visitInsn(NOP);
            tryExpr.emit(context, objx, method, gen);
            if (context == C.EXPRESSION) {
                AsmUtil.storeLocal(gen, retIdx);
            }

            gen.mark(endTry);

            if (finallyExpr == null)
                gen.goTo(retLabel);
            else
                gen.goTo(finallyLabel);

            if (catchExpr == null) {
                gen.mark(startCatch);
                //ex = stack.pop();
                AsmUtil.storeLocal(gen, throwIdx);
            } else {
                gen.mark(startCatch);
                AsmUtil.storeLocal(gen, catchLb.idx);
                gen.mark(startCatchTry);
                gen.visitInsn(NOP);
                //e = stack.pop();
                //result = doCatch(e);
                catchExpr.emit(context, objx, method, gen);
                if (context == C.EXPRESSION) {
                    AsmUtil.storeLocal(gen, retIdx);
                }

                gen.mark(endCatchTry);

                if (finallyExpr == null)
                    gen.goTo(retLabel);
                else
                    gen.goTo(finallyLabel);

                gen.mark(startCatchCatch);
                //ex = stack.pop();
                AsmUtil.storeLocal(gen, throwIdx);// lost e info
            }
            //doFinally();
            if (finallyExpr != null) {
                gen.mark(finallyLabel);
                finallyExpr.emit(C.STATEMENT, objx, method, gen);
            }

            //if(ex==null) return result;
            //else throw ex;
            AsmUtil.loadLocal(gen, throwIdx);
            gen.ifNull(retLabel);
            AsmUtil.loadLocal(gen, throwIdx);
            gen.throwException();
            gen.goTo(end);

            gen.mark(retLabel);
            if (context == C.EXPRESSION) {
                AsmUtil.loadLocal(gen, retIdx);
                //clear ret
                AsmUtil.clearLocal(gen, retIdx);
            }
            gen.mark(end);

            if (context == C.EXPRESSION) {
                releaseIdx(method, retIdx);
                gen.visitLocalVariable(RT.genName("ret"), "Ljava/lang/Object;", null, start, end, retIdx);
            }
            releaseIdx(method, throwIdx);
            gen.visitLocalVariable(RT.genName("ex"), Type.getType(Exception.class).getDescriptor(), null, start, end, throwIdx);

            gen.visitTryCatchBlock(startTry, endTry, startCatch, Type.getType(Exception.class).getInternalName());
            if (catchExpr != null) {
                releaseIdx(method, catchLb.idx);
                gen.visitLocalVariable(catchLb.sym.name, "Ljava/lang/Object;", null, startCatchTry, endCatchTry, catchLb.idx);
                gen.visitTryCatchBlock(startCatchTry, endCatchTry, startCatchCatch, Type.getType(Exception.class).getInternalName());
            }
        }
    }

    static class ParenExpr extends Expr {
        Vector<Expr> exprs = Vector.empty();

        public ParenExpr prependExpr(Expr expr) {
            exprs = exprs.prepend(expr);
            return this;
        }

        public ParenExpr appendExpr(Expr expr) {
            exprs = exprs.append(expr);
            return this;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            if (exprs.isEmpty()) {
                if (context == C.EXPRESSION) gen.visitInsn(ACONST_NULL);
            } else {
                exprs.dropRight(1).forEach(e ->
                        e.emit(C.STATEMENT, objx, method, gen));
                exprs.last().emit(context, objx, method, gen);
            }
        }

        @Override
        public String toString() {
            return exprs.mkString("(", ",\n", ")");
        }
    }

    static class ScopeExpr extends Expr {
        ScopeExpr parent;
        //该作用域下直接定义的变量，不包括子作用域中定义的变量。子作用域中的变量由子作用域释放。
        //Seq<LocalBinding> locals = Vector.empty();
        Map<Sym, LocalBinding> locals = LinkedHashMap.empty();
        Set<LocalBinding> lbs = LinkedHashSet.empty();
        Expr body;

        ScopeExpr(ScopeExpr parent) {
            this.parent = parent;
        }

        ScopeExpr prependBody(Expr expr) {
            if (body == null)
                body = expr;
            else if (body instanceof ParenExpr pbody)
                pbody.prependExpr(expr);
            else
                body = new ParenExpr().prependExpr(body).prependExpr(expr);
            return this;
        }

        ScopeExpr appendBody(Expr expr) {
            if (body == null)
                body = expr;
            else if (body instanceof ParenExpr pbody)
                pbody.appendExpr(expr);
            else
                body = new ParenExpr().appendExpr(body).appendExpr(expr);
            return this;
        }

        ScopeExpr addBinding(Compiler.LocalBinding binding) {
            locals = locals.put(binding.sym, binding);
            lbs = lbs.add(binding);
            return this;
        }

        LocalBinding findBindingR(Sym sym) {
            LocalBinding lb = locals.getOrElse(sym, null);
            if (lb != null)
                return lb;
            if (parent == null) {
                return null;//throw new RuntimeException("binding not found : " + sym);
            }
            return parent.findBindingR(sym);
        }

        public boolean containsBindingR(LocalBinding lb) {
            return lbs.contains(lb) ? true : parent != null && parent.containsBindingR(lb);
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            Set<LocalBinding> lbs = this.lbs.filter(it -> it.used
                    && it.bindingType != BindingType.METHOD_ARG
                    && it.bindingType != BindingType.THIS);
            lbs.forEach(lb -> lb.idx = acquireIdx(method));
            body.emit(context, objx, method, gen);
            Label end = gen.mark();
            lbs.forEach(lb -> {
                lb.endLabel = end;
                //这个变量槽可以被后面重用了
                releaseIdx(method, lb.idx);
                gen.visitLocalVariable(lb.sym.name, "Ljava/lang/Object;", null, lb.startLabel, lb.endLabel, lb.idx);
            });
        }
    }

    static class NewObjExpr extends Expr {
        Runnable beforeEmit;
        String className;
        Seq<Param> args;

        NewObjExpr(Runnable beforeEmit, String className, Seq<Param> args) {
            this.beforeEmit = beforeEmit;
            this.className = resolveAlias(className);
            this.args = args.peek(it -> it.type = resolveAlias(it.type));
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            if (beforeEmit != null)
                beforeEmit.run();
            gen.visitTypeInsn(NEW, Util.objectInnerType(className));
            gen.dup();
            args.forEach(p -> p.expr.emit(C.EXPRESSION, objx, method, gen));
            Constructor c = Util.findCtor(className, args.map(it -> it.type));
            Method ctor = Method.getMethod(c);
            gen.invokeConstructor(Type.getType(Util.typeDescriptor(className)), ctor);
            if (context != C.EXPRESSION) {
                gen.pop();
            }
        }
    }

    static class NewArrayExpr extends Expr {
        Type type = Type.getType(Object.class);
        Seq<Expr> items;

        ParenExpr parenExpr = new ParenExpr();

        NewArrayExpr() {
            items = Vector.empty();
        }

        NewArrayExpr(Seq<Expr> items) {
            this.items = items;
        }

        void addItem(Expr item) {
            items = items.append(item);
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            gen.push(items.length());
            gen.visitTypeInsn(ANEWARRAY, Type.getType(Object.class).getInternalName());
            items.forEachWithIndex((item, i) -> {
                gen.dup();
                gen.push(i);
                item.emit(C.EXPRESSION, objx, method, gen);
                gen.visitInsn(AASTORE);
            });
        }

        @Override
        public String toString() {
            return items.mkString("new Object[]{", ", ", "}");
        }
    }

    static class FnCallExpr extends Expr {

        static final int mrpc = Fn.MAX_REQ_PARAM_COUNT;
        Expr fn;
        Vector<Expr> reqParams;
        Expr expandParam;

        FnCallExpr(Expr fn, Vector<Expr> reqParams) {
            this(fn, reqParams, null);
        }

        FnCallExpr(Expr fn, Vector<Expr> reqParams, Expr extendParam) {
            this.fn = fn;
            this.reqParams = reqParams;
            this.expandParam = extendParam;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            java.lang.reflect.Method targetMethod;
            Vector<Expr> args;
            //没有拓展运算符，调用invoke
            if (expandParam == null) {
                //invoke最多支持20个位置参数+1个数组参数
                //超过的部分全部收集到数组中
                if (reqParams.length() > mrpc) {
                    args = reqParams.take(mrpc).append(new NewArrayExpr(reqParams.drop(mrpc)));
                } else {
                    args = reqParams;
                }
                targetMethod = FN_INVOKES.get(args.length());
            } else {//有拓展运算符，将参数收集到Seq，调用applyTo
                Expr seq = rt("concat", new NewArrayExpr(reqParams), expandParam);
                args = Vector.of(seq);
                targetMethod = FN_APPLY_TO;
            }

            fn.emit(C.EXPRESSION, objx, method, gen);
            gen.checkCast(Type.getType(Fn.class));
            args.forEach(p ->
                    p.emit(C.EXPRESSION, objx, method, gen)
            );
            //Method.getMethod(String) 如果有数组类型，数组类型要用类似Object[]这样的形式，而不能用它的className。
            //所以优先通过反射得到的java.lang.reflect.Method构造asm的Method
            gen.invokeVirtual(Type.getType(Fn.class), Method.getMethod(targetMethod));
            if (context != C.EXPRESSION) {
                gen.pop();
            }
        }

        @Override
        public String toString() {
            return reqParams.mkString(fn + "(", ", ", expandParam == null ? ")" : "..." + expandParam + ")");
        }
    }

    //LoopExpr可以看成一种特殊的ScopeExpr
    static class LoopExpr extends ScopeExpr {
        Vector<Expr> inits = Vector.empty();

        LoopExpr(ScopeExpr parent) {
            super(parent);
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            //lbs is loopArgs
            Label start = gen.mark();
            Label end = gen.newLabel();
            lbs.forEachWithIndex((lb, i) -> {
                lb.idx = acquireIdx(method);
                inits.get(i).emit(C.EXPRESSION, objx, method, gen);
                AsmUtil.storeLocal(gen, lb.idx);
            });
            try {
                Var.pushThreadBindings(HashMap.of(LOOP_LABEL, gen.mark()));
                body.emit(context, objx, method, gen);
            } finally {
                Var.popThreadBindings();
            }
            gen.mark(end);
            lbs.forEach(lb -> {
                gen.visitLocalVariable(lb.sym.name, "Ljava/lang/Object;", null, start, end, lb.idx);
                //这个变量槽可以被后面重用了
                releaseIdx(method, lb.idx);
            });
        }
    }

    static class RecurExpr extends Expr {
        Vector<AssignExpr> exprs = Vector.empty();

        public void appendExpr(AssignExpr expr) {
            exprs = exprs.append(expr);
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            exprs.forEach(expr -> expr.emit(C.STATEMENT, objx, method, gen));
            gen.goTo(LOOP_LABEL.deref());
        }

        @Override
        public String toString() {
            return exprs.mkString("recur(", ", ", ")");
        }
    }

    static class PreExpr extends Expr {
        Category category;
        String op;
        Expr n;

        public PreExpr(String op, Expr n) {
            this.category = CATEGORY.deref();
            this.op = op;
            this.n = n;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            n.emit(C.EXPRESSION, objx, method, gen);
            java.lang.reflect.Method opMethod = category.findPreOp(op);
            gen.invokeStatic(Type.getType(opMethod.getDeclaringClass()), Method.getMethod(opMethod));
            if (context != C.EXPRESSION) {
                gen.pop();
            }
        }
    }

    static class BinExpr extends Expr {
        Category category;
        String op;
        Expr n1;
        Expr n2;

        BinExpr(String op, Expr n1, Expr n2) {
            this.category = CATEGORY.deref();
            this.op = op;
            this.n1 = n1;
            this.n2 = n2;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            n1.emit(C.EXPRESSION, objx, method, gen);
            n2.emit(C.EXPRESSION, objx, method, gen);
            java.lang.reflect.Method opMethod = category.findBinOp(op);
            gen.invokeStatic(Type.getType(opMethod.getDeclaringClass()), Method.getMethod(opMethod));
            if (context != C.EXPRESSION) {
                gen.pop();
            }
        }

        @Override
        public String toString() {
            return n1 + " " + op + " " + n2;
        }
    }

    static class CharLiteralExpr extends Expr {
        char ch;

        CharLiteralExpr(char ch) {
            this.ch = ch;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            if (context == C.EXPRESSION)
                AsmUtil.pushObjLdc(gen, ch);
        }
    }

    static class StringLiteralExpr extends Expr {
        String text;

        public StringLiteralExpr(String text) {
            this.text = text;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            if (context == C.EXPRESSION)
                gen.visitLdcInsn(text);
        }

        @Override
        public String toString() {
            return "\"" + text + "\"";
        }
    }

    static class VecExpr extends Expr {
        Vector<Expr> items = Vector.empty();

        public VecExpr() {
        }

        public VecExpr(Vector<Expr> items) {
            this.items = items;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            Expr expr = new MethodCallExpr(true, "RT", "vector", Vector.of(new NewArrayExpr(items)));
            expr.emit(context, objx, method, gen);
        }

        @Override
        public String toString() {
            return Arrays.toString(items.toJavaArray());
        }
    }

    static class MapExpr extends Expr {
        Map<Expr, Expr> entries = LinkedHashMap.empty();

        MapExpr addEntry(Expr k, Expr v) {
            entries = entries.put(k, v);
            return this;
        }

        NewArrayExpr toArrayExpr() {
            NewArrayExpr arr = new NewArrayExpr();
            entries.forEach((k, v) -> {
                arr.addItem(k);
                arr.addItem(v);
            });
            return arr;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            Expr expr = new MethodCallExpr(true, "RT", "hashmap", Vector.of(toArrayExpr()));
            expr.emit(C.EXPRESSION, objx, method, gen);
        }
    }

    static class Param {
        Expr expr;
        String type;

        public static Object of(Expr expr, String type) {
            Param p = new Param();
            p.expr = expr;
            p.type = type;
            return p;
        }
    }

    static class MethodCallExpr extends Expr {
        String className;
        Expr target;
        String methodName;
        Vector<Param> args;

        java.lang.reflect.Method javaMethod;

        MethodCallExpr(boolean isStatic, String className, String methodName, Expr... args) {
            this(isStatic, className, methodName, Vector.ofAll(Arrays.asList(args)));
        }

        MethodCallExpr(boolean isStatic, String className, String methodName, Vector args) {
            this.className = resolveAlias(className);
            Vector<Param> ps = args.map(it -> {
                if (it instanceof Param p) {
                    p.type = resolveAlias(p.type);
                    return it;
                } else {
                    return Param.of((Expr) it, null);
                }
            });
            if (!isStatic) {
                this.target = ps.head().expr;
                this.args = ps.drop(1);
            } else {
                this.args = ps;
            }
            this.methodName = methodName;
            javaMethod = Util.findMethod(this.className, methodName, this.args.map(it -> it.type));
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            if (!Modifier.isStatic(javaMethod.getModifiers()))
                target.emit(C.EXPRESSION, objx, method, gen);
            Class[] argTypes = javaMethod.getParameterTypes();
            args.forEachWithIndex((p, i) -> {
                p.expr.emit(C.EXPRESSION, objx, method, gen);
                if (argTypes[i] != Object.class) {
                    gen.checkCast(Type.getType(argTypes[i]));
                }
            });
            //args.forEach(p -> p.expr.emit(C.EXPRESSION, objx, method, gen));
            Method m = Method.getMethod(javaMethod);
            if (Modifier.isStatic(javaMethod.getModifiers()))
                gen.invokeStatic(Type.getObjectType(Util.objectInnerType(className)), m);
            else
                gen.invokeVirtual(Type.getObjectType(Util.objectInnerType(className)), m);
            boolean retVoid = m.getReturnType().equals(Type.VOID_TYPE);
            if (context == C.EXPRESSION && retVoid) {
                gen.visitInsn(ACONST_NULL);
            } else if (context != C.EXPRESSION && !retVoid) {
                gen.pop();
            }
        }

        @Override
        public String toString() {
            return "#" + className + "/" + methodName + "(" + target + "," + args.map(it -> it.expr).mkString(",") + ")";
        }
    }

    static class FieldAccessExpr extends Expr {
        String className;
        Expr target;
        String fieldName;
        Expr value;

        static FieldAccessExpr init(String className, String fieldName, Expr target, Expr value) {
            FieldAccessExpr it = new FieldAccessExpr();
            it.className = resolveAlias(className);
            it.target = target;
            it.fieldName = fieldName;
            it.value = value;
            return it;
        }

        //for get static
        public static FieldAccessExpr forGetStatic(String className, String fieldName) {
            return init(className, fieldName, null, null);
        }

        //for put static
        public static FieldAccessExpr forPutStatic(String className, String fieldName, Expr value) {
            return init(className, fieldName, null, value);
        }

        //for get member
        public static FieldAccessExpr forGetMember(String className, String fieldName, Expr target) {
            return init(className, fieldName, target, null);
        }

        public static FieldAccessExpr forPutMember(String className, String fieldName, Expr target, Expr value) {
            return init(className, fieldName, target, value);
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            java.lang.reflect.Field javaField = Util.findField(className, fieldName);
            Type owner = Type.getType(Util.typeDescriptor(className));
            Type retType = Type.getType(javaField.getType());
            if (value == null) {//get
                if (context != C.EXPRESSION) return;
                if (target == null)//static
                    gen.getStatic(owner, fieldName, retType);
                else//member
                    gen.getField(owner, fieldName, retType);
            } else {
                value.emit(C.EXPRESSION, objx, method, gen);
                if (context == C.EXPRESSION) gen.dup();
                if (target == null) {
                    gen.putStatic(owner, fieldName, retType);
                } else {
                    gen.putField(owner, fieldName, retType);
                }
            }
        }
    }

    static Expr CONSTANT_NULL_EXPR = new Expr() {
        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            if (context != C.STATEMENT) gen.visitInsn(ACONST_NULL);
        }

        @Override
        public String toString() {
            return "null";
        }
    };

    static class BoolExpr extends Expr {
        static final BoolExpr TrueExpr = new BoolExpr(true);
        static final BoolExpr FalseExpr = new BoolExpr(false);
        boolean value;

        private BoolExpr(boolean v) {
            this.value = v;
        }

        static BoolExpr of(boolean v) {
            return v ? TrueExpr : FalseExpr;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            if (context == C.EXPRESSION)
                AsmUtil.pushObjLdc(gen, value);
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }


    static class NumberExpr extends Expr {
        static NumberExpr ZERO = new NumberExpr(0L);
        Number n;

        private NumberExpr(Number n) {
            this.n = n;
        }

        static NumberExpr of(Number n) {
            return new NumberExpr(n);
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            if (context == C.EXPRESSION)
                AsmUtil.pushObjLdc(gen, n);
        }

        @Override
        public String toString() {
            return n.toString();
        }
    }

    static class IfExpr extends Expr {
        Expr testExpr;
        Expr thenExpr;
        Expr elseExpr;

        public IfExpr(Expr testExpr, Expr thenExpr, Expr elseExpr) {
            this.testExpr = testExpr;
            this.thenExpr = thenExpr;
            this.elseExpr = elseExpr;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            Label nullLabel = gen.newLabel();
            Label falseLabel = gen.newLabel();
            Label endLabel = gen.newLabel();
            testExpr.emit(C.EXPRESSION, objx, method, gen);
            gen.dup();//拷贝一份test的返回值
            gen.ifNull(nullLabel);//如果栈顶的值为null，跳到null标签
            gen.getStatic(Type.getType(Boolean.class), "FALSE", Type.getType(Boolean.class));
            gen.visitJumpInsn(IF_ACMPEQ, falseLabel);
            thenExpr.emit(context, objx, method, gen);
            gen.goTo(endLabel);
            gen.visitLabel(nullLabel);//null标签
            gen.pop();//无需判断是否为true/false
            gen.visitLabel(falseLabel);
            elseExpr.emit(context, objx, method, gen);//访问false分支
            gen.visitLabel(endLabel);
        }


        @Override
        public String toString() {
            return "if(" + testExpr + ", " + thenExpr + ", " + elseExpr + ")";
        }
    }

    static class OrElseExpr extends Expr {
        Expr testExpr;
        Expr elseExpr;

        OrElseExpr(Expr testExpr, Expr elseExpr) {
            this.testExpr = testExpr;
            this.elseExpr = elseExpr;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            //orElse(x,y)
            Label elseLabel = gen.newLabel();
            Label endLabel = gen.newLabel();
            testExpr.emit(C.EXPRESSION, objx, method, gen);//[]->[x]
            gen.dup();//[x]->[x,x]
            gen.ifNull(elseLabel);//[x,x]->[x]
            gen.dup();//[x]->[x,x]
            gen.getStatic(Type.getType(Boolean.class), "FALSE", Type.getType(Boolean.class));
            gen.visitJumpInsn(IF_ACMPEQ, elseLabel);//[x,x]->[x]
            if (context != C.EXPRESSION) {
                gen.pop();//[x]->[]
            }
            gen.goTo(endLabel);
            gen.visitLabel(elseLabel);
            gen.pop();//[x]->[]
            elseExpr.emit(context, objx, method, gen);//访问false分支 [x]->[y]
            gen.visitLabel(endLabel);
        }

        @Override
        public String toString() {
            return "orElse(" + testExpr + "," + elseExpr + ")";
        }
    }

    static class Ret1Expr extends Expr {
        Expr ret;
        Expr expr;

        static Ret1Expr forClear(LocalBinding lb) {
            return new Ret1Expr(new LocalBindingExpr(lb), CONSTANT_NULL_EXPR);
        }

        Ret1Expr(Expr ret, Expr expr) {
            this.ret = ret;
            this.expr = expr;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            ret.emit(context, objx, method, gen);
            expr.emit(C.STATEMENT, objx, method, gen);
        }

        @Override
        public String toString() {
            return "ret1(" + ret + ", " + expr + ")";
        }
    }

    static class DefExpr extends Expr {
        LocalBinding binding;
        Expr init;

        public DefExpr(LocalBinding binding, Expr init) {
            this.binding = binding;
            this.init = init;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            emitLineNumber(gen);
            if (!binding.used) {
                //如果变量定义后没有使用，那就不分配idx，也就不用申请idx和执行startLabel、endLabel了。
                init.emit(context, objx, method, gen);
            } else {
                //要先申请idx再执行init，否则如果init是一个ScopeExpr，idx会申请到在这个ScopeExpr释放的idx。
                //虽然逻辑没问题，并且能节约栈空间，但是反编译后的代码有问题。应该保证反编译后的代码逻辑的正确性。
                //如果有多个def，反编译还是有问题。所以申请idx要提前到ScopeExpr的最前面。
                //binding.idx = acquireIdx(method);
                init.emit(C.EXPRESSION, objx, method, gen);
                binding.startLabel = gen.mark();
                AsmUtil.storeLocal(gen, binding.idx);
                if (context == C.EXPRESSION)
                    AsmUtil.loadLocal(gen, binding.idx);
            }
        }

        @Override
        public String toString() {
            return "def " + binding.sym.name + " = " + init;
        }
    }

    //要先执行init再registerLocal。
    //这样支持def x = 1; def x = x+1;//重新定义x，可以使用前面的x。
    DefExpr createDefExpr(String name,Supplier<Expr> f){
        Expr init = f.get();
        LocalBinding lb = registerLocal(new Sym(name));
        return new DefExpr(lb,init);
    }

    static abstract class ClearSupportExpr extends Expr {
        boolean shouldClear;
        PathNode clearPath;

        protected void processClearSites(LocalBinding b) {
            this.clearPath = CLEAR_PATH.deref();
            PathNode clearRoot = CLEAR_ROOT.deref();
            Map<LocalBinding, Set<ClearSupportExpr>> sitesMap = CLEAR_SITES.deref();
            Set<ClearSupportExpr> sites = sitesMap.getOrElse(b, HashSet.empty());
            if (sites != null) {
                for (ClearSupportExpr o : sites) {
                    PathNode common = commonPath(clearPath, o.clearPath);
                    if (common != null && common.type == PATHTYPE.PATH)
                        o.shouldClear = false;
                }
            }
            if (clearRoot == b.clearPathRoot) {
                this.shouldClear = true;
                sites = sites.add(this);
                CLEAR_SITES.set(sitesMap.put(b, sites));
            }
        }
    }

    static class AssignExpr extends ClearSupportExpr {
        LocalBinding binding;
        Expr initExpr;

        public AssignExpr(LocalBinding b, Expr initExpr) {
            b.used = true;
            this.binding = b;
            this.initExpr = initExpr;
            processClearSites(b);
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            if (objx.closes.contains(binding)) {
                gen.loadThis();
                initExpr.emit(C.EXPRESSION, objx, method, gen);
                gen.putField(objx.thisType, binding.sym.name, Type.getType(Object.class));
            } else {
                initExpr.emit(C.EXPRESSION, objx, method, gen);
                AsmUtil.storeLocal(gen, binding.idx);
            }
            if (context == C.EXPRESSION) AsmUtil.loadLocal(gen, binding.idx);
        }

        @Override
        public String toString() {
            return binding.sym + " = " + initExpr;
        }
    }

    static class LocalBindingExpr extends ClearSupportExpr {
        LocalBinding binding;

        public LocalBindingExpr(LocalBinding b) {
            b.used = true;
            this.binding = b;
            processClearSites(b);
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            if (objx.closes.contains(binding)) {
                if (context == C.EXPRESSION) {
                    gen.loadThis();
                    gen.getField(objx.thisType, binding.sym.name, Type.getType(Object.class));
                }
                if (binding.autoClear && objx.onceOnly && shouldClear) {
                    gen.loadThis();
                    gen.visitInsn(ACONST_NULL);
                    gen.putField(objx.thisType, binding.sym.name, Type.getType(Object.class));
                }
            } else {
                if (context == C.EXPRESSION) {
                    AsmUtil.loadLocal(gen, binding.idx);
                    //gen.visitVarInsn(ALOAD, binding.idx);
                }
                if (binding.autoClear && shouldClear) {
                    AsmUtil.clearLocal(gen, binding.idx);
                }
            }
        }

        @Override
        public String toString() {
            return binding.sym.name;
        }
    }

    static PathNode commonPath(PathNode n1, PathNode n2) {
        Seq xp = fwdPath(n1);
        Seq yp = fwdPath(n2);
        if (xp.head() != yp.head())
            return null;
        while (xp.length() > 1 && yp.length() > 1 && xp.get(1) == yp.get(1)) {
            xp = xp.drop(1);
            yp = yp.drop(1);
        }
        return (PathNode) xp.head();
    }

    static Seq fwdPath(PathNode p1) {
        Seq ret = List.empty();
        for (; p1 != null; p1 = p1.parent)
            ret = ret.prepend(p1);
        return ret;
    }

    static class FnExpr extends Expr {
        public static final Method voidctor = Method.getMethod("void <init>()");
        public static final String[] EMPTY_STRING_ARR = {};
        //FnMethod closeMethod;
        //如果是一次性函数，捕获的变量也可以在最后一次被使用后释放
        boolean onceOnly;
        String name;
        String className;
        String fullClassName;
        String doc;
        Type thisType;
        Vector<FnMethod> methods = Vector.empty();//args 0~20 + var args
        FnMethod restParamMethod;
        Set<LocalBinding> closes = LinkedHashSet.empty();
        Class clazz;
        Class<? extends Fn> supperClass = Fn.class;

        FnExpr(String name, String fullClassName, String doc) {
            this.name = name;
            this.fullClassName = fullClassName;
            this.className = Util.simpleClassName(fullClassName);
            this.doc = doc;
            this.thisType = Type.getObjectType(Util.objectInnerType(fullClassName));
        }

        public void addFnMethod(FnMethod fnMethod) {
            if (fnMethod.hasRestParam) {
                if (restParamMethod != null)
                    throw new RuntimeException("multiple rest arg method");
                supperClass = RestFn.class;
                restParamMethod = fnMethod;
            }
            methods = methods.append(fnMethod);
            fnMethod.objx = this;
        }

        public <T> T eval() {
            try {
                return (T) getCompiledClass().getConstructor().newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            throw new RuntimeException();
        }

        public void emit(ClassWriter cw) {
            closes.forEach(lb ->
                    cw.visitField(ACC_PUBLIC + ACC_FINAL,
                            lb.sym.name, Type.getType(Object.class).getDescriptor(),
                            null,
                            null).visitEnd());

            Type[] ctorArgTypes = new Type[closes.length()];
            for (int i = 0; i < closes.length(); i++) {
                ctorArgTypes[i] = Type.getType(Object.class);
            }
            GeneratorAdapter ctorgen = new GeneratorAdapter(ACC_PUBLIC, new Method("<init>", Type.VOID_TYPE, ctorArgTypes), null, null, cw);
            ctorgen.visitCode();
            Label ctorStart = ctorgen.mark();
            emitLineNumber(ctorgen);
            ctorgen.loadThis();
            ctorgen.invokeConstructor(Type.getType(supperClass), voidctor);
            int i = 0;
            for (LocalBinding lb : closes) {
                ctorgen.loadThis();
                ctorgen.loadArg(i++);
                ctorgen.putField(thisType, lb.sym.name, Type.getType(Object.class));
            }
            Label ctorEnd = ctorgen.mark();
            ctorgen.visitLocalVariable("this", thisType.getDescriptor(), null, ctorStart, ctorEnd, 0);
            ctorgen.returnValue();
            ctorgen.endMethod();

            //int getRequiredArity();
            if (restParamMethod != null) {
                Method method = new Method("getRequiredArity", Type.getType(int.class), new Type[0]);
                GeneratorAdapter gen = new GeneratorAdapter(ACC_PUBLIC, method, null, null, cw);
                gen.visitCode();
                gen.push(restParamMethod.args.length() - 1);
                gen.returnValue();
                gen.endMethod();
            }

            for (FnMethod method : methods) {
                try {
                    method.emit(cw);
                } catch (Exception e) {
                    throw new RuntimeException("emit method error. method=" + method, e);
                }
            }
            cw.visitEnd();
            if (WRITE_CLASS_FILE.deref()) {
                byte[] bytecode = cw.toByteArray();
                try {
                    Files.write(Paths.get(CLASSES_DIR.deref(), className + ".class"), bytecode);
                } catch (Exception e) {
                    throw Util.sneakyThrow(e);
                }
            }
        }

        Class getCompiledClass() {
            synchronized (this) {
                if (clazz == null) {
                    DynamicClassLoader loader = CLASS_LOADER.deref();
                    ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
                    cw.visit(V1_8, ACC_FINAL + ACC_SUPER + ACC_PUBLIC, fullClassName.replace('.', '/'), null, Util.objectInnerType(supperClass.getName()), EMPTY_STRING_ARR);
                    cw.visitSource(className, null);
                    emit(cw);
                    byte[] bytecode = cw.toByteArray();
                    try {
                        Path path = Paths.get(CLASSES_DIR.deref(), fullClassName.replace('.', '/') + ".class");
                        path.toFile().getParentFile().mkdirs();
                        Files.write(path, bytecode);
                    } catch (Exception e) {
                        throw Util.sneakyThrow(e);
                    }
                    clazz = loader.defineClass(fullClassName, bytecode, null);
                }
            }
            return clazz;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (FnMethod m : methods) {
                sb.append("  ").append(m).append("\n");
            }
            return "defn " + name + " (\n" + doc + "\n" + sb + ")";
        }
    }

    static class FnMethod extends Expr {
        Vector<LocalBinding> args = Vector.empty();
        //包括方法参数、本地变量。用于判断自由变量
        Set<LocalBinding> locals = LinkedHashSet.empty();
        ScopeExpr body;
        boolean hasRestParam;
        FnExpr objx;
        FnMethod parent;

        TreeSet<Integer> freeLocalIdxs = TreeSet.empty();

        Label start;
        Label end;

        int maxIdx = 0;

        void addArg(LocalBinding lb) {
            args = args.append(lb);
        }

        FnMethod(FnMethod parent, boolean hasRestParam) {
            this.parent = parent;
            this.hasRestParam = hasRestParam;
        }

        @Override
        public String toString() {
            Vector<String> ps = args.map(it -> it.sym.name);
            if (hasRestParam) {
                ps = ps.dropRight(1).append("..." + ps.last());
            }
            return ps.mkString("(", ", ", ")") + "=>" + body;
        }

        @Override
        public void emit(C context, FnExpr objx, FnMethod method, GeneratorAdapter gen) {
            throw new RuntimeException();
        }

        public void emit(ClassWriter cw) {
            String methodName = hasRestParam ? "doInvoke" : "invoke";
            //(p1,p2,省略,pn)=>... or (p1,p2,省略,pn,...r)=>... n<=20
            int maxParamCount = Fn.MAX_REQ_PARAM_COUNT + (hasRestParam ? 1 : 0);
            if (args.length() > maxParamCount) {
                throw new RuntimeException(String.format("arguments(%s) count must <= %s",
                        args.length(), maxParamCount));
            }

            Type[] pTypes = args.map(it -> Type.getType(Object.class)).toJavaArray(Type[]::new);
            Method method = new Method(methodName, Type.getType(Object.class), pTypes);
            GeneratorAdapter gen = new GeneratorAdapter(ACC_PUBLIC, method, null, null, cw);
            gen.visitCode();
            emitLineNumber(gen);
            start = gen.mark();
            end = gen.newLabel();

            args.forEachWithIndex((lb, i) -> lb.idx = acquireIdx(this, i + 1));
            try {
                Var.pushThreadBindings(HashMap.of(LOOP_LABEL, start));
                body.emit(C.EXPRESSION, objx, this, gen);
            } finally {
                Var.popThreadBindings();
            }
            gen.visitLabel(end);
            //生成本地变量表
            gen.visitLocalVariable("this", objx.thisType.getDescriptor(), null, start, end, 0);
            args.forEach(lb -> {
                lb.startLabel = start;
                lb.endLabel = end;
            });
            args.forEach(lb -> gen.visitLocalVariable(lb.sym.name, "Ljava/lang/Object;", null, lb.startLabel, lb.endLabel, lb.idx));
            gen.returnValue();
            gen.endMethod();
        }
    }

    enum BindingType {
        THIS, METHOD_ARG, LOOP_ARG, LOCAL
    }

    static class LocalBinding {
        Label startLabel;
        Label endLabel;
        Sym sym;
        int idx = -996;
        BindingType bindingType;
        boolean used;
        //自动清除
        boolean autoClear = true;
        PathNode clearPathRoot;


        LocalBinding(Sym sym, BindingType bindingType) {
            this.sym = sym;
            this.bindingType = bindingType;
        }

    }

    enum PATHTYPE {
        PATH, BRANCH;
    }

    static class PathNode {
        final PATHTYPE type;
        final PathNode parent;

        PathNode(PATHTYPE type, PathNode parent) {
            this.type = type;
            this.parent = parent;
        }
    }
}
