package sirenia.lang;

import io.vavr.collection.*;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public abstract class RT {
    public static final Vector EMPTY_VECTOR = Vector.empty();
    public static final Map EMPTY_MAP = HashMap.empty();
    public static final Set EMPTY_SET = HashSet.empty();

//    public static final Map<String,java.lang.reflect.Method> BIN_OPS;
//    public static final Map<String,java.lang.reflect.Method> PRE_OPS;
//    static {
//        Vector<java.lang.reflect.Method> methods = Vector.ofAll(Arrays.asList(RT.class.getMethods()));
//
//        BIN_OPS = methods.filter(it->it.isAnnotationPresent(BinOp.class))
//                .toMap(it->it.getAnnotation(BinOp.class).value(),it->it);
//
//        PRE_OPS = methods.filter(it->it.isAnnotationPresent(PreOp.class))
//                .toMap(it->it.getAnnotation(PreOp.class).value(),it->it);
//    }


//    public static Map<String,java.lang.reflect.Method> binOps(Class categoryClass){
//        Vector<java.lang.reflect.Method> methods = Vector.ofAll(Arrays.asList(categoryClass.getMethods()));
//        return methods.filter(it->it.isAnnotationPresent(BinOp.class))
//                .toMap(it->it.getAnnotation(BinOp.class).value(),it->it);
//    }
//
//    public static Map<String,java.lang.reflect.Method> preOps(Class categoryClass){
//        Vector<java.lang.reflect.Method> methods = Vector.ofAll(Arrays.asList(RT.class.getMethods()));
//
//        return methods.filter(it->it.isAnnotationPresent(PreOp.class))
//                .toMap(it->it.getAnnotation(PreOp.class).value(),it->it);
//    }
    private static AtomicLong id = new AtomicLong(0);

    public static String genName() {
        return genName("");
    }

    public static String genName(String origName) {
        long r = nextId();
        return "__auto__" + origName + "_" + Long.toString(r, 36);
    }

    public static boolean isGenName(String name) {
        return name.startsWith("__auto__");
    }
    private static long nextId() {
        return id.getAndUpdate(it -> it == Long.MAX_VALUE ? 0 : ++it);
    }

    public static Object println(Object o) {
        System.out.println(o);
        return o;
    }

    public static Object isEmpty(Object coll){
        if(coll instanceof Seq s)
            return s.isEmpty();
        else if(coll instanceof Map m)
            return m.isEmpty();
        throw new RuntimeException("not support yet!");
    }

    public static Object get(Object coll, Object key) {
        return get(coll,key,null);
    }
    public static Object get(Object coll, Object key,Object keyNotFound) {
        if (coll instanceof Map m) return m.getOrElse(key, keyNotFound);
        int idx = ((Number) key).intValue();
        if (coll instanceof Seq s){
            if(idx<s.length()){
                return s.get(idx);
            }else{
                return keyNotFound;
            }
        }
        if (coll instanceof java.util.List l) {
            if(idx<l.size()){
                return l.get(idx);
            }else{
                return keyNotFound;
            }
        }
        //if(coll.getClass().isArray())return ((Object[])coll)[((Number)key).intValue()];
        throw new RuntimeException("TODO");
    }

    public static Object drop(Object coll, Object n) {
        return ((Seq) coll).drop(((Number) n).intValue());
    }

    public static Object assoc(Object coll, Object key, Object value) {
        if (coll instanceof Map m) return m.put(key, value);
        if (coll instanceof Seq s) return s.update(((Number) key).intValue(), value);
        //if(coll instanceof java.util.List l)return l.get(((Number)key).intValue());
        //if(coll.getClass().isArray())return ((Object[])coll)[((Number)key).intValue()];
        throw new RuntimeException("TODO");
    }

    public static Object keys(Object coll){
        return ((Map)coll).keySet();
    }
    public static Object dissocAll(Object coll, Object keys) {
        return ((Map)coll).removeAll((Iterable) keys);
    }

    public static Object merge(Object coll, Object coll2) {
        if (coll instanceof Map m) return m.merge((Map) coll2, (o, n) -> n);
        if (coll instanceof Seq s) return s.appendAll((Iterable) coll2);
        //if(coll instanceof java.util.List l)return l.get(((Number)key).intValue());
        //if(coll.getClass().isArray())return ((Object[])coll)[((Number)key).intValue()];
        throw new RuntimeException("TODO");
    }

    public static Object cons(Object s1, Object s2) {
        return ((Seq) s2).prepend(s1);
    }

    public static Object concat(Object s1, Object s2) {
        if(s1.getClass().isArray()){
            s1 = vector(s1);
        }
        return ((Seq) s1).appendAll((Seq) s2);
    }

    @BinOp("<")
    public static Object lt(Object v1, Object v2) {
        return Maths.lt((Number) v1, (Number) v2);
    }
    @BinOp(">")
    public static Object gt(Object v1, Object v2) {
        return Maths.gt((Number) v1, (Number) v2);
    }

    @BinOp("==")
    public static Object eq(Object v1, Object v2) {
        if (v1 instanceof Number x && v2 instanceof Number y) {
            return Maths.eq(x, y);
        }
        return Objects.equals(v1, v2);
    }

    public static Object truth(Object x){
        return x!=null&&x!=Boolean.FALSE;
    }
    @BinOp("||")
    public static Object or(Object x,Object y){
        return (Boolean)truth(x)||(Boolean)truth(y);
    }

    @BinOp("+")
    public static Object add(Object v1, Object v2) {
        return Maths.add((Number) v1, (Number) v2);
    }

    @BinOp("-")
    public static Object sub(Object v1, Object v2) {
        return Maths.sub((Number) v1, (Number) v2);
    }

    @BinOp("/")
    public static Object div(Object v1, Object v2) {
        return Maths.div((Number) v1, (Number) v2);
    }

    @BinOp("%")
    public static Object rem(Object x,Object y){
        return Maths.rem((Number) x,(Number) y);
    }
    public static Object count(Object xs) {
        if (xs instanceof Vector<?> v) {
            return Long.valueOf(v.length());
        }
        throw new RuntimeException("TODO");
    }

    public static Object conj(Object xs, Object x) {
        return ((Seq) xs).append(x);
    }

    static public int boundedLength(Seq list, int limit) {
        int i = 0;
        for (Seq c = list; c != null && !c.isEmpty() && i <= limit; c = c.tail()) {
            i++;
        }
        return i;
    }



    private static class ReducedException extends RuntimeException {
        //TODO 异常栈？
    }

    private static final ReducedException reducedEx = new ReducedException();

    public static Object reduceWhile(Seq seq, Fn fn/*, Object init*/, Fn pred) {
        Box ret = new Box(null);
        try {
            seq.reduce((r, i) -> {
                if ((Boolean) pred.invoke(r, i)) {
                    ret.value = fn.invoke(r, i);
                    return ret.value;
                }
                throw reducedEx;
            });
        } catch (ReducedException e) {
        }
        return ret.value;
    }

    public static void main(String[] args) {
        Object r = reduceWhile(Vector.of(1, 2, 3, 4, 5), new Fn() {
            @Override
            public Object invoke(Object sum, Object item) {
                return (Integer) sum + (Integer) item;
            }
        }, new Fn() {
            @Override
            public Object invoke(Object sum, Object item) {
                //return (Integer)sum<10;
                return (Integer) item < 4;
            }
        });
        System.out.println(r);
    }

    public static Object vector(Object items) {
        if (items == null) return EMPTY_VECTOR;
        if (items.getClass().isArray())
            return EMPTY_VECTOR.appendAll(Arrays.asList((Object[]) items));
        throw new RuntimeException("un support yet");
    }

    public static Object vectorExpand(Object items, Set expandIdx) {
        if (items == null) return EMPTY_VECTOR;
        if (expandIdx == null || expandIdx.isEmpty()) return vector(items);
        Set<Integer> idx = expandIdx.map(it -> ((Number) it).intValue());
        if (items.getClass().isArray()) {
            Vector vec = EMPTY_VECTOR;
            Object[] oitems = (Object[]) items;
            for (int i = 0; i < oitems.length; i++) {
                vec = idx.contains(i) ? vec.appendAll((Iterable) oitems[i]) : vec.append(oitems[i]);
            }
            return vec;
        }
        throw new RuntimeException("un support yet");
    }

    public static Object hashset(Object kvs) {
        if (kvs == null) return EMPTY_SET;
        if (kvs.getClass().isArray()) {
            Object[] okvs = (Object[]) kvs;
            return EMPTY_SET.addAll(Arrays.asList(okvs));
        }
        throw new RuntimeException("un support yet");
    }

    public static Object hashmap(Object kvs) {
        if (kvs == null) return EMPTY_MAP;
        if (kvs.getClass().isArray()) {
            Object[] okvs = (Object[]) kvs;
            Map map = EMPTY_MAP;
            for (int i = 0; i < okvs.length; i = i + 2) {
                map = map.put(okvs[i], okvs[i + 1]);
            }
            return map;
        }
        throw new RuntimeException("un support yet");
    }

    /**
     * eg:
     * {a:1,b:2,...m1,c:3,d:4,...m2}
     * [k1,v1,k2,v2,map1,k4,v4,map2,...] expandIdx=[3,5] (type:Set)
     * 第3个pair和第5个pair是需要merge的map
     */
    public static Object hashmapExpand(Object kvs, Set<Number> expandIdx) {
        if (kvs == null) return EMPTY_MAP;
        if (expandIdx == null || expandIdx.isEmpty()) return hashmap(kvs);
        Set<Integer> idx = expandIdx.map(it -> it.intValue());
        if (kvs.getClass().isArray()) {
            Map map = EMPTY_MAP;
            Object[] okvs = (Object[]) kvs;
            for (int i = 0, pairIdx = 0; i < okvs.length; pairIdx++) {
                if (idx.contains(pairIdx)) {
                    map = map.merge((Map) okvs[i], (o, n) -> n);
                    i++;
                } else {
                    map = map.put(okvs[i], okvs[i + 1]);
                    i = i + 2;
                }
            }
            return map;
        }
        throw new RuntimeException("not support yet");
    }

    public static Object map(Object coll,Object f){
        Fn fn = (Fn) f;
        if(coll instanceof Seq s)
            return s.map(x->fn.invoke(x));
        throw new RuntimeException("not support yet");
    }

    public static Object range(Object from,Object to){
        return Maths.range((Number) from, (Number) to);
    }

    public static Object take(Object coll,Object n){
        if(coll instanceof Seq s)
            return s.take(((Number)n).intValue());
        throw new RuntimeException("not support yet");
    }

    public static Object sort(Object coll){
        if(coll instanceof Seq s)
            return s.sorted();
        else if(coll instanceof Set s)
            return List.ofAll(s).sorted();
        throw new RuntimeException("not support yet");
    }

    public static Object reverse(Object coll){
        if(coll instanceof Seq s)
            return s.reverse();
        throw new RuntimeException("not support yet");
    }
    public static Object repeat(Number n,Object x){
        int times = n.intValue();
        Seq s = List.empty();
        while(times-->0){
            s = s.prepend(x);
        }
        return s;
    }

    public static Object str(Object xs){
        if(xs instanceof Seq s){
            return s.mkString("");
        }
        throw new RuntimeException("not support yet");
    }
}
