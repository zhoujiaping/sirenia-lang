package sirenia.lang;

public class Sym {
    String name;

    Sym(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)return false;
        if(this == obj) return true;
        if(!(obj instanceof Sym))return false;
        return this.name.equals(((Sym)obj).name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
