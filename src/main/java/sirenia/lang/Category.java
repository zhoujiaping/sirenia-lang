package sirenia.lang;

import io.vavr.collection.Map;

import java.lang.reflect.Method;

public final class Category {

    final Category parent;
    //Class clazz;

    final Map<String, Method> binOps;
    final Map<String, Method> preOps;

    public Category(Category parent, Map<String, Method> binOps, Map<String, Method> preOps) {
        this.parent = parent;
        this.binOps = binOps;
        this.preOps = preOps;
    }

    public Method findBinOp(String op) {
        Method opMethod = binOps.getOrElse(op, null);
        return opMethod != null ? opMethod : parent.findBinOp(op);
    }

    public Method findPreOp(String op) {
        Method opMethod = preOps.getOrElse(op, null);
        return opMethod != null ? opMethod : parent.findPreOp(op);
    }
}
