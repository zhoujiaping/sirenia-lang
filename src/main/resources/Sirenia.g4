grammar Sirenia;

file: exprs;
exprs: (expr SEMI)* expr?;

expr:
expr LPAREN (EXPAND expr)? RPAREN  # FnCallExpr
|expr LPAREN expr (',' expr)* (',' EXPAND expr)? RPAREN  # FnCallExpr//antler4 可以处理直接左递归，但是不能处理间接左递归。所以fnCall直接写在这里。
|defnExpr # Defn
//|assignExpr # Assign
|ifExpr #If
//|caseExpr
|condExpr #Cond
|loopExpr #Loop
|recurExpr #Recur
|tryCatchExpr # TryCatch
|throwExpr #Throw
|expr '.' ID  # IdAttrExpr
|expr '[' expr ']' # ExprAttrExpr
//|sufExpr
|ID # idRefExpr
//|pipExpr
//|delayExpr // soService(x,y)
|<assoc=right> op=(NOT|BIT_NEG|ADD|SUB) expr #PreExpr
| expr op=(MUL|DIV|REM/*|QUOT*/) expr # BinExpr
| expr op=(ADD|SUB) expr # BinExpr
| expr op=(SHIFT_LEFT|SHIFT_RIGHT|SHIFT_RIGHT_UNSIGNED) expr # BinExpr
| expr op=(LT|LT_EQ|GT|GT_EQ) expr # BinExpr
| expr op=(EQ|NOT_EQ) expr # BinExpr
| expr op=(BIT_AND|BIT_XOR|BIT_OR) expr # BinExpr
| expr op=(AND|OR) expr # BinExpr
| expr op=(RANGE|RANGE_TO) expr # BinExpr
|LBRACE exprs RBRACE # ScopeExpr
|literalExpr #Literal
|LPAREN exprs RPAREN # ParenExpr
|javaInterOpExpr # javaInterOp
|categoryExpr # category
|expr '|>' ID expr # pipExpr
|'def' defItem (',' defItem)* # DefExpr
;

categoryExpr:'use' LPAREN STRING RPAREN LBRACE exprs RBRACE;
/*
use("org.demo.DecimalCategory"){
    def a = 3.14*2
}

对于运算符，会优先使用DecimalCategory中定义的方法。
例如，可以支持数学运算都采用BigDecimal来计算，避免像java那样写一堆代码导致大量语法噪声。
还可以实现日期时间的加减运算。
*/

maybeExpandExpr: EXPAND? expr;

/*javainterop
如果存在重载版本，会调用哪个方法？
例如
Object add(Object x,Object y){...} 版本1
Object add(Number x,Number y){...} 版本2
某参数未指定类型，则该位置匹配任意类型。
某参数指定了类型，则匹配指定类型。
所以
#invokeStatic sirenia.lang.RT.add(1:java.lang.Number,2); 会匹配版本2。
#invokeStatic sirenia.lang.RT.add(1,2); 会匹配到两个方法，报错。
*/
javaInterOpExpr:
'#import' LPAREN STRING ('as' STRING)? # importClass
|'#new' LPAREN STRING (',' typedExpr)* RPAREN # newObjExpr
|('#'|'#invoke') LPAREN STRING (',' typedExpr)* RPAREN # methodCallExpr
|'#get' LPAREN STRING (',' expr)? RPAREN# fieldGetExpr
|'#set' LPAREN STRING ',' expr (',' expr)? RPAREN # fieldSetExpr;

INVOKE_TYPE:'invoke'|'invokeStatic';
typedExpr: expr STRING?;

defItem: (ID|structParams) ('=' expr)?;

defnExpr:
'defn' ID LPAREN fnParams RPAREN expr # defnExpr1
|'defn' ID LBRACE fnLiteral (SEMI fnLiteral)* SEMI? RBRACE #defnExprN;
/*
defn sum(){

}
defn sum{
  (x)=>x ;
  (x,y)=> x+y
}

*/
fnParams : (EXPAND (ID|structParams))?
| paramsD (',' paramsD)* (',' EXPAND (ID|structParams))?;

paramsD : (ID|structParams) ('=' expr)? ;

//assignExpr: ID '=' expr;

throwExpr: 'throw' expr;

tryCatchExpr: tryBlock (catchBlock)? (finallyBlock)?;
tryBlock:'try' expr;
catchBlock:'catch' '(' ID ')' expr;
finallyBlock: 'finally' expr;

ifExpr:'if' expr expr ('else' expr)?;

condExpr: 'cond' LBRACE RBRACE
| 'cond' LBRACE (expr COND_SEP expr) (';' expr COND_SEP expr)* ';'? RBRACE;

COND_SEP: '->';

loopExpr: 'loop' LPAREN RPAREN expr
| 'loop' LPAREN loopParam '=' expr (',' loopParam '=' expr)* RPAREN expr
;//loop([a,b]=[1,2] )
loopParam:ID|structParams;

recurExpr: 'recur' LPAREN RPAREN
|'recur' LPAREN expr (',' expr)* (',' EXPAND expr)? RPAREN
;
literalExpr:
CHAR # charLiteral
| STRING # strLiteral
| '0' # zeroLiteral //0
| INT # intLiteral
// | RATIO # ratio 除法自动返回分数
| FLOAT # floatLiteral
//|REG
| 'true' # trueLiteral
| 'false' # falseLiteral
| 'null' # nullLiteral
|TMPL_STRING # tmplStringLiteral
| LBRACE RBRACE # emptyMapLiteral
| LBRACE kvPair (',' kvPair)* (',')? RBRACE # MapLiteral
| LBRACK RBRACK # emptyVecLiteral
| LBRACK maybeExpandExpr (',' maybeExpandExpr)* ','? RBRACK # VecLiteral
| fnLiteral  # Fn // (name)=>println(name)
;
fnLiteral: (ID|(LPAREN fnParams RPAREN)) '=>' expr;

kvPair:ID ':' expr # IdKeyPair
|STRING ':' expr # StrKeyPair
|LPAREN expr RPAREN ':' expr # ExprKeyPair
|EXPAND expr # expandPair
;

structParams: vecStructParams | mapStructParams;

vecStructParams: LBRACK (EXPAND (ID|vecStructParams))? as? RBRACK //(...r)
| LBRACK paramsD (','paramsD)* (',' EXPAND (ID|vecStructParams))? as? RBRACK ;//(a=1,...r)

mapStructParams: LBRACE (EXPAND (ID|mapStructParams))? as? RBRACE
| LBRACE structPair (',' structPair)* (','  EXPAND (ID|mapStructParams))? as? RBRACE;

structPair: ID (':' paramsD)? ;

as: 'as' ID;

//SIGN:[\-+];

//POS:'+';
//NEG:'-';

NOT:'!';
BIT_NEG:'~';
EXPAND: '...';

POW:'**';

MUL:'*';
DIV:'/';
REM:'%';
//QUOT:'//';

ADD:'+';
SUB:'-';

SHIFT_LEFT:'<<';
SHIFT_RIGHT:'>>';
SHIFT_RIGHT_UNSIGNED:'>>>';

LT:'<';
LT_EQ:'<=';
GT:'>';
GT_EQ:'>=';

EQ:'==';
NOT_EQ: '!=';

BIT_AND:'&';
BIT_XOR:'^';
BIT_OR:'|';
AND:'&&';
OR:'||';

RANGE:'..<';
RANGE_TO:'..';

TMPL_STRING: '"""' (ESC | ~["\\])* '"""' ;
CHAR: '\'' (('\\' (['\\/bfnrt]|UNICODE))|~['\\]) '\'';

STRING: '"' (ESC | ~["\\])* '"';

INT: ([1-9][0-9]* 'N'?)
|('0'[xX][0-9A-Fa-f]+ 'N'?)
|[1-9][0-9]?[rR][0-9A-Za-z]+ 'N'?;
FLOAT: [0-9]+('.'[0-9]*)([eE][\-+]?[0-9]+)?'M'?;
ID : [_$a-zA-Z][_$a-zA-Z0-9]*;// 支持中文？

LPAREN : '(';
RPAREN : ')';

LBRACK : '[';
RBRACK : ']';

LBRACE : '{';
RBRACE : '}';

NEWLINE: '\r'? '\n' -> channel(HIDDEN);
//NEWLINE: '\r'? '\n' -> skip;
SEMI:';';
WS: [ \t\r\n]+ -> channel(HIDDEN) ;
LINE_COMMENT: '//'.*? '\n' -> channel(HIDDEN);
COMMENT: '/*'.*? '*/' -> channel(HIDDEN);
fragment ESC : '\\' (["\\/bfnrt]|UNICODE);
fragment UNICODE: 'u' HEX HEX HEX HEX;
fragment HEX: [0-9A-Fa-f];



