源代码 -> 
词法分析（antlr）-> 
语法分析（antlr）-> 
语义分析（sirenia.lang.Compiler）-> 
字节码生成（sirenia.lang.Compiler）

首先我们定义了词法和语法（Sirenia.g4文件），  
词法分析由SireniaLexer完成，  
它是我们通过运行antlr的org.antlr.v4.Tool#main生成的。   
它会同时生成语法分析器SireniaParser，  
我们用它和词法分析得到的tokens生成抽象语法树（AST）。   
接下来我们需要做语义分析。  
做语义分析，需要遍历AST，antlr贴心的为我们提供了Listener和Visitor两种模式。   
我们选择了Visitor模式，因为它比Listener更灵活。   
我们的Compiler继承了antlr生成的SireniaBaseVisitor。   

如果不考虑编译成字节码，可以直接在遍历AST的时候进行运算。   
但是这样性能会非常不理想（因为需要自己实现函数调用栈，需要自己去实现各种优化）。  
为了能够利用jit帮助我们优化性能，所以选择了编译成字节码。   


