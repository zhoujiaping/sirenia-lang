println("=>"+sirenia.lang.Compiler.eval("user","""

defn keys(coll){
    #("RT/keys",coll)
};
defn sort(coll){
    #("RT/sort",coll)
};
defn reverse(coll){
    #("RT/reverse",coll)
};

defn get(coll,key){
    #("RT/get",coll,key)
};
def romanNums = {
    (1000):"M",
    (900):"CM", (500):"D", (400):"CD", (100):"C",
    (90):"XC", (50):"L", (40):"XL", (10):"X",
    (9):"IX", (5):"V", (4):"IV", (1):"I",
};

def nums = keys(romanNums) |> it sort(it) |> it reverse(it);

def x = 1/0;
//def a = 1;
//def a = a+1;
"""))