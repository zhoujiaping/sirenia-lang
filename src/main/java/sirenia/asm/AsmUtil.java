package sirenia.asm;

import com.github.kiprobinson.bigfraction.BigFraction;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.commons.Method;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;

import static org.objectweb.asm.Opcodes.*;

public class AsmUtil {

    public static final Map<Class, BiConsumer<GeneratorAdapter, Object>> WRAPPERS = new HashMap<>();

    static {
        WRAPPERS.put(Integer.class, (gen, x) -> {
            gen.push((int)x);
            gen.invokeStatic(Type.getType(Integer.class), getMethod("int", Integer.class, "valueOf", int.class));
        });
        WRAPPERS.put(Long.class, (gen, x) -> {
            gen.push((long)x);
            gen.invokeStatic(Type.getType(Long.class), getMethod("long", Long.class, "valueOf", long.class));
        });
        WRAPPERS.put(Double.class, (gen, x) -> {
            gen.push((double)x);
            gen.invokeStatic(Type.getType(Double.class), getMethod("double", Double.class, "valueOf", double.class));
        });
        WRAPPERS.put(Character.class, (gen, x) -> {
            gen.push((char)x);
            gen.invokeStatic(Type.getType(Character.class), getMethod("char", Character.class, "valueOf", char.class));
        });
        WRAPPERS.put(Boolean.class, (gen, x) -> {
            gen.getStatic(Type.getType(Boolean.class), (Boolean)x ? "TRUE" : "FALSE", Type.getType(Boolean.class));
            //gen.push((boolean)x);
            //gen.invokeStatic(Type.getType(Boolean.class), getMethod("char", Boolean.class, "valueOf", boolean.class));
        });
        WRAPPERS.put(BigDecimal.class, (gen, x) -> {
            gen.push(x.toString());
            gen.invokeStatic(Type.getType(BigDecimal.class), getMethod("bigDecimal", BigDecimal.class, "<init>", String.class));
        });
        WRAPPERS.put(BigInteger.class, (gen, x) -> {
            gen.push(x.toString());
            gen.invokeStatic(Type.getType(BigInteger.class), getMethod("bigDecimal", BigDecimal.class, "<init>", String.class));
        });
        WRAPPERS.put(BigFraction.class, (gen, x) -> {
            gen.push(x.toString());
            gen.invokeStatic(Type.getType(BigFraction.class), getMethod("bigFraction", BigFraction.class, "valueOf", String.class));
        });
        WRAPPERS.put(String.class, (gen, x) -> gen.push(x.toString()));
    }

    private static final ConcurrentHashMap<String, Method> methods = new ConcurrentHashMap<>();

    public static Method getMethod(String id, Class owner, String name, Class... paramTypes) {
        Method m = methods.get(id);
        if (m != null) return m;
        try {
            m = Method.getMethod(owner.getMethod(name, paramTypes));
            methods.put(id, m);
            return m;
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
    /**
     * 往栈顶放置对象类型的常量。
     * jvm指令不支持BigInteger、BigDecimal等类型的常量，这里提供支持（实际上可能是调用方法/字段）。
     */
    public static void pushObjLdc(GeneratorAdapter gen,Object x) {
        if (x == null) {
            gen.visitInsn(ACONST_NULL);
            return;
        }
        WRAPPERS.get(x.getClass()).accept(gen, x);
    }

    public static void clearLocal(GeneratorAdapter gen,int idx){
        gen.visitInsn(ACONST_NULL);
        storeLocal(gen,idx);
    }
    public static void loadLocal(GeneratorAdapter gen,int idx){
        gen.visitVarInsn(Type.getType(Object.class).getOpcode(Opcodes.ILOAD), idx);
        //gen.visitVarInsn(ALOAD,idx);
    }
    public static void storeLocal(GeneratorAdapter gen,int idx){
        gen.visitVarInsn(Type.getType(Object.class).getOpcode(ISTORE),idx);
        //gen.visitVarInsn(ASTORE,idx);
    }

}
