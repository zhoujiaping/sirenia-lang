package sirenia.lang;

import org.antlr.v4.runtime.*;

public class BailErrorStrategy extends DefaultErrorStrategy {
    //不同异常中恢复
    @Override
    public void recover(Parser recognizer, RecognitionException e) {
        //super.recover(recognizer, e);
        throw new RuntimeException(e);
    }

    //不会试图执行行内恢复
    @Override
    public Token recoverInline(Parser recognizer) throws RecognitionException {
        //return super.recoverInline(recognizer);
        throw new RuntimeException(new InputMismatchException(recognizer));
    }
    //确保不会试图从子规则的问题中恢复
    @Override
    public void sync(Parser recognizer) throws RecognitionException {
        //super.sync(recognizer);
        //noop
    }
}
