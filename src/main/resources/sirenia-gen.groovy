import org.antlr.v4.Tool

def userDir = System.getProperty("user.dir")
def projectRoot = new File(userDir).parentFile.parentFile.parentFile
def opts = "${userDir}/Sirenia.g4 -no-listener -visitor -o ${projectRoot}/src/main/java/sirenia -package sirenia"
Tool.main(opts.split(/\s+/))