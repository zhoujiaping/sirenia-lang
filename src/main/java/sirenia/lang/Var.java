package sirenia.lang;

import io.vavr.Tuple2;
import io.vavr.collection.Map;

import java.util.Iterator;

public class Var {
    public static Var UNBOUND = new Var(){
        @Override
        public String toString() {
            return "<UNBOUND>";
        }
    };
    static class TBox {
        volatile Object val;
        final Thread thread;

        public TBox(Thread t, Object val) {
            this.thread = t;
            this.val = val;
        }

    }

    volatile Object root;

    protected Var() {
    }

    public static Var create(Object rootVal) {
        Var var = new Var();
        var.root = rootVal;
        return var;
    }

    final public <T> T deref() {
        TBox b = getThreadBinding();
        if (b != null)
            return (T) b.val;
        return (T) root;
    }

    public Object set(Object val) {
        TBox b = getThreadBinding();
        if (b != null) {
            if (Thread.currentThread() != b.thread)
                throw new IllegalStateException(String.format("Can't set! from non-binding thread"));
            return (b.val = val);
        }
        throw new IllegalStateException(String.format("Can't change/establish root binding of: with set"));
    }

    public final TBox getThreadBinding() {
        return (TBox) dvals.get().vars.getOrElse(this, null);
    }

    private static ThreadLocal<Frame> dvals = new ThreadLocal<>() {
        @Override
        protected Frame initialValue() {
            return Frame.TOP;
        }
    };

    public static Object getThreadBindingFrame() {
        return dvals.get();
    }

    public static void pushThreadBindings(Map bindings) {
        Frame f = dvals.get();
        Map bmap = f.vars;

        for (Iterator<Tuple2> iter = bindings.iterator(); iter.hasNext(); ) {
            Tuple2 e = iter.next();
            Var v = (Var) e._1;
            bmap = bmap.put(v, new TBox(Thread.currentThread(), e._2));
        }
        dvals.set(new Frame(bmap, f));
    }

    public static void popThreadBindings() {
        Frame f = dvals.get().prev;
        if (f == null) {
            throw new IllegalStateException("Pop without matching push");
        } else if (f == Frame.TOP) {
            dvals.remove();
        } else {
            dvals.set(f);
        }
    }
}
