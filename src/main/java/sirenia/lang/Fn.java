package sirenia.lang;

import io.vavr.collection.Seq;

import java.util.concurrent.Callable;

/**
 * 调用函数时，如果包含拓展运算符（...），则将元素收集到Seq，然后调用applyTo。
 * applyTo会分派给具体的invoke方法。
 * */
public abstract class Fn implements Runnable, Callable {
    public static final int MAX_REQ_PARAM_COUNT = 20;
    public String name;

    static public Object applyToHelper(Fn fn, Seq arglist) {
        switch (RT.boundedLength(arglist, 20)) {
            case 0:
                return fn.invoke();
            case 1:
                return fn.invoke(Util.ret1(arglist.head(), arglist = null));
            case 2:
                return fn.invoke(arglist.head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 3:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 4:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 5:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 6:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 7:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 8:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 9:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 10:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 11:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 12:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 13:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 14:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 15:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 16:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 17:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 18:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 19:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            case 20:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1((arglist = arglist.tail()).head(), arglist = null)
                );
            default:
                return fn.invoke(arglist.head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , (arglist = arglist.tail()).head()
                        , Util.ret1(arglist.toJavaArray(), arglist = null));
        }
    }

    public Object applyTo(Seq args) {
        return applyToHelper(this, args);
    }


    @Override
    public void run() {
        invoke();
    }

    @Override
    public Object call() {
        return invoke();
    }

    public Object invoke() {
        return throwArity(0);
    }

    public Object invoke(Object arg0) {
        return throwArity(1);
    }

    public Object invoke(Object arg0, Object arg1) {
        return throwArity(2);
    }

    public Object invoke(Object arg0, Object arg1, Object arg2) {
        return throwArity(3);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3) {
        return throwArity(4);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4) {
        return throwArity(5);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5) {
        return throwArity(6);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6) {
        return throwArity(7);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7) {
        return throwArity(8);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8) {
        return throwArity(9);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9) {
        return throwArity(10);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9, Object arg10) {
        return throwArity(11);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9, Object arg10, Object arg11) {
        return throwArity(12);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9, Object arg10, Object arg11, Object arg12) {
        return throwArity(13);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13) {
        return throwArity(14);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14) {
        return throwArity(15);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14, Object arg15) {
        return throwArity(16);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14, Object arg15, Object arg16) {
        return throwArity(17);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14, Object arg15, Object arg16, Object arg17) {
        return throwArity(18);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14, Object arg15, Object arg16, Object arg17, Object arg18) {
        return throwArity(19);
    }


    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14, Object arg15, Object arg16, Object arg17, Object arg18, Object arg19) {
        return throwArity(20);
    }

    public Object invoke(Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8, Object arg9, Object arg10, Object arg11, Object arg12, Object arg13, Object arg14, Object arg15, Object arg16, Object arg17, Object arg18, Object arg19, Object... rest) {
        return throwArity(21);
    }

    public Object throwArity(int n){
        String name = getClass().getName();
        throw new ArityException(n, name);
    }

}
